DROP INDEX IF EXISTS sessions_active_view_index;
DROP INDEX IF EXISTS sessions_last_touched_index;
DROP INDEX IF EXISTS sessions_user_index;

DROP INDEX IF EXISTS requests_email_index;
DROP INDEX IF EXISTS requests_section_index;
DROP INDEX IF EXISTS requests_event_section_index;

DROP INDEX IF EXISTS event_section_helpers_helper_index;
DROP INDEX IF EXISTS event_section_helpers_section_index;

DROP INDEX IF EXISTS event_sections_section_index;

DROP INDEX IF EXISTS event_organizers_organizer_index;

DROP INDEX IF EXISTS events_archived_until_index;
DROP INDEX IF EXISTS events_decisions_until_index;

DROP INDEX IF EXISTS management_user_admin_for_section_index;

DROP INDEX IF EXISTS section_tags_tag_index;

DROP INDEX IF EXISTS section_heads_head_index;

DROP INDEX IF EXISTS email_users_last_touched_index;
