INSERT INTO meta(version, signin_length, archive_length, passive_update_length, active_update_length)
VALUES (0, INTERVAL '1 hour', INTERVAL '1 year', INTERVAL '15 seconds', INTERVAL '1 second')
ON CONFLICT (meta_id) DO UPDATE
SET version = 0;

DO $$
BEGIN
    IF NOT EXISTS (SELECT * FROM management_users)
    THEN
        INSERT INTO management_users(email, kind, complete, display_name, password_hash)
        VALUES (
            'admin@change.this',  -- used for removal as well!
            'super-admin',
            TRUE,
            'Super Admin',
            -- the password is "change this"
            '$2y$10$eH9riQY4BlfG04Gq52WkA.gB0pvto004CipQ7QxdZ6QYqlXC0HSsy'
        );
    END IF;

    IF NOT EXISTS (SELECT * FROM sections WHERE guest OR id = 'guest')
    THEN
        INSERT INTO sections(id, name, guest, stats)
        VALUES ('guest', 'Gäste', TRUE, ROW(0, 0, NOW()));
    END IF;
END
$$
