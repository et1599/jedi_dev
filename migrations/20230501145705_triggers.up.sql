-- `user_admin_for` is only valid if the user kind is an `organizer`
CREATE FUNCTION no_management_user_admin_for_if_not_admin()  -- triggered on insert into `umanagement_ser_admin_for`
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
    IF EXISTS (SELECT * FROM management_users WHERE id = NEW.user_id AND kind <> 'admin')
    THEN
        RAISE EXCEPTION 'cannot add admin status for sections if the user isnt an admin';
    END IF;
    RETURN NULL;
END;
$$;

CREATE TRIGGER no_management_user_admin_for_if_not_admin_trigger
    AFTER INSERT ON management_user_admin_for
    FOR EACH ROW
    EXECUTE FUNCTION no_management_user_admin_for_if_not_admin();


CREATE FUNCTION fix_management_user_admin_for()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
    IF NEW.kind <> 'admin'
    THEN
        DELETE FROM management_user_admin_for
        WHERE user_id = NEW.id;
    END IF;
    RETURN NULL;
END;
$$;

CREATE TRIGGER fix_management_user_admin_for_trigger
    AFTER UPDATE OF kind ON management_users
    FOR EACH ROW
    EXECUTE FUNCTION fix_management_user_admin_for();


CREATE FUNCTION log_everyone_out()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
    DELETE FROM sessions;
    RETURN NULL;
END;
$$;

CREATE TRIGGER log_everyone_out_on_meta_update
    AFTER UPDATE OR DELETE OR INSERT ON meta
    EXECUTE FUNCTION log_everyone_out();

CREATE FUNCTION drop_empty_email_user()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
    IF NOT EXISTS (SELECT * FROM requests AS after WHERE after.email = OLD.email)
    THEN
        DELETE FROM email_users
        WHERE email_users.email = OLD.email;
    END IF;
    RETURN NULL;
END;
$$;

CREATE TRIGGER drop_empty_email_user_after_request_del
    AFTER DELETE ON requests
    FOR EACH ROW
    EXECUTE FUNCTION drop_empty_email_user();

CREATE FUNCTION remove_partial_requests_on_inactive_events()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
   IF NEW.state <> 'active'
   THEN
       DELETE FROM requests
       WHERE event = NEW.id AND NOT filled;
   END IF;
   RETURN NULL;
END;
$$;

CREATE TRIGGER remove_partial_requests_on_inactive_events_trigger
    AFTER UPDATE OF state ON events
    FOR EACH ROW
    EXECUTE FUNCTION remove_partial_requests_on_inactive_events();

-- TODO: trigger to check event forms
