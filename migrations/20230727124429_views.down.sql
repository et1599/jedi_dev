DROP VIEW IF EXISTS valid_sessions;
DROP VIEW IF EXISTS relevant_event_sections;
DROP VIEW IF EXISTS events_with_misc;
DROP VIEW IF EXISTS sections_with_tags;
DROP VIEW IF EXISTS management_users_with_admin_for;
DROP TYPE IF EXISTS BASIC_SECTION_INFO;
DROP TYPE IF EXISTS BASIC_USER_INFO;
