CREATE TYPE USER_KIND AS ENUM ('normal', 'organizer', 'admin', 'super-admin');

CREATE TYPE SECTION_STATS AS (
    youth_leaders INTEGER,
    member_count INTEGER,
    last_update DATE
);

CREATE TYPE EVENT_STATE AS ENUM ('setup', 'active', 'done');

CREATE TABLE meta(
    meta_id INTEGER NOT NULL PRIMARY KEY DEFAULT (1) CHECK (meta_id = 1),
    version INTEGER NOT NULL,
    signin_length INTERVAL NOT NULL,
    archive_length INTERVAL NOT NULL,
    passive_update_length INTERVAL NOT NULL,
    active_update_length INTERVAL NOT NULL
);

CREATE TABLE management_users(
    id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    email TEXT NOT NULL UNIQUE,
    kind USER_KIND NOT NULL,
    complete BOOLEAN NOT NULL DEFAULT FALSE,
    partial_secret TEXT,
    display_name TEXT,
    password_hash TEXT,
    password_reset_secret TEXT,
    CHECK (
        (complete AND partial_secret IS NULL AND display_name IS NOT NULL) OR
        (
            NOT complete AND partial_secret IS NOT NULL AND password_hash IS NULL
            AND password_reset_secret IS NULL
        )
    )
);

CREATE TABLE email_users(
    email TEXT NOT NULL PRIMARY KEY,
    secret TEXT NOT NULL
);

CREATE TABLE sections(
    id TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    guest BOOLEAN NOT NULL DEFAULT FALSE,
    stats SECTION_STATS NOT NULL CHECK (stats IS NOT NULL),
    CHECK ((guest AND (stats).youth_leaders = 0 AND (stats).member_count = 0) OR NOT guest)
);

CREATE TABLE section_heads(
    section TEXT NOT NULL REFERENCES sections(id) ON DELETE CASCADE ON UPDATE CASCADE,
    head UUID NOT NULL REFERENCES management_users(id) ON DELETE CASCADE,
    PRIMARY KEY (section, head)
);

CREATE TABLE section_tags(
    section TEXT NOT NULL REFERENCES sections(id) ON DELETE CASCADE ON UPDATE CASCADE,
    tag TEXT NOT NULL,
    PRIMARY KEY (section, tag)
);

CREATE TABLE management_user_admin_for(
    user_id UUID NOT NULL REFERENCES management_users(id) ON DELETE CASCADE,
    section TEXT NOT NULL REFERENCES sections(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (user_id, section)
);

CREATE TABLE events(
    id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    state EVENT_STATE NOT NULL,
    signup_until DATE,
    decisions_until DATE,
    archived_until DATE,
    CHECK (
        (
            state = 'setup' AND
            signup_until IS NULL AND decisions_until IS NULL AND
            archived_until IS NULL
        ) OR (
            state = 'active' AND
            signup_until IS NOT NULL AND decisions_until IS NOT NULL AND
            decisions_until >= signup_until AND
            archived_until IS NULL
        ) OR (
            state = 'done' AND
            signup_until IS NULL AND decisions_until IS NULL AND
            archived_until IS NOT NULL
        )
    ),
    -- very complex to model correctly for relatively minor gain
    -- also triggers exist
    form JSON[] NOT NULL,
    can_users_leave BOOLEAN NOT NULL,
    can_users_edit_name BOOLEAN NOT NULL,
    max_total_delegates INTEGER NOT NULL
);

CREATE TABLE event_organizers(
    event UUID NOT NULL REFERENCES events(id) ON DELETE CASCADE,
    organizer UUID NOT NULL REFERENCES management_users(id) ON DELETE CASCADE,
    PRIMARY KEY (event, organizer)
);

CREATE TABLE event_sections(
    event UUID NOT NULL REFERENCES events(id) ON DELETE CASCADE,
    section TEXT NOT NULL REFERENCES sections(id) ON DELETE CASCADE ON UPDATE CASCADE,
    max_delegates INTEGER NOT NULL,
    last_section_stats SECTION_STATS NOT NULL CHECK (last_section_stats IS NOT NULL),
    secret TEXT NOT NULL,
    PRIMARY KEY (event, section)
);

CREATE TABLE event_section_helpers(
    event UUID NOT NULL REFERENCES events(id) ON DELETE CASCADE,
    section TEXT NOT NULL REFERENCES sections(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (event, section) REFERENCES event_sections(event, section)
        ON DELETE CASCADE ON UPDATE CASCADE,
    helper UUID NOT NULL REFERENCES management_users(id) ON DELETE CASCADE,
    PRIMARY KEY (event, section, helper)
);

CREATE TABLE requests(
    id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    event UUID NOT NULL REFERENCES events(id) ON DELETE CASCADE,
    section TEXT NOT NULL REFERENCES sections(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (event, section) REFERENCES event_sections(event, section)
        ON DELETE CASCADE ON UPDATE CASCADE,
    email TEXT NOT NULL REFERENCES email_users(email) ON DELETE CASCADE ON UPDATE CASCADE,
    filled BOOLEAN NOT NULL,
    first_name TEXT,
    last_name TEXT,
    filled_form JSON[],
    CHECK (
        (NOT filled AND first_name IS NULL AND last_name IS NULL AND filled_form IS NULL)
        OR (filled AND first_name IS NOT NULL AND last_name IS NOT NULL AND filled_form IS NOT NULL)
    ),
    position INTEGER,
    accepted_as_guest BOOLEAN NOT NULL DEFAULT FALSE CHECK (
        (position IS NULL) OR (position IS NOT NULL AND NOT accepted_as_guest)
    ),
    UNIQUE (event, section, position)
);

CREATE UNLOGGED TABLE sessions(
    id UUID NOT NULL PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL REFERENCES management_users(id) ON DELETE CASCADE,
    last_touched TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    active_view_uri TEXT,
    active_view_until TIMESTAMPTZ,
    CHECK (
        (active_view_uri IS NULL AND active_view_until IS NULL)
        OR (active_view_uri IS NOT NULL AND active_view_until IS NOT NULL)
    )
);
