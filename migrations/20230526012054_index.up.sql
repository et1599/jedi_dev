CREATE INDEX email_users_last_touched_index ON sessions(last_touched);  -- for email user purging

-- IMPLIED: CREATE INDEX section_heads_section_index ON section_heads(section);  -- for section deletion cascade
CREATE INDEX section_heads_head_index ON section_heads(head);  -- for user deletion cascade, relevant event listing

-- IMPLIED: CREATE INDEX section_tags_section_index ON section_tags(section);  -- for tag retrieval
CREATE INDEX section_tags_tag_index ON section_tags(tag);  -- for tag search

-- IMPLIED: CREATE INDEX management_user_admin_for_user_index ON user_admin_for(user_id);  -- for user deletion cascade
CREATE INDEX management_user_admin_for_section_index ON management_user_admin_for(section);  -- for section deletion cascade

CREATE INDEX events_decisions_until_index ON events(decisions_until);  -- for timed event state updates
CREATE INDEX events_archived_until_index ON events(archived_until);  -- for timed event state updates

-- IMPLIED: CREATE INDEX event_organizers_event_index ON event_organizers(event);  -- for event deletion cascade, event organizer listing
CREATE INDEX event_organizers_organizer_index ON event_organizers(organizer);  -- for user deletion cascade, relevant event listing

-- IMPLIED: CREATE INDEX event_sections_event_index ON event_sections(event);  -- for event deletion cascade, event section listing, calculation
CREATE INDEX event_sections_section_index ON event_sections(section);  -- for relevant event listing via section head

CREATE INDEX event_section_helpers_section_index ON event_section_helpers(section);  -- for section deletion cascade
CREATE INDEX event_section_helpers_helper_index ON event_section_helpers(helper);  -- for finding relevant event listing via helper, user deletion cascade

CREATE INDEX requests_event_section_index ON requests(event, section);  -- for event section listing, event deletion cascade
CREATE INDEX requests_section_index ON requests(section);  -- for section deletion cascade
CREATE INDEX requests_email_index ON requests(email);  -- for anything email user related

CREATE INDEX sessions_user_index ON sessions(user_id);  -- for user deletion cascade
CREATE INDEX sessions_last_touched_index ON sessions(last_touched);  -- for section purging
CREATE INDEX sessions_active_view_index ON sessions(active_view_uri);  -- for distinguishing active reload necessity (and displaying other users)
