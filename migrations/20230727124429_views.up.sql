CREATE TYPE BASIC_USER_INFO AS (
    id UUID,
    complete BOOLEAN,
    email TEXT,
    display_name TEXT,
    kind USER_KIND,
    admin_for TEXT[]
);

CREATE TYPE BASIC_SECTION_INFO AS (
    id TEXT,
    name TEXT,
    guest BOOLEAN
);

CREATE VIEW management_users_with_admin_for AS
    SELECT *, ARRAY(SELECT section FROM management_user_admin_for WHERE user_id = id) AS admin_for
    FROM management_users;

CREATE VIEW sections_with_tags AS
    SELECT *, ARRAY(SELECT tag FROM section_tags WHERE section = id) AS tags
    FROM sections;

CREATE VIEW events_with_misc AS
    SELECT
        *,
        ARRAY(
            SELECT CAST(
                (u.id, u.complete, u.email, u.display_name, u.kind, u.admin_for) AS BASIC_USER_INFO
            )
            FROM event_organizers AS o JOIN management_users_with_admin_for AS u ON o.organizer = u.id
            WHERE o.event = e.id
        ) AS organizers,
        ARRAY(
            SELECT CAST((s.id, s.name, s.guest) AS BASIC_SECTION_INFO)
            FROM event_sections AS es JOIN sections AS s ON es.section = s.id
            WHERE es.event = e.id
        ) AS sections
    FROM events AS e;

CREATE VIEW relevant_event_sections AS
    (
        SELECT event, section, helper AS user, 'helper' AS reason
        FROM event_section_helpers
    ) UNION (
        SELECT
            event_sections.event AS event,
            event_sections.section AS section,
            section_heads.head AS user,
            'head' AS reason
        FROM event_sections JOIN section_heads ON event_sections.section = section_heads.section
    );

CREATE VIEW valid_sessions AS
    SELECT *
    FROM sessions
    WHERE now() < last_touched + (SELECT signin_length FROM meta);
