DROP TRIGGER IF EXISTS remove_partial_requests_on_inactive_events_trigger ON events;
DROP FUNCTION IF EXISTS remove_partial_requests_on_inactive_events;

DROP TRIGGER IF EXISTS drop_empty_email_user_after_request_del ON requests;
DROP FUNCTION IF EXISTS drop_empty_email_user;

DROP TRIGGER IF EXISTS log_everyone_out_on_meta_update ON meta;
DROP FUNCTION IF EXISTS log_everyone_out;

DROP TRIGGER IF EXISTS fix_management_user_admin_for_trigger ON management_users;
DROP FUNCTION IF EXISTS fix_management_user_admin_for;

DROP TRIGGER IF EXISTS no_management_user_admin_for_if_not_admin_trigger ON management_user_admin_for;
DROP FUNCTION IF EXISTS no_management_user_admin_for_if_not_admin;
