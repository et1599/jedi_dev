FROM docker.io/node:latest AS build

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain stable -y
RUN mkdir /sqlx && /root/.cargo/bin/cargo +stable install --root /sqlx sqlx-cli

WORKDIR /app
COPY management/package*.json management/

WORKDIR management
RUN npm ci

COPY management/ .
RUN npm run build

WORKDIR /app
COPY . /app/
RUN /root/.cargo/bin/cargo build --release


FROM docker.io/debian:stable-slim as run
RUN apt update && apt install -y libssl3 ca-certificates && rm -rf /var/lib/apt/lists/*

COPY --from=build /sqlx/bin/sqlx /usr/local/bin/sqlx

WORKDIR /app
COPY --from=build /app/target/release/jedi .

WORKDIR /app/migrations
COPY --from=build /app/migrations .

WORKDIR /app/templates
COPY --from=build /app/templates .

WORKDIR /app/static
COPY --from=build /app/static .

WORKDIR /app/management/build
COPY --from=build /app/management/build .

WORKDIR /app

USER www-data
EXPOSE 8080

CMD ["/app/jedi"]
