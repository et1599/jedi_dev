use darling::{FromDeriveInput, FromField};
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Ident};

#[derive(FromDeriveInput, Default)]
#[darling(default, attributes(macrow))]
struct TyOpts {
    try_from: Option<syn::Path>,
}

#[derive(FromField, Default)]
#[darling(default, attributes(macrow))]
struct FieldOpts {
    context: bool,
    flatten: bool,
    try_from: bool,
}

fn path_from_type(ty: &syn::Type) -> Option<&syn::Path> {
    match ty {
        &syn::Type::Group(ref group) => path_from_type(&group.elem),
        &syn::Type::Paren(ref paren) => path_from_type(&paren.elem),
        &syn::Type::Path(ref path) => Some(&path.path),
        _ => None,
    }
}

fn base_path(path: &syn::Path) -> syn::Path {
    let mut path = path.clone();
    path.segments.pop();
    path
}

fn map_ident(ident: &Ident, map: impl FnOnce(String) -> String) -> Ident {
    syn::Ident::new(&map(ident.to_string()), ident.span())
}

#[proc_macro_derive(Macrow, attributes(macrow))]
pub fn derive_macrow(input: TokenStream) -> TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    let ty_opts = TyOpts::from_derive_input(&input).expect("Wrong options");
    let ident = input.ident;
    let make_macro_ident = |ident| map_ident(ident, |x| format!("{x}__macro"));
    let macro_ident = make_macro_ident(&ident);
    let vis = match input.vis {
        syn::Visibility::Public(_) => quote! { pub(crate) },
        restricted => quote! { #restricted },
    };

    let try_from = quote! { ::std::convert::TryFrom::try_from };

    let inner = match ty_opts.try_from {
        Some(other) => {
            let path = base_path(&other);
            quote! { #try_from(#other!(@$($prefix)*, [#path], $input; $context))? }
        },
        None => {
            if let syn::Data::Struct(struct_) = input.data {
                let fields = struct_.fields.iter().map(|field| {
                let opts = FieldOpts::from_field(field).expect("Wrong field options");
                let field_ident = field.ident.as_ref().expect("Doesn't work on anonymous structs");

                let mutually_exclusive = [opts.context, opts.flatten, opts.try_from];
                if mutually_exclusive.into_iter().filter(|x| *x).count() > 1 {
                    panic!("context, flatten & try_from are all mutually exclusive");
                }

                let inner = if opts.context {
                    quote!{ $context.#field_ident }
                } else if opts.flatten {
                    let field_macro = path_from_type(&field.ty).unwrap();
                    let path = base_path(&field_macro);
                    let ext_field_ident = map_ident(field_ident, |x| format!("{x}_"));
                    quote!{ #field_macro!(@$($prefix)* #ext_field_ident, [#path], $input; $context) }
                } else {
                    let inner = quote!{ ::paste::paste!($input.[<$($prefix)* #field_ident>]) };

                    if opts.try_from {
                        quote!{ #try_from(#inner)? }
                    } else {
                        inner
                    }
                };

                quote!{ #field_ident: #inner }
            });
                quote! { $($path::)* #ident { #(#fields,)* } }
            } else {
                panic!("Can only be derived on `struct`s unless `try_from` is used");
            }
        },
    };

    quote! {
        macro_rules! #macro_ident {
            (@$($prefix:ident)*, [$($path:tt ::)*], $input:ident; $context:ident) => {
                #inner
            };
            ($input:ident$(; $($context:ident = $value:expr),+ $(,)?)?) => {
                (|| -> Result<#ident, ::sqlx::Error> {
                    $(
                        #[allow(non_camel_case_types)]
                        struct Context<$($context),+> {
                            $($context: $context,)+
                        }

                        $(let $context = $value;)*
                        let context = Context { $($context,)* };
                    )?

                    Ok(#ident!(@, [], $input; context))
                })()
            }
        }

        #vis use #macro_ident as #ident;
    }
    .into()
}
