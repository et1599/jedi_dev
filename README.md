# Jedi

The »JDAV-Elektronischen-Delegierten-Informationsystem (kurz JEDI)«.

## Production Build

### Start Necessary Services

The docker compose project in the root of the repo starts a postgres and a stump smtp server, both necessary for the build.

```bash
docker compose up -d
```

### Fill DB with Schema

On first build or after deleting the postgres data volume, it is necessary to fill the db with the schema.

Therefor build sqlx-cli and run the migrations:

```bash
docker build -f sqlx-cli-Dockerfile -t sqlx-cli .
docker run --rm -it -v ${PWD}/migrations:/migrations --network=host --env-file .env sqlx-cli /migrations/setup_db.sh
```

#### Reset DB

```bash
docker compose down
docker volume rm building-jedi_database_data
docker compose up -d
```

and execute the setup_db.sh as above

### Build Docker Container

```bash
docker build --network=host -t jedi .
```

### Push to Registry

**Note: This maybe subject to change, see [Issue #141](https://gitlab.com/jdav-de/jedi/jedi/-/issues/141)**

Login to the registry with suitable credentials:

```bash
docker login registry.gitlab.com
```

Check if you don't have any local changes and are on a suitable branch for the docker tag!

Build and tag image:

```bash
docker build --network=host -t registry.gitlab.com/jdav-de/jedi/jedi:latest .
```

Push the image:

```bash
docker push registry.gitlab.com/jdav-de/jedi/jedi:latest
```

### Execute

#### Testing Deployment with services from build process

This uses the services running with `--network=host` as started in the build process.

For configuration see the appropriate section below.

```bash
docker run --rm --name jedi --network=host -v $PWD/.env:/app/.env jedi
```

The service is then available at [http://0.0.0.0:8080](http://0.0.0.0:8080).

#### Testing Deployment with separate services

Go to the `deployment` directory.

Configure `.env` as needed.

```bash
# start database and mailserver
docker compose -f compose-development.yaml up -d

# start jedi
docker run --rm --name jedi --network=jedi-development_db -v $PWD/.env:/app/.env jedi

# to stop the database and mailserver
docker compose -f compose-development.yaml down
```

#### Preview Deployment

Go to the `deployment` directory.

Configure `.env` similar to the production deployment below.
Except for: `SMTP_URL=smtp://mailserver:1025`

The setup for preview isn't fully configured.
You also have to adapt the Caddy config to be able to reach mailpit and possibly other things.

```bash
# start
docker compose -f compose-preview.yaml up -d

# stop
docker compose -f compose-preview.yaml down
```

#### Production Deployment

Clone this repo and go to the `deployment` directory (or download the `deployment` directory separately).

Copy `example.env` to `.env` and adjust the values:

1. generate a secure random password for the postgresql database and set it as `POSTGRES_PASSWORD`
2. set the `SMTP_URL` to match you email provider
    * usually something like `SMTP_URL=smtps://username:password@smtpserver.example.com:465`
3. set `EMAILS_FROM` to match your email setup
4. set `HOST` to the domain name at which the server is reachable

The default config will start a Caddy reverse proxy with automatic TLS certs for `HOST`.
This is fine for most setups with only jedi on the server.
If a different setup is needed, change the `Caddyfile` or remove caddy altogether from the `compose.yaml` (and setup `app` to expose port `8080` to whatever needed).

Before the first start, the DB schema has to be applied:

```bash
docker compose up database -d
docker compose run app /app/migrations/setup_db.sh
```

To start the services:

```bash
docker compose up -d
```

jedi should be reachable at `https://HOST/`.


To stop, simply stop the compose-project:

```bash
docker compose down
```

##### Backup

The only persistent data is the postgresql database.
Use a suitable way to backup it.

##### Update

TODO, see #141

## Configuration

Both environment variables and the `.env` file are read, while the former take precedence.

However, currently it doesn't like a missing `.env` file, see [Issue #134](https://gitlab.com/jdav-de/jedi/jedi/-/issues/134) for further info.

Important options:

* `DATABASE_URL=postgres://postgres:password@127.0.0.1/jedi` The database URL
* `SMTP_URL=smtp://127.0.0.1:1025` The smtp URL
* `EMAILS_FROM="The Sith <sith@jedihome.com>"` (quoted, see above #134) The FROM field for the mail
* `HOST=localhost` The host part of links (in the production setup also used for the reverse proxy)
    * In debug builds, if not specified, defaults to `localhost:<bind-port>`.
* `SCHEME=http` The scheme used for links
    * In debug builds, if not specified, defaults to `http`.
* `BIND_ADDR=0.0.0.0:8080`
    * If not specified, defaults to `0.0.0.0:80` when run as root and `0.0.0.0:8080` when not.

For all, see [`main.rs`](https://gitlab.com/jdav-de/jedi/jedi/-/blob/d61dd09233666a01354c376673df7bcff8ec4ef1/src/main.rs#L153-180) in its current version.

May be refactored to clap, see [Issue #139](https://gitlab.com/jdav-de/jedi/jedi/-/issues/139).

## Development Setup

As this project uses PostreSQL as its database, you'll need some sort of it on the dev machine. There are ways to run it through docker  or you can install it natively.

A SMTP server is also necessary for building and testing. [mailpit](https://github.com/axllent/mailpit) is a easy testing server. 

A docker-compose config for PostreSQL and mailpit is described above in [Start necessary services](#start-necessary-services) and [Fill DB with Schema](#fill-db-with-schema).

The backend uses Rust, so Rust's package manager cargo will be needed. It is installed the easiest via [rustup](https://rustup.rs/).

The web frontend consists of
- templates rendered by the backend (for "unauthorized" pages)
- a [Svelte](https://svelte.dev/) app (for everything management related)

The templates (located in [./templates/]()) are built with basic html and [bootstrap](https://getbootstrap.com/). The goal is not to use javascript in here to reduce the pages footprint and also be more accessible for users who block javascript in their browsers.

The Svelte app (located in [./management/src/]()) also uses bootstrap for the UI. Svelte is meant to be "compiled" on the development machine before shipping. To aid with that, one could _either_ use the [`update_frontend`](./update_frontend) script and something like `inotifywatch` or [entr](https://github.com/eradman/entr/) _or_ install cargo watch (`cargo install watch`) and run `cargo watch -c -C management -- npm run build` from the project's root. Those will recompile the management frontend every time something in the project's folder changes.

So a possible way to get started is:

1. Install PostgreSQL, mailpit, cargo and npm (for Svelte)
2. Start PostgreSQL and mailpit, if necessary adjust `.env`.
3. Either compile the Svelte app manually beforehand or use one of the autocompile ways mentioned above
4. Fill the DB with the Schema: `sqlx db create` and `sqlx migrate run`
5. Start the Backend server with `cargo run` from the project's root.
6. The site is now reachable from [localhost:8080](localhost:8080).

The first User has [currently](https://gitlab.com/jdav-de/jedi/jedi/-/blob/main/migrations/20230521030508_initial.up.sql?ref_type=heads#L10-15) the credentials e-mail: `admin@change.this` and password: `change this`. See [Issue #138](https://gitlab.com/jdav-de/jedi/jedi/-/issues/138) for further changes.

## License
This project is fully licensed under the GNU GPL version 3, see the [LICENSE](https://gitlab.com/jdav-de/jedi/jedi/-/blob/main/LICENSE) file.

We also include Bootstrap & Bootstrap Icons (which are both licensed under MIT) in /static, the license text can be found there in the appropriate subdirectories ([for Bootstrap](https://gitlab.com/jdav-de/jedi/jedi/-/tree/main/static/bootstrap-5.3.3/LICENSE) and [for Bootstrap Icons](https://gitlab.com/jdav-de/jedi/jedi/-/tree/main/static/bootstrap-icons-1.11.3/LICENSE)).

[Macrow](https://gitlab.com/jdav-de/jedi/jedi/-/tree/main/macrow), while developed for this project, is licensed under [CC0](https://gitlab.com/jdav-de/jedi/jedi/-/blob/main/macrow/LICENSE.CC0) or [MIT](https://gitlab.com/jdav-de/jedi/jedi/-/blob/main/macrow/LICENSE.MIT).
