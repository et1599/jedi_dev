// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.
import type { Form } from "./Form";
import type { FullEventState } from "./FullEventState";
import type { PartialUserOverview } from "./PartialUserOverview";
import type { RenderedMarkdown } from "./RenderedMarkdown";
import type { SectionOverview } from "./SectionOverview";
import type { UserOverview } from "./UserOverview";

export interface Event {
  id: string;
  name: string;
  description: RenderedMarkdown;
  state: FullEventState;
  organizers: Array<UserOverview>;
  partial_organizers: Array<PartialUserOverview>;
  form: Form;
  can_users_leave: boolean;
  can_users_edit_name: boolean;
  sections: Array<SectionOverview>;
  max_total_delegates: number;
}
