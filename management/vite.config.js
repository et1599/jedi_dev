import { sveltekit } from "@sveltejs/kit/vite";

/** @type {import('vite').UserConfig} */
const config = {
  build: {
    sourcemap: true, // Source map generation must be turned on
  },
  plugins: [sveltekit()],
};

export default config;
