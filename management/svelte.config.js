import adapter from "@sveltejs/adapter-static";
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://kit.svelte.dev/docs/integrations#preprocessors
  // for more information about preprocessors
  preprocess: vitePreprocess(),
  compilerOptions: {
    enableSourcemap: true,
  },
  kit: {
    adapter: adapter({
      fallback: "fallback.html",
    }),
    paths: {
      base: "/manage/app",
    },
  },
};

export default config;
