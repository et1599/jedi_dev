import { toast } from "@zerodevx/svelte-toast";
import type { UserOverview } from "$lib/bindings/UserOverview";
import type { EventId } from "$lib/bindings/EventId";
import type { Event } from "$lib/bindings/Event";
import type { EventOverview } from "$lib/bindings/EventOverview";
import type { CreateEvent } from "$lib/bindings/CreateEvent";
import type { EventDiff } from "$lib/bindings/EventDiff";
import type { PartialUserOverview } from "$lib/bindings/PartialUserOverview";
import type { UserId } from "$lib/bindings/UserId";
import type { CreateUser } from "$lib/bindings/CreateUser";
import type { User } from "$lib/bindings/User";
import type { UserDiff } from "$lib/bindings/UserDiff";
import type { SectionId } from "$lib/bindings/SectionId";
import type { EventSection } from "$lib/bindings/EventSection";
import type { EventSectionDiff } from "$lib/bindings/EventSectionDiff";
import type { Section } from "$lib/bindings/Section";
import type { SectionPut } from "$lib/bindings/SectionPut";
import type { SectionDiff } from "$lib/bindings/SectionDiff";
import type { SectionHeadsRow } from "$lib/bindings/SectionHeadsRow";
import type { EventSectionInfo } from "$lib/bindings/EventSectionInfo";
import type { ConfigDiff } from "$lib/bindings/ConfigDiff";
import { until_logged_in } from "$lib/stores";
import { get } from "svelte/store";
import { assert } from "$lib/utils";

export const api_base: string = "/api/v1/";

export type UriIsh = string | string[];

function urlBuilder(input: UriIsh): string {
  if (typeof input == "string") {
    return api_base + input;
  } else {
    return api_base + input.join("/");
  }
}

export const eventExport = (id: string) =>
  urlBuilder(["event", id, "all", "export"]);

export const response_messages: { [key: string]: string } = {
  "doesnt-exist": "Die angeforderte Ressource existiert nicht (mehr).",
  "organizer-updates": "Änderungen der Organisatoren war nicht möglich.",
  "request-doesnt-exist": "Anmeldung wurde inzwischen schon entfernt.",
  "event-section-doesnt-exist":
    "Zielsektion existiert nicht (mehr) für die Veranstaltung.",
  "invalid-body": "Ungültige Eingaben.",
  "invalid-query": "Ungültige Eingaben.",
  "email-exists-already": "Die Email-Adresse ist bereits in Verwendung.",
  "not-allowed": "Diese Operation ist nicht erlaubt.",
  "not-logged-in": "Nicht mehr eingeloggt.", // TODO: Handle this more gracefully, see #178
  "event-being-set-up": "Veranstaltung wird noch vorbereitet.",
  "event-active-but-signup-done":
    "Anmeldefrist für die Veranstaltung ist vorüber.",
  "event-already-active": "Veranstaltung ist schon aktiv.",
  "event-already-done": "Veranstaltung ist schon archiviert.",
  "decisions-before-signup":
    "Die Anmeldefrist darf nicht nach der Entscheidungsfrist sein.",
  "event-state-dates-not-future":
    "Mindestens ein angegebenes Datum liegt nicht in der Zukunft.",
  "section-guest-change":
    "Die Sektion kann nicht in ihrem Gaststatus geändert werden.",
};

export const toast_themes: { [key: string]: { [prop: string]: any } } = {
  success: {
    "--toastBarHeight": 0,
    "--toastBackground": "rgba(72,187,120,0.9)",
  },
  error: {
    "--toastBarHeight": 0,
    "--toastBackground": "rgba(237,30,14,0.93)",
  },
};

export function responseToast(ok: boolean, content: string): void {
  toast.push(
    `<strong>${ok ? "Erfolgreich" : "Fehlgeschlagen"}</strong><br>${content}`,
    {
      theme: ok ? toast_themes.success : toast_themes.error,
    },
  );
}

type HttpMethod = "GET" | "POST" | "PUT" | "DELETE" | "PATCH";

class NotLoggedInError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = "NotLoggedInError";
  }
}

async function doRequest(
  method: HttpMethod,
  url: UriIsh,
  body: string,
  success_toast?: string,
  no_error_toast: boolean = false,
  mimetype: string | null = null,
  retry_on_logged_out: boolean = true,
): Promise<any> {
  let requestContentType: string | undefined =
    mimetype == null ? (body == "" ? undefined : "application/json") : mimetype;
  let response: Response;
  while (true) {
    let until_logged_in_promise = get(until_logged_in);
    if (until_logged_in_promise == null) {
      response = await fetch(urlBuilder(url), {
        method,
        body: ["GET", "DELETE"].includes(method) ? undefined : body,
        headers:
          requestContentType == undefined
            ? undefined
            : {
                "Content-Type": requestContentType,
              },
      });
      if (response.status != 401) {
        break;
      }
      if (get(until_logged_in) == null) {
        let resolve: ((_: null) => void) | null = null;
        let promise = new Promise((res, _) => (resolve = res));
        assert(resolve !== null);
        until_logged_in.set({ resolve, promise: promise as Promise<null> });
        if (!retry_on_logged_out) {
          throw new NotLoggedInError();
        }
        await promise;
      }
      continue;
    } else if (!retry_on_logged_out) {
      throw new NotLoggedInError();
    } else {
      await until_logged_in_promise.promise;
    }
  }
  const contentType = response.headers.get("content-type");
  const isJson = contentType && contentType.indexOf("application/json") !== -1;
  if (!response.ok) {
    let msg;
    let json;
    if (isJson) {
      json = await response.json();
      const type = json.type;
      if (type in response_messages) {
        msg = response_messages[type];
      } else {
        msg = type;
      }
    } else if (response.status == 422) {
      // unprocessable entity
      msg = "Fehlerhafte Daten";
    } else {
      msg = "Unbekannter Fehler";
    }
    if (!no_error_toast) {
      responseToast(false, msg);
    }
    if (isJson) {
      throw json;
    } else {
      throw await response.text();
    }
  }
  if (success_toast !== null && success_toast !== undefined) {
    responseToast(true, success_toast);
  }
  if (isJson) {
    return await response.json();
  } else {
    return await response.text();
  }
}

export async function getRequest<T>(
  url: UriIsh,
  success_toast?: string,
  no_error_toast: boolean = false,
): Promise<T> {
  return await doRequest("GET", url, "", success_toast, no_error_toast);
}

export async function postRequest<T>(
  url: UriIsh,
  body: string = "",
  success_toast?: string,
  no_error_toast: boolean = false,
  mimetype: string | null = null,
): Promise<T> {
  return await doRequest(
    "POST",
    url,
    body,
    success_toast,
    no_error_toast,
    mimetype,
  );
}

export async function patchRequest<T>(
  url: UriIsh,
  body: string = "",
  success_toast?: string,
  no_error_toast: boolean = false,
): Promise<T> {
  return await doRequest("PATCH", url, body, success_toast, no_error_toast);
}

export async function putRequest<T>(
  url: UriIsh,
  body: string = "",
  success_toast?: string,
  no_error_toast: boolean = false,
): Promise<T> {
  return await doRequest("PUT", url, body, success_toast, no_error_toast);
}

export async function deleteRequest<T>(
  url: UriIsh,
  success_toast?: string,
  no_error_toast: boolean = false,
): Promise<T> {
  return await doRequest("DELETE", url, "", success_toast, no_error_toast);
}

// Returns all other users viewing this page, if any
export async function pushView(uri: string | null): Promise<string[]> {
  return (await postRequest(
    "push-view",
    JSON.stringify({ uri }),
    undefined,
    true,
  )) as string[];
}

export async function getEvents(): Promise<EventOverview[]> {
  return (await getRequest("events")) as EventOverview[];
}

export async function postEvent(create_event: CreateEvent): Promise<EventId> {
  return (await postRequest(
    "event",
    JSON.stringify(create_event),
    "Veranstaltung angelegt",
  )) as EventId;
}

export async function getEvent(event_id: EventId): Promise<Event> {
  return (await getRequest(["event", event_id])) as Event;
}

export async function getEventSectionInfos(
  event_id: EventId,
): Promise<EventSectionInfo[]> {
  return (await getRequest([
    "event",
    event_id,
    "all",
    "info",
  ])) as EventSectionInfo[];
}

export async function eventNotifyHeads(event_id: EventId): Promise<void> {
  return await postRequest(["event", event_id, "all", "notify-heads"]);
}

export async function patchEvent(
  event_id: EventId,
  event_diff: EventDiff,
  toast_text: string = "Änderungen gespeichert",
): Promise<void> {
  return await patchRequest(
    ["event", event_id],
    JSON.stringify(event_diff),
    toast_text,
  );
}

export async function deleteEvent(event_id: EventId): Promise<void> {
  return await deleteRequest(["event", event_id], "Veranstaltung gelöscht");
}

export async function getEventExport(event_id: EventId): Promise<void> {
  return await getRequest(["event", event_id, "all", "export"]);
}

export async function getUsers(): Promise<UserOverview[]> {
  return (await getRequest("users")) as UserOverview[];
}

export async function getPartialUsers(): Promise<PartialUserOverview> {
  return (await getRequest("partial-users")) as PartialUserOverview;
}

export async function postUserResetPassword(user_id: UserId): Promise<void> {
  return await postRequest(
    ["user", user_id, "reset-password"],
    "",
    "Passwort zurückgesetzt",
  );
}

export async function postUserMassResetPassword(): Promise<void> {
  return await postRequest(
    ["user", "mass-reset-password"],
    "",
    "Passwörter zurückgesetzt",
  );
}

export async function postUser(create_user: CreateUser): Promise<UserId> {
  return (await postRequest(
    "user",
    JSON.stringify(create_user),
    "Benutzer angelegt",
  )) as UserId;
}

export async function getUserSelf(): Promise<User> {
  return (await getRequest(["user", "self"])) as User;
}

export async function getUser(user_id: UserId): Promise<UserOverview> {
  return (await getRequest(["user", user_id])) as UserOverview;
}

export async function patchUserSelf(user_diff: UserDiff): Promise<void> {
  return await patchRequest(
    ["user", "self"],
    JSON.stringify(user_diff),
    "Änderungen gespeichert",
  );
}

export async function patchUser(
  user_id: UserId,
  user_diff: UserDiff,
): Promise<void> {
  return await patchRequest(
    ["user", user_id],
    JSON.stringify(user_diff),
    "Änderungen gespeichert",
  );
}

export async function deleteUserSelf(): Promise<void> {
  return await deleteRequest(["user", "self"], "Benutzer gelöscht");
}

export async function deleteUser(user_id: UserId): Promise<void> {
  return await deleteRequest(["user", user_id], "Benutzer gelöscht");
}

export async function getEventSection(
  event_id: EventId,
  section_id: SectionId,
): Promise<EventSection> {
  return (await getRequest(["event", event_id, section_id])) as EventSection;
}

export async function patchEventSection(
  event_id: EventId,
  section_id: SectionId,
  event_section_diff: EventSectionDiff,
  show_toast: boolean = true,
  toast_text: string = "Änderungen gespeichert",
): Promise<void> {
  return await patchRequest(
    ["event", event_id, section_id],
    JSON.stringify(event_section_diff),
    show_toast ? toast_text : undefined,
  );
}

export async function getSections(): Promise<Section[]> {
  return (await getRequest("sections")) as Section[];
}

export async function getSection(section_id: SectionId): Promise<Section> {
  return (await getRequest(["section", section_id])) as Section;
}

export async function putSection(
  section_id: SectionId,
  section_put: SectionPut,
  show_toast: boolean = true,
  toast_text: string = "Sektion angelegt",
): Promise<void> {
  return await putRequest(
    ["section", section_id],
    JSON.stringify(section_put),
    show_toast ? toast_text : undefined,
  );
}

export async function patchSection(
  section_id: SectionId,
  section_diff: SectionDiff,
  show_toast: boolean = true,
  toast_text: string = "Änderungen gespeichert",
): Promise<void> {
  return await patchRequest(
    ["section", section_id],
    JSON.stringify(section_diff),
    show_toast ? toast_text : undefined,
  );
}

export async function deleteSection(
  section_id: SectionId,
  show_toast: boolean = true,
  toast_text: string = "Sektion gelöscht",
): Promise<void> {
  return await deleteRequest(
    ["section", section_id],
    show_toast ? toast_text : undefined,
  );
}

export async function getSectionHeads(): Promise<SectionHeadsRow[]> {
  return await getRequest("section-heads");
}

export async function patchConfig(config_diff: ConfigDiff) {
  return await patchRequest("config", JSON.stringify(config_diff));
}

export async function login(
  email: string,
  password: string,
): Promise<Response> {
  return fetch("/manage/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    },
    body: new URLSearchParams({
      email: email,
      password: password,
    }),
    redirect: "manual",
  });
}
