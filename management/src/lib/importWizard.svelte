<script lang="ts" context="module">
  // NOTE: Typing in this component is on a best-effort basis, and sadly does not cover everything
  // satisfactorily; though it should be "typesafe enough" for most incorrect usages to get the type
  // checker to scream.
  type BaseWizardField<K, T = string> = {
    key: K;
    label: string;
    required?: boolean;
    optional_for_adding?: boolean;
    compare?: (left: T, right: T) => boolean;
  } & (
    | {
        multiple_values?: false;
        map: (field: string) => T;
      }
    | (T extends string ? { multiple_values?: false } : never)
    | {
        multiple_values: true;
        map: (field: string[]) => T;
      }
    | (T extends string[] ? { multiple_values: true } : never)
  );

  type WizardFieldIter<T extends object, K extends keyof T> = K extends keyof T
    ? BaseWizardField<K, T[K]>
    : never;

  export type WizardField<T extends object> = Readonly<
    WizardFieldIter<T, keyof T>
  >;

  type RequiredPart<T extends object, W extends WizardField<T>> = W extends {
    required: true;
  }
    ? W["key"]
    : never;
  export type Output<
    T extends object,
    W extends readonly WizardField<T>[],
  > = Partial<Pick<T, W[number]["key"]>> & Pick<T, RequiredPart<T, W[number]>>;

  export type Mapping<
    T extends object,
    W extends readonly WizardField<T>[],
  > = Pick<{ [key in keyof T]: string[] }, W[number]["key"]>;

  export type MapError<
    T extends object,
    W extends readonly WizardField<T>[],
  > = {
    error: unknown;
    column: W[number]["key"];
    row: { [k: string]: string };
    row_number: number;
    excel_row_number: number;
    mapping: Mapping<T, W>;
  };

  export type TabType = "changed" | "unchanged" | "added" | "removed";
  export type SublistsType<
    T extends object,
    W extends readonly WizardField<T>[],
  > = { [tab in TabType]: Output<T, W>[] };
</script>

<script
  lang="ts"
  generics="ComponentT extends object, ComponentFields extends readonly WizardField<ComponentT>[]"
>
  import * as XLSX from "xlsx";
  import ProgressSteps from "$lib/progressSteps.svelte";
  import { assert, deepEq, objectEntries } from "$lib/utils";
  import { createEventDispatcher } from "svelte";
  import { fade, fly } from "svelte/transition";
  import { quintOut } from "svelte/easing";

  export const _deduce_type_because_svelte_doesnt_allow_explicit_types: ComponentT =
    undefined as any;

  type Key = ComponentFields[number]["key"];
  type Out = Output<ComponentT, ComponentFields>;
  type Err = MapError<ComponentT, ComponentFields>;

  interface $$Slots {
    mapping_config: {};
    changed: {};
    unchanged: {};
    added: {};
    removed: {};
    column: {
      column: Key;
      data: Out[Key];
    };
  }

  export let old: readonly Out[];
  export let old_by_id: { readonly [key: string]: Out };
  export let fields: ComponentFields;
  export let id_getter:
    | ((x: Out) => string)
    | (ComponentT extends { id: string } ? null : never) = null as any;
  export let restructure_hook: (
    restructured: Out[],
    extra_mappings: Key[],
  ) => Out[] = (restructured, extra) => restructured;
  export let map_errors: Err[] = [];
  export let disable_continue: boolean = false;
  export let change_successful: string[] = []; // array of IDs
  export let change_failed: string[] = []; // array of IDs
  export let enable_tabs: TabType[] = [
    "changed",
    "unchanged",
    "added",
    "removed",
  ];

  function actual_id_getter(x: Out): string {
    if (id_getter == null) {
      if ("id" in x && typeof x.id == "string") {
        return x.id;
      } else {
        return "";
      }
    } else {
      return id_getter(x);
    }
  }

  let empty_mapping: Mapping<ComponentT, ComponentFields>;
  $: empty_mapping = Object.fromEntries(
    field_order.map((key) => [key, [] as string[]]),
  ) as Mapping<ComponentT, ComponentFields>;

  export let mapping: Mapping<ComponentT, ComponentFields> = undefined as any;
  $: mapping = structuredClone(empty_mapping);

  export let tab_label_overrides: Partial<{ [key in TabType]: string }> = {};

  let header: string[] | null = null;
  let parsed: { [key: string]: string }[] | null = null;
  let restructured: Out[] | null = null;
  let wizard_step: 1 | 2 | 3 = 1;
  let current_tab: TabType = "changed";
  let restructured_sublists: { [tab in TabType]: Out[] | undefined };
  let cached_old: readonly Out[] = structuredClone(old);
  let cached_old_by_id: { readonly [key: string]: Out } =
    structuredClone(old_by_id);
  let modalOpen: boolean = false;

  $: {
    if (wizard_step !== 3) {
      cached_old = structuredClone(old);
      cached_old_by_id = structuredClone(old_by_id);
    }
  }

  type TabData = {
    tab: TabType;
    label: string;
  };

  const tabs_base: TabData[] = [
    { tab: "changed", label: "geändert" },
    { tab: "unchanged", label: "ungeändert" },
    { tab: "added", label: "neu hinzugefügt" },
    { tab: "removed", label: "entfernt" },
  ] as const;

  let tabs: TabData[];
  $: tabs = tabs_base
    .filter((tab) => enable_tabs.includes(tab.tab))
    .map((tab) => ({
      tab: tab.tab,
      label: tab_label_overrides[tab.tab] ?? tab.label,
    }));

  type TabState = {
    checked: boolean;
    done: boolean;
  };

  let tab_states: { [key in TabType]: TabState } = Object.fromEntries(
    tabs_base.map((tab): [TabType, TabState] => [
      tab.tab,
      { checked: false, done: false },
    ]),
  ) as { [key in TabType]: TabState };

  const wizard_steps: { disabled: boolean; function: () => void }[] = [
    {
      disabled: false,
      function: () => (wizard_step = 1),
    },
    {
      disabled: false,
      function: () => (wizard_step = 2),
    },
    {
      disabled: false,
      function: () => (wizard_step = 3),
    },
  ];

  type BaseCompleteWizardField<K, T = string> = {
    key: K;
    label: string;
    required: boolean;
    optional_for_adding: boolean;
    compare: (left: T, right: T) => boolean;
  } & (
    | {
        multiple_values: false;
        map: (field: string) => T;
      }
    | {
        multiple_values: true;
        map: (field: string[]) => T;
      }
  );

  type CompleteWizardFields = {
    [key in Key]: BaseCompleteWizardField<key, ComponentT[key]>;
  };

  function complete_fields(
    fields: ComponentFields,
  ): [CompleteWizardFields, Key[]] {
    return [
      Object.fromEntries(
        fields.map((value) => {
          const optional = (name: string, default_: any) =>
            Object.hasOwn(value, name)
              ? value[name as keyof typeof value]
              : default_;
          return [
            value.key,
            {
              label: value.label,
              required: optional("required", false),
              optional_for_adding: optional("optional_for_adding", false),
              multiple_values: optional("multiple_values", false),
              map: optional("map", (x: any) => x),
              compare: optional("compare", deepEq),
            },
          ];
        }),
      ) as CompleteWizardFields,
      fields.map((value) => value.key as Key),
    ];
  }

  const dispatch = createEventDispatcher<{
    applyChanges: SublistsType<ComponentT, ComponentFields>;
  }>();

  let fields_by_key: CompleteWizardFields;
  let field_order: Key[];
  $: [fields_by_key, field_order] = complete_fields(fields);

  let missing_fields: Key[];
  $: missing_fields = field_order
    .filter((key) => fields_by_key[key].required)
    .filter((key) => mapping[key].length == 0);

  function humanJoin(arr: string[], comma: string, and: string): string {
    if (arr.length == 0) {
      return "";
    } else if (arr.length == 1) {
      return arr[0];
    } else {
      return (
        arr.slice(0, arr.length - 1).join(comma) + and + arr[arr.length - 1]
      );
    }
  }

  function toggle_field(key: Key, csv_field: string): void {
    if (mapping[key].includes(csv_field)) {
      // remove
      mapping[key] = mapping[key].filter((x) => x !== csv_field);
    } else {
      // add
      for (let i of Object.keys(mapping) as (keyof typeof mapping)[]) {
        mapping[i] = mapping[i].filter((x: string) => x !== csv_field);
      }
      if (!fields_by_key[key].multiple_values && mapping[key].length > 0) {
        // replace
        mapping[key] = [csv_field];
      } else {
        mapping[key] = [...mapping[key], csv_field];
      }
    }
  }

  function assigned_field(csv_field: string): CompleteWizardFields[Key] | null {
    const out: Key = objectEntries(mapping)
      .filter(([key, value]) => value.includes(csv_field))
      .map(([key, value]) => key)[0];
    if (out !== undefined) {
      return fields_by_key[out];
    } else {
      return null;
    }
  }

  function stringOrEmpty(x: unknown): string {
    if (x === null || x === undefined) {
      return "";
    } else {
      return x.toString();
    }
  }

  $: switch (wizard_step) {
    case 1:
      wizard_steps[1].disabled = parsed === null || disable_continue;
      break;
    case 2:
      wizard_steps[2].disabled = missing_fields.length != 0 || disable_continue;
      break;
    default:
  }

  let extra_mappings: Key[] = [];

  $: if (wizard_step == 3) {
    // if we switch to step 3
    assert(parsed !== null);
    extra_mappings = [];
    let local_map_errors: Err[] = [];
    restructured = restructure_hook(
      parsed
        .map((raw, row_number) => {
          const mapped: [Key, ComponentT[Key]][] = field_order
            .filter((key: Key) => mapping[key].length > 0)
            .map((key: Key): [Key, ComponentT[Key]] | null => {
              const field: BaseCompleteWizardField<Key, ComponentT[Key]> =
                fields_by_key[key];

              let raw_value: string[] = mapping[key].map((csv_field) =>
                stringOrEmpty(raw[csv_field]),
              );

              let value: ComponentT[Key];
              try {
                if (field.multiple_values) {
                  value = field.map(raw_value);
                } else {
                  value = field.map(raw_value[0]);
                }
              } catch (e) {
                local_map_errors.push({
                  error: e,
                  column: key,
                  row: raw,
                  row_number: row_number,
                  excel_row_number: raw.__rowNum__ as unknown as number,
                  mapping: mapping,
                });
                return null;
              }

              return [key, value];
            })
            .filter((i: [Key, ComponentT[Key]] | null) => i !== null) as [
            Key,
            ComponentT[Key],
          ][];

          return Object.fromEntries(mapped) as Out;
        })
        .filter((elem: Out, row_number: number) => {
          let valid = actual_id_getter(elem).toString().length > 0;
          let had_error = local_map_errors.some(
            (i) => i.row_number == row_number,
          );
          if (!valid) {
            local_map_errors = local_map_errors.filter(
              (i) => i.row_number != row_number,
            );
          }
          return valid && !had_error;
        }),
      extra_mappings,
    );
    extra_mappings = extra_mappings;
    map_errors = local_map_errors;
  }

  let included_fields: Key[];
  $: included_fields = field_order
    .filter((key) => mapping[key].length > 0)
    .concat(extra_mappings);

  function optionalMap<T, O>(
    f: (x: T) => O,
    o: T | undefined | null,
  ): O | null {
    return o === null || o === undefined ? null : f(o);
  }

  function subsetEq<T extends object>(
    innerEq: (l: T[keyof T], r: T[keyof T], k: keyof T) => boolean,
    smaller: T,
    bigger: T,
  ): boolean {
    for (const [key, smaller_value] of objectEntries(smaller)) {
      if (smaller_value === undefined) {
        continue;
      } else if (!(key in bigger)) {
        return false;
      } else {
        const bigger_value: T[keyof T] | undefined = bigger[key];
        if (
          bigger_value === undefined ||
          !innerEq(smaller_value, bigger_value, key)
        ) {
          return false;
        }
      }
    }
    return true;
  }

  function compareItems(l: Out, r: Out): boolean {
    return subsetEq(
      (item_l, item_r, key: Key) =>
        fields_by_key[key].compare(
          item_l as ComponentT[Key],
          item_r as ComponentT[Key],
        ),
      l,
      r,
    );
  }

  function compareKey(key: Key, row: Out): boolean {
    return fields_by_key[key].compare(
      cached_old_by_id[actual_id_getter(row)][key] as ComponentT[Key],
      row[key] as ComponentT[Key],
    );
  }

  let last_step: number = 1;

  $: {
    if (wizard_step != last_step) {
      const restructured_by_id: { [key: string]: Out } | null = optionalMap(
        Object.fromEntries,
        restructured?.map((i) => [actual_id_getter(i), i]),
      );
      const existing: Out[] | undefined = restructured?.filter(
        (x) => actual_id_getter(x) in cached_old_by_id,
      );

      restructured_sublists = {
        added: restructured?.filter(
          (x) => !(actual_id_getter(x) in cached_old_by_id),
        ),
        removed:
          optionalMap(
            (restructured_by_id) =>
              cached_old.filter(
                (x) => !(actual_id_getter(x) in restructured_by_id),
              ),
            restructured_by_id,
          ) ?? undefined,
        unchanged: existing?.filter((x) =>
          compareItems(x, cached_old_by_id[actual_id_getter(x)]),
        ),
        changed: existing?.filter(
          (x) => !compareItems(x, cached_old_by_id[actual_id_getter(x)]),
        ),
      };

      if (restructured !== null) {
        current_tab =
          tabs
            .map(({ tab }) => tab)
            .filter(
              (tab) => (restructured_sublists[tab]?.length ?? 0) > 0,
            )[0] ?? "added";
      }
    }

    last_step = wizard_step;
  }

  let missing_fields_for_adding: Key[];
  $: missing_fields_for_adding = field_order.filter(
    (field) =>
      mapping[field].length == 0 && !fields_by_key[field].optional_for_adding,
  );

  async function onFileChange(evt: Event) {
    assert(evt.target instanceof HTMLInputElement && evt.target.files !== null);
    const workbook = XLSX.read(await evt.target.files[0].arrayBuffer(), {
      cellDates: true,
    });
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    header = XLSX.utils
      .sheet_to_json(worksheet, { header: 1 })
      .shift() as string[];
    parsed = XLSX.utils.sheet_to_json(worksheet).map((o) =>
      Object.fromEntries(
        Object.entries(o as object).map(([k, v]: [string, any]) => {
          if (v instanceof Date) {
            return [k, v.toISOString().split("T")[0]];
          } else {
            return [k, v + ""];
          }
        }),
      ),
    );
    mapping = structuredClone(empty_mapping);
  }

  function handleStartClick(): void {
    if (tab_states["removed"].checked) {
      modalOpen = true;
    } else {
      startImport();
    }
  }

  function startImport(): void {
    const sublists: Partial<SublistsType<ComponentT, ComponentFields>> = {};
    tabs.forEach((tab) => {
      let state = tab_states[tab.tab];
      sublists[tab.tab] =
        state.checked && !state.done ? restructured_sublists[tab.tab] : [];
    });
    dispatch(
      "applyChanges",
      sublists as SublistsType<ComponentT, ComponentFields>,
    );
    Object.values(tab_states).forEach(
      (state) => (state.done = state.done || state.checked),
    );
    tab_states = tab_states;
    modalOpen = false;
  }
</script>

<main class="w-50 mx-auto">
  <ProgressSteps bind:current_step={wizard_step} steps={wizard_steps} />
  {#if wizard_step == 1}
    <label class="card card-body d-inline-flex">
      <i class="bi-upload mx-auto" style="font-size: 5rem;" />
      <input type="file" class="form-control" on:change={onFileChange} />
    </label>
    <div class="btn-group mt-3" role="group">
      <button
        type="button"
        class="btn btn-outline-primary"
        on:click={() => (wizard_step -= 1)}
        disabled
      >
        Zurück
      </button>
      <button
        type="button"
        class="btn btn-outline-primary"
        on:click={() => (wizard_step += 1)}
        disabled={wizard_steps[1].disabled}
      >
        Weiter
      </button>
    </div>
  {:else if wizard_step == 2}
    {#if parsed !== null}
      {#if missing_fields.length > 0}
        <div class="alert alert-danger" role="alert">
          {missing_fields.length == 1 ? "Das Feld" : "Die Felder"}
          <b>
            {humanJoin(
              missing_fields.map((key) => fields_by_key[key].label),
              ", ",
              " und ",
            )}
          </b>
          {missing_fields.length == 1 ? "ist" : "sind"} nicht zugewiesen.
        </div>
      {/if}
      <slot name="mapping_config" />
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            {#each header ?? [] as csv_field}
              {@const field = assigned_field(csv_field)}
              {#key mapping}
                <th scope="col">
                  <div class="d-inline-flex align-items-center text-nowrap">
                    <span>{csv_field}</span>
                    <i class="bi-arrow-right mx-1" />
                    <div class="dropdown">
                      <button
                        class="btn btn-sm btn-{field !== null
                          ? 'success'
                          : 'secondary'} dropdown-toggle"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        {field !== null ? field.label : "Feld zuweisen"}
                      </button>
                      <ul class="dropdown-menu">
                        {#each field_order as key}
                          {@const possible_field = fields_by_key[key]}
                          <li>
                            <button
                              type="button"
                              class="dropdown-item"
                              class:bg-danger-subtle={missing_fields.includes(
                                key,
                              )}
                              on:click={() => toggle_field(key, csv_field)}
                            >
                              {#if mapping[key].length > 0}
                                <i class="bi-check me-1" />
                              {/if}
                              <span>{possible_field?.label}</span>
                            </button>
                          </li>
                        {/each}
                      </ul>
                    </div>
                  </div>
                </th>
              {/key}
            {/each}
          </thead>
          <tbody>
            {#each parsed.slice(0, 10) as row}
              <tr>
                {#each header ?? [] as col}
                  <td>{stringOrEmpty(row[col])}</td>
                {/each}
              </tr>
            {/each}
          </tbody>
        </table>
      </div>
    {/if}
    <div class="btn-group mt-3 float-end" role="group">
      <button
        type="button"
        class="btn btn-outline-primary"
        on:click={() => (wizard_step -= 1)}
      >
        Zurück
      </button>
      <button
        type="button"
        class="btn btn-outline-primary"
        on:click={() => (wizard_step += 1)}
        disabled={wizard_steps[2].disabled}
      >
        Weiter
      </button>
    </div>
  {:else if wizard_step == 3}
    {#if !wizard_steps[2].disabled && restructured != null}
      {#if map_errors.length > 0}
        <div class="alert alert-danger" role="alert">
          Es gab ungültige Zeilen:
          <ul>
            {#each map_errors as map_error}
              <li>
                <b>Zeile</b>
                <!--
                  +1 because of zero-indexing
                  +1 because of header size
                -->
                {(map_error.excel_row_number ?? map_error.row_number) + 2},
                <b>Spalte</b>
                "{map_error.mapping[map_error.column]?.[0]}"
                {#if typeof map_error.error == "string"}
                  : {map_error.error}
                {/if}
              </li>
            {/each}
          </ul>
        </div>
      {/if}
      <div class="alert alert-light text-dark" role="alert">
        Änderungen übernehmen aus:
        {#each tabs.filter((t) => t.tab !== "unchanged") as { tab, label }}
          {@const disabled =
            (restructured_sublists[tab]?.length ?? 0) == 0 ||
            (tab == "added" && missing_fields_for_adding.length > 0) ||
            tab_states[tab].done}
          <div class="mb-1 form-check">
            <input
              type="checkbox"
              class="form-check-input"
              id="check-{tab}"
              bind:checked={tab_states[tab].checked}
              {disabled}
            />
            <label class="form-check-label" for="check-{tab}">
              {label}
              {#if tab == "added" && disabled}
                (nicht verfügbar, da {humanJoin(
                  missing_fields_for_adding.map(
                    (f) => fields_by_key[f]?.label ?? "",
                  ),
                  ", ",
                  " und ",
                )} nicht gesetzt sind)
              {/if}
            </label>
          </div>
        {/each}
        <button
          type="button"
          class="btn btn-primary mt-2"
          disabled={!Object.values(tab_states).some(
            (i) => i.checked && !i.done,
          )}
          on:click={handleStartClick}
        >
          Start
        </button>
      </div>
      <ul class="nav nav-tabs">
        {#each tabs as { tab, label }}
          <li class="nav-item">
            <button
              type="button"
              class="nav-link"
              class:active={current_tab == tab}
              on:click={() => (current_tab = tab)}
              disabled={(restructured_sublists[tab]?.length ?? 0) == 0}
              aria-current="page"
            >
              {label}
            </button>
          </li>
        {/each}
      </ul>
      <div>
        {#if restructured.length > 0}
          {#if current_tab == "changed"}
            <slot name="changed" />
          {:else if current_tab == "unchanged"}
            <slot name="unchanged" />
          {:else if current_tab == "added"}
            <slot name="added" />
          {:else if current_tab == "removed"}
            <slot name="removed" />
          {/if}
          <table class="table">
            <thead>
              {#each current_tab == "added" ? included_fields : field_order as key}
                <th scope="col">{fields_by_key[key]?.label}</th>
              {/each}
            </thead>
            <tbody>
              {#each restructured_sublists[current_tab] ?? [] as row}
                <tr
                  class:table-success={change_successful.includes(
                    actual_id_getter(row),
                  )}
                  class:table-danger={change_failed.includes(
                    actual_id_getter(row),
                  )}
                >
                  {#each current_tab == "added" ? included_fields : field_order as key}
                    <td>
                      {#if !included_fields.includes(key)}
                        <slot
                          name="column"
                          column={key}
                          data={cached_old_by_id[actual_id_getter(row)][key]}
                        >
                          {cached_old_by_id[actual_id_getter(row)][key]}
                        </slot>
                      {:else if current_tab == "changed" && !compareKey(key, row)}
                        <slot
                          name="column"
                          column={key}
                          data={cached_old_by_id[actual_id_getter(row)][key]}
                        >
                          {cached_old_by_id[actual_id_getter(row)][key]}
                        </slot>
                        ➡️
                        <slot name="column" column={key} data={row[key]}>
                          {row[key]}
                        </slot>
                      {:else}
                        <slot name="column" column={key} data={row[key]}>
                          {row[key]}
                        </slot>
                      {/if}
                    </td>
                  {/each}
                </tr>
              {/each}
            </tbody>
          </table>
        {:else}
          <h2>Es hat sich nichts geändert.</h2>
        {/if}
      </div>
    {/if}
  {/if}
  {#if modalOpen}
    {@const elements_to_remove = restructured_sublists["removed"]?.length ?? 0}
    <div
      class="modal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="modalLabel"
      aria-hidden={false}
      style="display: block;"
    >
      <div
        class="modal-dialog"
        role="document"
        in:fly={{ y: -50, duration: 300 }}
        out:fly={{ y: -50, duration: 300, easing: quintOut }}
      >
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Wirklich löschen?</h5>
            <button
              type="button"
              class="btn-close"
              data-dismiss="modal"
              aria-label="Close"
              on:click={() => (modalOpen = false)}
            />
          </div>
          <div class="modal-body justify-content-center">
            <p>
              Sie sind im Begriff, {elements_to_remove} Element{elements_to_remove >
              1
                ? "e"
                : ""} zu löschen.
            </p>
            <p>Wollen sie wirklich fortfahren?</p>
            <button type="button" class="btn btn-danger" on:click={startImport}>
              Löschen
            </button>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
              on:click={() => (modalOpen = false)}
            >
              Schließen
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-backdrop show" transition:fade={{ duration: 150 }} />
  {/if}
</main>
