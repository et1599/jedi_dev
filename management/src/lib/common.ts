import type { UserKind } from "$lib/bindings/UserKind";
import type { FormItem } from "$lib/bindings/FormItem";

export const user_kinds: {
  [key in UserKind]: { label: string; access_level: number };
} = {
  normal: {
    label: "Normal",
    access_level: 0,
  },
  organizer: {
    label: "Organisator",
    access_level: 1,
  },
  admin: {
    label: "Admin",
    access_level: 2,
  },
  "super-admin": {
    label: "Super Admin",
    access_level: 3,
  },
} as const;

type FormItemConfigField<K, T> = {
  label: string;
  name: K;
  input: T extends string
    ? "text" | "textarea" | "date" | "email" | "tel"
    : T extends string | null
      ? "form_text"
      : T extends boolean
        ? "checkbox"
        : T extends number
          ? "number"
          : T extends string[]
            ? "options"
            : never;
  default_value: T;
  nullable?: boolean;
};

type FormItemConfigFieldDistributeFieldsInner<
  K extends keyof T,
  T,
> = K extends keyof T ? FormItemConfigField<K, T[K]> : never;
type FormItemConfigFieldDistributeFieldsOuter<T> =
  FormItemConfigFieldDistributeFieldsInner<keyof T, T>;
type FormItemConfigFieldDistributeTypes<K extends FormItem["type"]> =
  K extends FormItem["type"]
    ? FormItemConfigFieldDistributeFieldsOuter<FormItem & { type: K }>
    : never;

type FormItemDescription<K extends FormItem["type"]> = {
  label: string;
  icon: string;
  fields: FormItemConfigFieldDistributeTypes<K>[];
};

type FormItemDescriptionDistribute<K extends FormItem["type"]> =
  K extends FormItem["type"] ? FormItemDescription<K> : never;
export type GeneralFormItemDescription = FormItemDescriptionDistribute<
  FormItem["type"]
>;

export const available_form_component_props: {
  [K in FormItem["type"]]: FormItemDescription<K>;
} = {
  markdown: {
    label: "Markdown",
    icon: "markdown",
    fields: [
      {
        label: "Markdown input",
        name: "source",
        input: "textarea",
        default_value: "",
      },
    ],
  },
  text: {
    label: "Text",
    icon: "cursor-text",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Standardwert",
        name: "default",
        input: "form_text",
        default_value: null,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Erforderlich",
        name: "required",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  email: {
    label: "Email",
    icon: "at",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Standardwert",
        name: "default",
        input: "email",
        default_value: null,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Erforderlich",
        name: "required",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  telephone: {
    label: "Telefonnummer",
    icon: "telephone",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Standardwert",
        name: "default",
        input: "tel",
        default_value: null,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Erforderlich",
        name: "required",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  "check-box": {
    label: "Checkbox",
    icon: "check-square",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Standardwert",
        name: "default",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Erforderlich",
        name: "required",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  select: {
    label: "Dropdown",
    icon: "caret-down-square",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Optionen",
        name: "values",
        input: "options",
        default_value: [],
        nullable: false,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  radio: {
    label: "Radio",
    icon: "ui-radios",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Optionen",
        name: "values",
        input: "options",
        default_value: [],
        nullable: true,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  date: {
    label: "Datum",
    icon: "calendar-event",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Standardwert",
        name: "default",
        input: "date",
        default_value: null,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Erforderlich",
        name: "required",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
  number: {
    label: "Zahl",
    icon: "123",
    fields: [
      { label: "Label", name: "label", input: "text", default_value: "" },
      {
        label: "Standardwert",
        name: "default",
        input: "number",
        default_value: 0,
      },
      {
        label: "Später bearbeitbar",
        name: "can_edit",
        input: "checkbox",
        default_value: false,
      },
      {
        label: "Erforderlich",
        name: "required",
        input: "checkbox",
        default_value: false,
      },
    ],
  },
};
