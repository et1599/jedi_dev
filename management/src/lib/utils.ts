async function hash(s: string): Promise<number[]> {
  const msgUint8 = new TextEncoder().encode(s);
  const hashBuffer = await crypto.subtle.digest("SHA-256", msgUint8);
  return Array.from(new Uint8Array(hashBuffer));
}

// convert the given string to RGB values
export async function toRGB(s: string): Promise<[number, number, number]> {
  const hex = await hash(s);
  return hex.slice(0, 3) as [number, number, number];
}

declare global {
  interface Array<T> {
    sameElements(other: Array<T>): boolean;

    findLastIndex( // not in the existing typescript type information
      predicate: (value: T, index: number, obj: T[]) => unknown,
      thisArg?: any,
    ): number;
  }
}

// check that both arrays contain the same values, out of order
Array.prototype.sameElements = function <T>(this: T[], other: T[]): boolean {
  return this.length == other.length && this.every((x) => other.includes(x));
};

// perceived luminance, adapted from https://stackoverflow.com/a/596243/13095625
export function luminance(r: number, g: number, b: number): number {
  return 0.299 * r + 0.587 * g + 0.114 * b;
}

export function dedup<T>(x: T[]): T[] {
  return [...new Set(x)];
}

export function assert(check: boolean): asserts check {
  if (!check) {
    throw new Error();
  }
}

export function deepEq(l: any, r: any): boolean {
  if (typeof l != typeof r) {
    return false;
  } else if (typeof l != "object") {
    return Object.is(l, r);
  } else if (Array.isArray(l) && Array.isArray(r)) {
    if (l.length != r.length) {
      return false;
    }
    return new Array(...l).every((i, idx) => deepEq(i, r[idx]));
  } else if (!Array.isArray(l) && !Array.isArray(r)) {
    if (!deepEq(Object.keys(l).sort(), Object.keys(r).sort())) {
      return false;
    }
    return Object.keys(l).every((k) => deepEq(l[k], r[k]));
  } else {
    return false;
  }
}

export type SearchScores = {
  changedOrInserted: number;
  ignoredInner: number;
  ignoredAffix: number;
};

function compareScores(left: SearchScores, right: SearchScores): number {
  let lexicographic = (...args: number[]): number => {
    for (let i of args) {
      if (i != 0) {
        return i;
      }
    }
    return 0;
  };

  return lexicographic(
    left.changedOrInserted - right.changedOrInserted,
    left.ignoredInner - right.ignoredInner,
    left.ignoredAffix - right.ignoredAffix,
  );
}

export type SearchScoresIntermediate = {
  ignoredSuffix: number;
  startedMatching: boolean;
  scores: SearchScores;
};

export function searchScores(search: string, entry: string): SearchScores {
  let initialRow = (row: number): SearchScoresIntermediate[] => [
    {
      ignoredSuffix: 0,
      startedMatching: false,
      scores: {
        changedOrInserted: row,
        ignoredInner: 0,
        ignoredAffix: 0,
      },
    },
  ];

  let best = (
    ...args: SearchScoresIntermediate[]
  ): SearchScoresIntermediate => {
    let best = args[0];

    for (let i of args.slice(1)) {
      if (compareScores(i.scores, best.scores) < 0) {
        best = i;
      }
    }

    return best;
  };

  let lastRow: SearchScoresIntermediate[] = Array.from({
    length: entry.length + 1,
  }).map((_, idx) => ({
    ignoredSuffix: 0,
    startedMatching: false,
    scores: {
      changedOrInserted: 0,
      ignoredInner: 0,
      ignoredAffix: idx,
    },
  }));

  for (let i = 1; i < search.length + 1; i++) {
    let nextRow = initialRow(i);

    for (let j = 1; j < entry.length + 1; j++) {
      const fromLeftSrc = nextRow[j - 1];
      const fromLeft = {
        ignoredSuffix:
          fromLeftSrc.ignoredSuffix + (fromLeftSrc.startedMatching ? 1 : 0),
        startedMatching: fromLeftSrc.startedMatching,
        scores: {
          changedOrInserted: fromLeftSrc.scores.changedOrInserted,
          ignoredAffix: fromLeftSrc.scores.ignoredAffix + 1,
          ignoredInner: fromLeftSrc.scores.ignoredInner,
        },
      };

      const fromTopSrc = lastRow[j];
      const fromTop = {
        ignoredSuffix: fromTopSrc.ignoredSuffix,
        startedMatching: fromTopSrc.startedMatching,
        scores: {
          changedOrInserted: fromTopSrc.scores.changedOrInserted + 1,
          ignoredAffix: fromTopSrc.scores.ignoredAffix,
          ignoredInner: fromTopSrc.scores.ignoredInner,
        },
      };

      let fromDiag = lastRow[j - 1]; // we can reuse & mutate it since this will never be used again from that row
      fromDiag.scores.ignoredAffix -= fromDiag.ignoredSuffix;
      fromDiag.scores.ignoredInner += fromDiag.ignoredSuffix;
      fromDiag.ignoredSuffix = 0;
      fromDiag.startedMatching = true;
      if (search[i - 1].toLowerCase() != entry[j - 1].toLowerCase()) {
        fromDiag.scores.changedOrInserted += 1;
      }

      nextRow.push(best(fromLeft, fromTop, fromDiag));
    }

    lastRow = nextRow;
  }

  return lastRow[lastRow.length - 1].scores;
}

export type SearchResult<T> = {
  scores: SearchScores;
  isPerfectMatch: boolean;
  entry: T;
  originalIdx: number;
};

export function searchList<T>(
  searchTerm: string,
  list: T[],
  accessor: (_: T) => string | string[],
): SearchResult<T>[] {
  let out: SearchResult<T>[] = list.map((i, idx) => {
    const keyUnion = accessor(i);
    const key: string[] = typeof keyUnion == "string" ? [keyUnion] : keyUnion;
    let scores = searchScores(searchTerm, key[0]);
    for (let i of key.slice(1)) {
      const iScores = searchScores(searchTerm, i);
      if (compareScores(iScores, scores) < 0) {
        scores = iScores;
      }
    }
    return {
      scores,
      isPerfectMatch: scores.changedOrInserted === 0,
      entry: i,
      originalIdx: idx,
    };
  });
  out.sort((a, b) => compareScores(a.scores, b.scores));
  return out;
}

export type KeysOfUnion<T> = T extends T ? keyof T : never;

// https://stackoverflow.com/questions/60141960/typescript-key-value-relation-preserving-object-entries-type/75337277#75337277
type Entries<T> = { [key in keyof T]: [key, T[key]] }[keyof T][];
// Same as `Object.entries()` but with type inference
export function objectEntries<T extends object>(obj: T): Entries<T> {
  return Object.entries(obj) as Entries<T>;
}

export function validEmail(mail: string): boolean {
  return /^[\x00-\x7F]+@[\x00-\x7F]+\.[\x00-\x7F]+/.test(mail);
}
