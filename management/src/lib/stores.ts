import type { Writable } from "svelte/store";
import { writable } from "svelte/store";
import { api_base, getRequest } from "$lib/apiHelper";
import { browser } from "$app/environment";
import { assert } from "$lib/utils";
import type { User } from "$lib/bindings/User";
import type { Config } from "$lib/bindings/Config";
import type { UserOverview } from "$lib/bindings/UserOverview";
import type { EventOverview } from "$lib/bindings/EventOverview";
import type { ExtendedSectionOverview } from "./bindings/ExtendedSectionOverview";

type Mapping<T> = (
  by: string,
) => Promise<T extends any[] ? { [key: string]: T[number] } : any>;

type MappingStorage<T> = {
  [by: string]: T extends any[] ? { [key: string]: T[number] } : any;
};

type FetchStore<T> = {
  subscribe: Writable<T>["subscribe"];
  refresh: () => Promise<T>;
  mapping: Mapping<T>;
  filled: () => Promise<T>;
};

function createFetchStore<T>(api_uri: string, initial: T): FetchStore<T> {
  let value: Promise<T> | null = null;
  let mappings: MappingStorage<T> = {};

  function fetchValue(set: Writable<T>["set"]): Promise<T> {
    if (browser) {
      value = (async () => {
        const out = (await getRequest(api_uri)) as T;
        mappings = {};
        set(out);
        return out;
      })();
      return value;
    } else {
      return new Promise(() => {});
    }
  }

  const { subscribe, set } = writable(initial, () => {
    if (value === null) {
      fetchValue(set);
    }
  });

  async function get(): Promise<T> {
    if (value === null) {
      return await fetchValue(set);
    } else {
      return await value;
    }
  }

  return {
    subscribe,
    refresh: () => fetchValue(set),
    mapping: async (by: string) => {
      let data = await get();

      assert(data instanceof Array);
      if (!(by in mappings)) {
        mappings[by] = Object.fromEntries(data.map((i) => [i[by], i]));
      }

      return mappings[by];
    },
    filled: get,
  };
}

export const login_info = createFetchStore<User>("user/self", {
  id: "[id]",
  email: "[email]",
  display_name: "User",
  kind: "normal",
  admin_for: [],
  relevant_events: [],
  relevant_event_sections: [],
});

export const config = createFetchStore<Config>("config", {
  signin_length: 1,
  archive_length: 1,
  passive_update_length: 1,
  active_update_length: 1,
});

export const sections = createFetchStore<ExtendedSectionOverview[]>(
  "sections",
  [],
);

export const users = createFetchStore<UserOverview[]>("users", []);

export const events = createFetchStore<EventOverview[]>("events", []);

type NotLoggedIn = { promise: Promise<null>; resolve: (_: null) => void };
export const until_logged_in = writable<null | NotLoggedIn>(null);
