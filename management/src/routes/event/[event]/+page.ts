import { getEvent } from "$lib/apiHelper";
import type { Event } from "$lib/bindings/Event";

export async function load({
  params,
}: {
  params: { event: string };
}): Promise<Event> {
  return await getEvent(params.event);
}
