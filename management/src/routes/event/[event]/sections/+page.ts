import { getEvent, getEventSectionInfos } from "$lib/apiHelper";
import type { Event } from "$lib/bindings/Event";
import type { EventSectionInfo } from "$lib/bindings/EventSectionInfo";

export async function load({
  params,
}: {
  params: { event: string };
}): Promise<Event & { infos: EventSectionInfo[] }> {
  const infos = await getEventSectionInfos(params.event);
  const event: Event = await getEvent(params.event);
  let out = event as Event & { infos: EventSectionInfo[] };
  out.infos = infos;
  return out;
}
