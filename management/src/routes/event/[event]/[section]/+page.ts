import { getEvent, getEventSection } from "$lib/apiHelper";
import type { EventSection } from "$lib/bindings/EventSection";
import type { Event } from "$lib/bindings/Event";
import type { Form } from "$lib/bindings/Form";
import type { SectionOverview } from "$lib/bindings/SectionOverview";

export type PageData = EventSection & {
  event_id: string;
  section_id: string;
  form: Form;
  sections: SectionOverview[];
};

export async function load({
  params,
}: {
  params: { event: string; section: string };
}): Promise<PageData> {
  const event_section: EventSection = await getEventSection(
    params.event,
    params.section,
  );
  const event: Event = await getEvent(params.event);
  return {
    event_id: params.event,
    section_id: params.section,
    form: event.form,
    sections: event.sections,
    ...event_section,
  };
}
