export type PageData = {
  event: string;
  section: string;
};

export function load({ params }: { params: PageData }): PageData {
  return params;
}
