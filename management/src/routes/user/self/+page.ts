import { getUserSelf } from "$lib/apiHelper";
import type { User } from "$lib/bindings/User";

export async function load(): Promise<User> {
  return await getUserSelf();
}
