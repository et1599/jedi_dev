import { getUser } from "$lib/apiHelper";
import type { UserOverview } from "$lib/bindings/UserOverview";

export async function load({
  params,
}: {
  params: { user: string };
}): Promise<UserOverview> {
  return await getUser(params.user);
}
