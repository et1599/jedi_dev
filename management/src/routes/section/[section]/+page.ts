import { getSection } from "$lib/apiHelper";
import type { Section } from "$lib/bindings/Section";

export async function load({
  params,
}: {
  params: { section: string };
}): Promise<Section> {
  return await getSection(params.section);
}
