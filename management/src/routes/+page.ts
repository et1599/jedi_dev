import { goto } from "$app/navigation";
import { base } from "$app/paths";

export const ssr = false;

export async function load({ params }: { params: any }): Promise<void> {
  goto(`${base}/dashboard`);
}
