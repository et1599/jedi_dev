use sqlx::Type;

use crate::{
    db::sqlx_err,
    helpers::NonEmptyString,
    management_user::{
        split_users, BasicUserInfo, Invitation, PartialUserOverview, UserKind, UserOverview,
    },
    markdown::{RenderedMarkdown, UnrenderedMarkdown},
    prelude::*,
    section::SectionOverview,
};

pub mod form;
use form::{Form, RenderedForm};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, Type, Debug, TS)]
#[sqlx(type_name = "EVENT_STATE")]
#[sqlx(rename_all = "kebab-case")]
#[serde(rename_all = "kebab-case")]
#[ts(export)]
pub enum EventState {
    Setup,
    Active,
    Done,
}

#[derive(Macrow)]
pub struct DbFullEventState {
    pub state: EventState,
    pub signup_until: Option<Date>,
    pub decisions_until: Option<Date>,
    pub archived_until: Option<Date>,
}

#[derive(Clone, Copy, Serialize, Deserialize, TS, Macrow)]
#[ts(export)]
#[serde(rename_all = "kebab-case", tag = "type")]
#[macrow(try_from = "crate::event::DbFullEventState")]
pub enum FullEventState {
    Setup,
    Active {
        #[ts(type = "string")]
        signup_until: Date,
        #[ts(type = "string")]
        decisions_until: Date,
    },
    Done {
        #[ts(type = "string")]
        archived_until: Date,
    },
}

impl FullEventState {
    pub async fn assert_correct(&self, conn: &mut sqlx::PgConnection) -> Result<(), Error> {
        let state = sqlx::query!(r#"SELECT CAST(NOW() AS DATE) AS "current_date!""#)
            .fetch_one(conn)
            .await?;
        match *self {
            FullEventState::Active {
                decisions_until,
                signup_until,
            } if decisions_until < signup_until => {
                Err(UserError::DecisionsBeforeSignupDeadline.into())
            },
            FullEventState::Active {
                decisions_until, ..
            } if decisions_until <= state.current_date => {
                Err(UserError::EventStateDatesNotFuture.into())
            },
            FullEventState::Done { archived_until } if archived_until <= state.current_date => {
                Err(UserError::EventStateDatesNotFuture.into())
            },
            _ => Ok(()),
        }
    }
}

impl TryFrom<DbFullEventState> for FullEventState {
    type Error = sqlx::Error;

    fn try_from(value: DbFullEventState) -> Result<Self, sqlx::Error> {
        Ok(
            match (
                value.state,
                value.signup_until,
                value.decisions_until,
                value.archived_until,
            ) {
                (EventState::Setup, None, None, None) => FullEventState::Setup,
                (EventState::Active, Some(signup_until), Some(decisions_until), None) => {
                    FullEventState::Active {
                        signup_until,
                        decisions_until,
                    }
                },
                (EventState::Done, None, None, Some(archived_until)) => {
                    FullEventState::Done { archived_until }
                },
                _ => {
                    return Err(sqlx_err(
                        "Invalid combination of event state and NULL state of associated columns",
                    ))
                },
            },
        )
    }
}

impl From<FullEventState> for EventState {
    fn from(value: FullEventState) -> Self {
        match value {
            FullEventState::Setup { .. } => EventState::Setup,
            FullEventState::Active { .. } => EventState::Active,
            FullEventState::Done { .. } => EventState::Done,
        }
    }
}

#[derive(Macrow)]
pub struct DbBasicEventOverview {
    pub id: EventId,
    pub name: NonEmptyString,
    pub description: String,
    pub state: EventState,
    pub signup_until: Option<Date>,
    pub decisions_until: Option<Date>,
    pub archived_until: Option<Date>,
}

#[derive(Clone, Serialize, TS, Macrow)]
#[ts(export)]
#[macrow(try_from = "crate::event::DbBasicEventOverview")]
pub struct EventOverview {
    pub id: EventId,
    pub name: NonEmptyString,
    pub description: RenderedMarkdown,
    pub state: FullEventState,
}

impl TryFrom<DbBasicEventOverview> for EventOverview {
    type Error = sqlx::Error;

    fn try_from(x: DbBasicEventOverview) -> Result<Self, sqlx::Error> {
        Ok(EventOverview {
            id: x.id,
            name: x.name,
            description: RenderedMarkdown::render(x.description),
            state: DbFullEventState {
                state: x.state,
                signup_until: x.signup_until,
                decisions_until: x.decisions_until,
                archived_until: x.archived_until,
            }
            .try_into()?,
        })
    }
}

#[derive(Deserialize, TS)]
#[ts(export)]
pub struct CreateEvent {
    pub name: NonEmptyString,
    pub description: UnrenderedMarkdown,
    pub state: FullEventState,
    pub form: Form,
    pub can_users_leave: bool,
    pub can_users_edit_name: bool,
    pub max_total_delegates: i32,
    pub sections: Vec<SectionId>,
}

pub async fn add_event_sections_without_updating_delegate_count(
    db: &mut sqlx::PgConnection,
    event: EventId,
    sections: &[SectionId],
) -> Result<(), sqlx::Error> {
    sqlx::query!(
        r#"
            INSERT INTO event_sections(event, section, max_delegates, last_section_stats, secret)
            SELECT
                $1 AS event, sections.id AS section,
                -- Using 0 for max_delegates is fine since this is always in the middle of a
                -- transaction and overridden later; and it's also correct for guest sections
                0 as max_delegates,
                sections.stats AS last_section_stats,
                encode(gen_random_bytes(32), 'hex') AS secret
            FROM UNNEST(CAST($2 AS TEXT[])) AS section_list(section_id)
                    JOIN sections ON section_list.section_id = sections.id
            ON CONFLICT (event, section) DO NOTHING
        "#,
        event as EventId,
        sections as &[SectionId],
    )
    .execute(db)
    .await?;

    Ok(())
}

pub async fn update_delegate_count(
    db: &mut sqlx::PgConnection,
    event: EventId,
) -> Result<(), sqlx::Error> {
    sqlx::query_file!("src/update_delegate_count.sql", event as EventId)
        .execute(db)
        .await?;
    Ok(())
}

#[derive(Macrow)]
pub struct DbEvent {
    #[macrow(context)]
    pub user_kind: UserKind,
    #[macrow(flatten)]
    pub overview: crate::event::EventOverview,
    pub organizers: Vec<BasicUserInfo>,
    #[macrow(try_from)]
    pub form: Form,
    pub can_users_leave: bool,
    pub can_users_edit_name: bool,
    pub sections: Vec<SectionOverview>,
    pub max_total_delegates: i32,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
#[macrow(try_from = "crate::event::DbEvent")]
pub struct Event {
    #[serde(flatten)]
    pub overview: EventOverview,
    pub organizers: Vec<UserOverview>,
    pub partial_organizers: Vec<PartialUserOverview>,
    pub form: Form,
    pub can_users_leave: bool,
    pub can_users_edit_name: bool,
    pub sections: Vec<SectionOverview>,
    pub max_total_delegates: i32,
}

impl TryFrom<DbEvent> for Event {
    type Error = sqlx::Error;

    fn try_from(db: DbEvent) -> Result<Self, sqlx::Error> {
        let (organizers, partial_organizers) = split_users(db.organizers, db.user_kind);

        Ok(Event {
            overview: db.overview,
            organizers,
            partial_organizers,
            form: db.form,
            can_users_leave: db.can_users_leave,
            can_users_edit_name: db.can_users_edit_name,
            sections: db.sections,
            max_total_delegates: db.max_total_delegates,
        })
    }
}

diff! {
    pub struct EventDiff {
        pub name: Option<NonEmptyString>,
        pub description: Option<UnrenderedMarkdown>,
        pub state: Option<FullEventState>,
        pub form: Option<Form>,
        pub can_users_leave: Option<bool>,
        pub can_users_edit_name: Option<bool>,
        pub sections: Option<Vec<SectionId>>,
        pub max_total_delegates: Option<u32>,
        pub update_section_stats: bool,
        pub add_organizers: Vec<UserId>,
        pub invite_organizers: Vec<Invitation>,
        pub remove_organizers: Vec<UserId>,
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum EventStateErrorCutoff {
    BeforeSignupDeadline { now: Date },
    BeforeArchival,
}

pub use EventStateErrorCutoff::*;

pub fn event_state_to_error(
    state: FullEventState,
    purpose: EventStateErrorCutoff,
) -> Result<(), UserError<'static>> {
    match (state, purpose) {
        (FullEventState::Setup, _) => Err(UserError::EventBeingSetUp),
        (FullEventState::Active { signup_until, .. }, BeforeSignupDeadline { now })
            if now > signup_until =>
        {
            Err(UserError::EventActiveButSignupDone)
        },
        (FullEventState::Active { .. }, _) => Ok(()),
        (FullEventState::Done { .. }, _) => Err(UserError::EventAlreadyDone),
    }
}

#[derive(Serialize, Macrow)]
pub struct TemplateEvent {
    #[serde(flatten)]
    #[macrow(flatten)]
    pub overview: crate::event::EventOverview,
    #[macrow(try_from)]
    pub form: RenderedForm,
    pub can_users_leave: bool,
    pub can_users_edit_name: bool,
}
