use std::{borrow::Cow, fmt, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::{types::Uuid, Type};

use crate::helpers::NonEmptyString;

macro_rules! id_wrapper {
    {@impl $t:ident(parse $inner:ty)} => {
        impl<'a> TryFrom<Cow<'a, str>> for $t {
            type Error = <Self as FromStr>::Err;

            fn try_from(value: Cow<'a, str>) -> Result<Self, Self::Error> {
                Self::from_str(&value)
            }
        }

        impl From<$t> for String {
            fn from(value: $t) -> Self {
                value.0.to_string()
            }
        }

        impl FromStr for $t {
            type Err = <$inner as FromStr>::Err;

            fn from_str(value: &str) -> Result<Self, Self::Err> {
                Ok($t(value.parse()?))
            }
        }

        impl Copy for $t { }
    };
    {@impl $t:ident(owned $inner:ty)} => {
        impl<'a> TryFrom<Cow<'a, str>> for $t {
            type Error = <$inner as TryFrom<String>>::Error;

            fn try_from(value: Cow<'a, str>) -> Result<Self, Self::Error> {
                Ok($t(value.into_owned().try_into()?))
            }
        }

        impl From<$t> for String {
            fn from(value: $t) -> Self {
                value.0.into()
            }
        }

        impl FromStr for $t {
            type Err = <$inner as TryFrom<String>>::Error;

            fn from_str(value: &str) -> Result<Self, Self::Err> {
                Ok($t(value.to_owned().try_into()?))
            }
        }
    };
    {$($vis:vis struct $name:ident($innervis:vis $via:ident $t:ident);)*} => {
        $(
            #[derive(Clone, PartialEq, Eq, Serialize, Deserialize, Type, Debug, Hash)]
            #[sqlx(transparent)]
            $vis struct $name($innervis $t);

            forward_ts!{impl TS for $name = $t}

            paste::paste!{
                #[allow(non_snake_case, unused_imports)]
                mod [<export_ $name>] {
                    use ts_rs::TS;
                    use super::*;

                    #[allow(dead_code)]
                    #[derive(TS)]
                    #[ts(export)]
                    struct $name($t);
                }
            }

            id_wrapper!{@impl $name($via $t)}

            impl fmt::Display for $name {
                fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                    fmt::Display::fmt(&self.0, f)
                }
            }
        )*
    }
}

id_wrapper! {
    pub struct UserId(pub parse Uuid);
    pub struct EventId(pub parse Uuid);
    pub struct SectionId(pub owned NonEmptyString);
    pub struct RequestId(pub parse Uuid);
    pub struct SessionId(pub parse Uuid);
}

pub fn sqlx_err(x: &(impl ToString + ?Sized)) -> sqlx::Error {
    sqlx::Error::Decode(x.to_string().into())
}
