#![allow(single_use_lifetimes)] // happens in derives

use std::{convert::Infallible, future::Future, ops};

use serde::Serialize;
use warp::{
    hyper::StatusCode,
    reject::{self, Reject, Rejection},
    reply,
    reply::{with_status, Response},
    Reply,
};

use crate::{
    db::{RequestId, SectionId, UserId},
    event::form::FormValidationError,
    AppContext,
};

enum Cow<'a, T> {
    Borrowed(&'a T),
    Owned(T),
}

impl<T> ops::Deref for Cow<'_, T> {
    type Target = T;

    fn deref(&self) -> &T {
        match *self {
            Cow::Borrowed(borrowed) => borrowed,
            Cow::Owned(ref owned) => owned,
        }
    }
}

#[derive(Debug, thiserror::Error, Serialize)]
#[serde(rename_all = "kebab-case", tag = "type")]
pub enum UserError<'a> {
    #[error("The requested item does not exist")]
    DoesntExist,
    #[error("Invalid email address")]
    #[serde(rename = "invalid-email")]
    EmailUserInvalidEmail,
    #[error("The email users credentials are incorrect")]
    #[serde(rename = "login-failure")]
    EmailUserLoginFailure,
    #[error(
        "Invalid organizer updates (don't exist: {doesnt_exist:?}, tried to remove self: \
         {tried_to_remove_self:?}"
    )]
    #[serde(rename = "organizer-updates")]
    EventInvalidOrganizerUpdates {
        doesnt_exist: Vec<UserId>,
        tried_to_remove_self: bool,
    },
    #[error("At least two of guests, delegates and removals are overlapping: {overlap:?}")]
    #[serde(rename = "request-action-overlap")]
    EventSectionRequestActionOverlap { overlap: Vec<RequestId> },
    #[error("A referenced request doesn't exist ({ids:?})")]
    #[serde(rename = "request-doesnt-exist")]
    EventSectionRequestDoesntExist { ids: Vec<RequestId> },
    #[error("A referenced section doesn't exist for the event ({ids:?})")]
    #[serde(rename = "event-section-doesnt-exist")]
    EventSectionTargetDoesntExist { ids: Vec<SectionId> },
    #[error("A request cannot be moved to a position in a guest section")]
    #[serde(rename = "positioning-in-guest-section")]
    EventSectionPositioningInGuestSection,
    #[error("Integer conversion failed")]
    IntConv(#[serde(skip)] std::num::TryFromIntError),
    #[error("Invalid body: {0}")]
    InvalidBody(#[serde(skip)] &'a warp::body::BodyDeserializeError),
    #[error("Invalid query: {0}")]
    InvalidQuery(#[serde(skip)] &'a warp::reject::InvalidQuery),
    #[error("JSON deserialization failed")]
    Json(#[serde(skip)] serde_json::error::Error),
    #[error("Email exists already")]
    #[serde(rename = "email-exists-already")]
    ManageUserEmailExistsAlready,
    #[error("Invalid method: {0}")]
    MethodNotAllowed(#[serde(skip)] &'a warp::reject::MethodNotAllowed),
    #[error("Insufficient permissions")]
    NotAllowed,
    #[error("Not logged in")]
    NotLoggedIn,
    #[error("The action cannot occur because the event is being set up")]
    EventBeingSetUp,
    #[error("Action cannot occur because the event is already active")]
    EventAlreadyActive,
    #[error("The action cannot occur because the signup deadline has passed")]
    EventActiveButSignupDone,
    #[error("The action cannot occur because the event is already done")]
    EventAlreadyDone,
    #[error("The decision deadline cannot happen before the signup deadline")]
    #[serde(rename = "decisions-before-signup")]
    DecisionsBeforeSignupDeadline,
    #[error("The dates in an event state should not be in the past or today")]
    EventStateDatesNotFuture,
    #[error("An event with {min} has to have at least {min} delegates, this one had {value}")]
    #[serde(rename = "event-min-max-delegates")]
    EventMaxDelegatesTooFew { value: i32, min: i32 },
    #[error("A guest section may not have non-zero statistics")]
    #[serde(rename = "guest-section-stats-not-zero")]
    GuestSectionStatsNotZero,
    #[error("A section may not be changed in regards to it being a guest section")]
    SectionGuestChange,
    #[error("You should be redirected to the email user dashboard")]
    RedirectToEmailUserDashboard,
    #[error(
        "Form error (empty name: {empty_name:?}, name change not allowed: \
         {name_change_not_allowed:?}, form validation errors: {form:?})"
    )]
    #[serde(rename = "form-error")]
    RequestFormError {
        empty_name: bool,
        name_change_not_allowed: bool,
        form: Vec<FormValidationError>,
    },
    #[error("Referenced request does not exist")]
    RequestGone,
    #[error("A request cannot be removed because the event has disabled that ability")]
    #[serde(rename = "not-allowed")]
    RequestRemovalNotAllowed,
}

#[derive(Debug, thiserror::Error)]
pub enum LettreSendError {
    #[error("SMTP error: {0}")]
    Smtp(#[from] lettre::transport::smtp::Error),
    #[error("Sendmail error: {0}")]
    Sendmail(#[from] lettre::transport::sendmail::Error),
    #[error("EML-file error: {0}")]
    File(#[from] lettre::transport::file::Error),
}

#[derive(Debug, thiserror::Error)]
pub enum GeneralError<'a> {
    #[error(transparent)]
    UserError(UserError<'a>),
    #[error("SQL error: {0}")]
    Sql(#[from] sqlx::Error),
    #[error("Templating error: {0}")]
    Hbs(#[from] handlebars::RenderError),
    #[error("Password handling error: {0}")]
    PwHash(#[from] pwhash::error::Error),
    #[error("Int conversion error: {0}")]
    GeneratedIntConv(std::num::TryFromIntError),
    #[error("JSON serialization error: {0}")]
    GeneratedJson(serde_json::error::Error),
    #[error("CSV generation error: {0}")]
    GeneratedCsv(csv::Error),
    #[error("HTTP error: {0}")]
    Http(#[from] warp::http::Error),
    #[error("Email couldn't be rendered because of a missing body")]
    MissingEmailBody,
    #[error("Email couldn't be rendered because of a missing title")]
    MissingEmailTitle,
    #[error(transparent)]
    LettreAddress(#[from] lettre::address::AddressError),
    #[error(transparent)]
    LettreEmail(#[from] lettre::error::Error),
    #[error("Couldn't send emails: {0:?}")]
    LettreSend(Vec<LettreSendError>),
    #[error("Unknown rejection: {0:?}")]
    UnknownRejection(&'a Rejection),
}

impl<'a> GeneralError<'a> {
    fn from_rejection(rejection: &'a Rejection) -> Cow<'a, Self> {
        if let Some(err) = rejection.find() {
            return Cow::Borrowed(err);
        } else if let Some(err) = rejection.find::<warp::reject::InvalidQuery>() {
            return Cow::Owned(GeneralError::UserError(UserError::InvalidQuery(err)));
        } else if let Some(err) = rejection.find::<warp::body::BodyDeserializeError>() {
            return Cow::Owned(GeneralError::UserError(UserError::InvalidBody(err)));
        } else if let Some(err) = rejection.find::<warp::reject::MethodNotAllowed>() {
            return Cow::Owned(GeneralError::UserError(UserError::MethodNotAllowed(err)));
        } else {
            return Cow::Owned(GeneralError::UnknownRejection(rejection));
        }
    }

    #[allow(clippy::enum_glob_use, clippy::match_same_arms)]
    pub fn status(&self) -> StatusCode {
        use UserError::*;

        match *self {
            GeneralError::UserError(ref e) => match *e {
                DoesntExist => StatusCode::NOT_FOUND,
                EmailUserInvalidEmail => StatusCode::BAD_REQUEST,
                EmailUserLoginFailure => StatusCode::FORBIDDEN,
                EventInvalidOrganizerUpdates { .. } => StatusCode::CONFLICT,
                EventSectionRequestActionOverlap { .. } => StatusCode::BAD_REQUEST,
                EventSectionRequestDoesntExist { .. } => StatusCode::BAD_REQUEST,
                EventSectionTargetDoesntExist { .. } => StatusCode::BAD_REQUEST,
                EventSectionPositioningInGuestSection { .. } => StatusCode::BAD_REQUEST,
                IntConv(_) => StatusCode::BAD_REQUEST,
                InvalidBody(_) => StatusCode::BAD_REQUEST,
                InvalidQuery(_) => StatusCode::BAD_REQUEST,
                Json(_) => StatusCode::BAD_REQUEST,
                ManageUserEmailExistsAlready => StatusCode::CONFLICT,
                MethodNotAllowed(_) => StatusCode::METHOD_NOT_ALLOWED,
                NotAllowed => StatusCode::FORBIDDEN,
                NotLoggedIn => StatusCode::UNAUTHORIZED,
                EventBeingSetUp => StatusCode::CONFLICT,
                EventAlreadyActive => StatusCode::CONFLICT,
                EventActiveButSignupDone => StatusCode::CONFLICT,
                EventAlreadyDone => StatusCode::CONFLICT,
                DecisionsBeforeSignupDeadline => StatusCode::BAD_REQUEST,
                EventStateDatesNotFuture => StatusCode::BAD_REQUEST,
                EventMaxDelegatesTooFew { .. } => StatusCode::BAD_REQUEST,
                GuestSectionStatsNotZero => StatusCode::BAD_REQUEST,
                SectionGuestChange => StatusCode::CONFLICT,
                // Shouldn't ever be observed:
                RedirectToEmailUserDashboard => StatusCode::INTERNAL_SERVER_ERROR,
                RequestFormError { .. } => StatusCode::BAD_REQUEST,
                RequestGone => StatusCode::GONE,
                RequestRemovalNotAllowed => StatusCode::FORBIDDEN,
            },
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

impl Reject for GeneralError<'static> {}

impl<'a> From<UserError<'a>> for GeneralError<'a> {
    fn from(value: UserError<'a>) -> Self {
        GeneralError::UserError(value)
    }
}

impl From<Vec<LettreSendError>> for GeneralError<'_> {
    fn from(value: Vec<LettreSendError>) -> Self {
        GeneralError::LettreSend(value)
    }
}

pub type Error = GeneralError<'static>;

pub async fn auto_reject<T>(
    value: impl Future<Output = Result<T, GeneralError<'static>>> + Send,
) -> Result<T, Rejection> {
    value.await.map_err(Rejection::from)
}

pub async fn handle_endpoint_rejections_api(
    _: &'static AppContext,
    rejection: Rejection,
) -> Result<Response, Rejection> {
    if rejection.is_not_found() {
        return Err(rejection);
    }

    let error = GeneralError::from_rejection(&rejection);
    let error = &*error;

    let status = error.status();

    let response = match *error {
        GeneralError::UserError(ref e) => {
            log::debug!(target: "jedi", "user error: {e}");
            reply::json(e)
        },
        ref err => {
            log::error!(target: "jedi", "{err}");
            return Ok(status.into_response());
        },
    };

    Ok(with_status(response, status).into_response())
}

pub async fn handle_endpoint_rejections_human(
    ctx: &'static AppContext,
    rejection: Rejection,
) -> Result<Response, Rejection> {
    if rejection.is_not_found() {
        return Err(rejection);
    }

    let error = GeneralError::from_rejection(&rejection);
    let error = &*error;

    let error = rejection.find::<GeneralError>().unwrap_or(error);
    let status = error.status();

    let response = match *error {
        GeneralError::UserError(UserError::DoesntExist) => return Err(reject::not_found()),
        GeneralError::UserError(UserError::EmailUserLoginFailure) => ctx
            .html_template("user/dashboard_failure.html", &crate::EmptyStruct {})
            .unwrap(),
        GeneralError::UserError(ref e) => {
            log::debug!(target: "jedi", "user error: {e}");
            reply::html(format!("<h3>{e}</h3>")).into_response()
        },
        ref err => {
            log::error!(target: "jedi", "{err}");
            reply::html(
                "<h3>An internal server error occured. Try again later, and if it still doesn't \
                 work, please send an email to 42triangles@tutanota.com.</h3>"
                    .to_owned(),
            )
            .into_response()
        },
    };

    Ok(with_status(response, status).into_response())
}

#[derive(Debug)]
pub struct CrossOriginFailure;

impl Reject for CrossOriginFailure {}

pub async fn handle_final_rejections(
    _: &'static AppContext,
    rejection: Rejection,
) -> Result<Response, Infallible> {
    if rejection.is_not_found() {
        return Ok(
            with_status(reply::html("<h3>Not found.</h3>"), StatusCode::NOT_FOUND).into_response(),
        );
    }

    if rejection.find::<CrossOriginFailure>().is_some() {
        return Ok(with_status(
            "Cross origin requests are not allowed",
            StatusCode::BAD_REQUEST,
        )
        .into_response());
    }

    Ok(with_status(format!("{rejection:?}"), StatusCode::INTERNAL_SERVER_ERROR).into_response())
}

pub async fn handle_task_error(inner: impl Future<Output = Result<(), Error>> + Send) {
    if let Err(error) = inner.await {
        log::error!(target: "jedi", "{error}");
    }
}

pub fn with_err_status(
    reply: Result<impl Reply, Error>,
    err: Option<UserError<'static>>,
) -> Result<Response, Error> {
    Ok(with_status(
        reply?.into_response(),
        err.map_or(StatusCode::OK, |e| Error::UserError(e).status()),
    )
    .into_response())
}
