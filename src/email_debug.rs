use std::{collections::HashMap, sync::RwLock};

use lazy_static::lazy_static;
use serde::Deserialize;
use warp::{query, reply, reply::Response, Filter, Reply};

use crate::helpers::{Email, NonEmptyString};

lazy_static! {
    static ref LOG: RwLock<HashMap<Email, HashMap<&'static str, String>>> =
        RwLock::new(HashMap::new());
}

pub fn log(email: &Email, kind: &'static str, payload: &str) {
    LOG.write()
        .unwrap()
        .entry(email.clone())
        .or_default()
        .insert(kind, payload.to_owned());
}

#[derive(Deserialize)]
#[serde(untagged)]
enum EmailLogFilter {
    None {},
    Email { email: Email },
    EmailTag { email: Email, tag: NonEmptyString },
}

pub fn return_log() -> filter!((Response,)) {
    warp::path!("email-log")
        .and(query::<EmailLogFilter>())
        .map(|filter: EmailLogFilter| {
            let log = &*LOG.read().unwrap();

            match filter {
                EmailLogFilter::None {} => reply::json(log).into_response(),
                EmailLogFilter::Email { email } => {
                    reply::json(&log.get(&email).unwrap_or(&HashMap::new())).into_response()
                },
                EmailLogFilter::EmailTag { email, tag } => {
                    reply::json(&log.get(&email).and_then(|inner| inner.get(&*tag.0)))
                        .into_response()
                },
            }
        })
}
