//pub struct

//use either::{Either, Left, Right};
use serde::{Deserialize, Serialize};
use warp::{
    hyper::{StatusCode, Uri},
    redirect,
    reply::{with_status, Response},
    Filter, Reply,
};

use crate::{
    helpers::{Email, NonEmptyString, Password, PasswordHash},
    management_user::{reset_password, ResetParams, ResetPasswordReason, SignupParams},
    prelude::*,
    rejections::{auto_reject, Error},
    warp_ext::{maybe, with_cookies},
};

pub mod auth;
use auth::{login, logout, with_login, with_login_unchecked};

// TODO: Rename to `unchecked_login_to_dashboard`
async fn login_to_dashboard(
    user_id: UserId,
    db: &mut sqlx::PgConnection,
) -> Result<Response, Error> {
    Ok(with_cookies(
        [login(user_id, db).await?],
        redirect::see_other(Uri::from_static(DASHBOARD_PATH)),
    )
    .into_response())
}

// TODO: Rename to `unchecked_login_to_dashboard_transaction`
async fn login_to_dashboard_transaction(
    user_id: UserId,
    mut db: sqlx::Transaction<'_, sqlx::Postgres>,
) -> Result<Response, Error> {
    let out = login_to_dashboard(user_id, &mut db).await?;
    db.commit().await?;
    Ok(out)
}

#[derive(Deserialize)]
struct LoginForm {
    email: Email,
    #[serde(default)] // for the password reset form
    password: Password,
}

async fn handle_login(form: Option<LoginForm>, ctx: StaticContext) -> EndpointResult {
    #[derive(Serialize)]
    #[serde(tag = "type", rename_all = "kebab-case")]
    enum Error {
        InvalidCredentials,
        PasswordRemoved,
    }

    let email = form.as_ref().map(|form| &*form.email).unwrap_or_default();

    let error = 'error: {
        match form {
            None => break 'error None, // GET
            Some(ref form) => {
                // POST
                let mut transaction = ctx.db.begin().await?;

                let Some(user) = sqlx::query!(
                    r#"
                        SELECT id AS "user_id: UserId", password_hash AS "hash: PasswordHash"
                        FROM management_users
                        WHERE email = $1 AND complete
                    "#,
                    &form.email,
                )
                .fetch_optional(&mut *transaction)
                .await?
                else {
                    break 'error Some(Error::InvalidCredentials);
                };

                match user.hash {
                    None => break 'error Some(Error::PasswordRemoved),
                    Some(hash) if form.password.verify(&hash) => {
                        return login_to_dashboard_transaction(user.user_id, transaction).await;
                    },
                    Some(_) => break 'error Some(Error::InvalidCredentials),
                }
            },
        }
    };

    Ok(with_status(
        ctx.html_template(
            "manage/login.html",
            &json!({ "email": email, "error": error }),
        )?,
        if error.is_some() {
            StatusCode::FORBIDDEN
        } else {
            StatusCode::OK
        },
    )
    .into_response())
}

#[derive(Deserialize, Serialize)]
struct SignupForm {
    new_email: Email,
    display_name: NonEmptyString,
    password: Password,
}

async fn handle_signup(
    params: SignupParams,
    form: Option<SignupForm>,
    ctx: StaticContext,
) -> EndpointResult {
    #[derive(Serialize)]
    #[serde(tag = "type", rename_all = "kebab-case")]
    enum SignupError {
        EmailExistsAlready,
    }

    let to_login = || Ok(redirect::see_other(Uri::from_static(LOGIN_PATH)).into_response());
    let render_form = |prefill: &SignupForm, error: Option<SignupError>| {
        ctx.html_template(
            "manage/signup.html",
            &json!({ "signup": params, "prefill": prefill, "error": error }),
        )
    };

    match form {
        None => {
            let Some(display_name) = sqlx::query_scalar!(
                r#"
                    SELECT display_name AS "display_name: NonEmptyString"
                    FROM management_users
                    WHERE NOT complete AND email = $1 AND partial_secret = $2
                "#,
                &params.email,
                &params.secret,
            )
            .fetch_optional(&ctx.db)
            .await?
            else {
                return to_login();
            };

            // for prefills incorrect non-empty strings are okay
            render_form(
                &SignupForm {
                    new_email: params.email.clone(),
                    display_name: display_name.unwrap_or_else(|| NonEmptyString(String::new())),
                    password: Password::default(),
                },
                None,
            )
        },
        Some(ref form) => {
            use sqlx::Error::Database as DatabaseErr;

            let mut transaction = ctx.db.begin().await?;

            let user_id = sqlx::query_scalar!(
                r#"
                    UPDATE management_users
                    SET complete = TRUE,
                        email = $3,
                        partial_secret = NULL,
                        display_name = $4,
                        password_hash = $5
                    WHERE NOT complete AND email = $1 AND partial_secret = $2
                    RETURNING id AS "id: UserId"
                "#,
                &params.email,
                &params.secret,
                &form.new_email,
                &form.display_name,
                &form.password.hash()?,
            )
            .fetch_optional(&mut *transaction)
            .await;

            match user_id {
                Ok(Some(user_id)) => login_to_dashboard_transaction(user_id, transaction).await,
                Ok(None) => {
                    transaction.commit().await?;
                    to_login()
                },
                Err(DatabaseErr(e)) if e.constraint() == Some("management_users_email_key") => {
                    render_form(form, Some(SignupError::EmailExistsAlready))
                },
                Err(err) => Err(err.into()),
            }
        },
    }
}

#[derive(Deserialize, Serialize)]
struct ResetForm {
    password: Password,
}

async fn handle_finalize_reset(
    params: ResetParams,
    form: Option<ResetForm>,
    ctx: StaticContext,
) -> EndpointResult {
    let to_login = || Ok(redirect::see_other(Uri::from_static(LOGIN_PATH)).into_response());

    match form {
        None => {
            let Some(_) = sqlx::query_scalar!(
                r#"
                    SELECT 1
                    FROM management_users
                    WHERE complete AND email = $1 AND password_reset_secret = $2
                "#,
                &params.email,
                &params.secret,
            )
            .fetch_optional(&ctx.db)
            .await?
            else {
                return to_login();
            };

            ctx.html_template("manage/password_reset_form.html", &params)
        },
        Some(ref form) => {
            let mut transaction = ctx.db.begin().await?;

            let user_id = sqlx::query_scalar!(
                r#"
                    UPDATE management_users
                    SET password_hash = $3, password_reset_secret = NULL
                    WHERE complete AND email = $1 AND password_reset_secret = $2
                    RETURNING id AS "id: UserId"
                "#,
                &params.email,
                &params.secret,
                &form.password.hash()?,
            )
            .fetch_optional(&mut *transaction)
            .await?;

            if let Some(user_id) = user_id {
                let login_redirect = login_to_dashboard(user_id, &mut transaction).await?;
                transaction.commit().await?;
                Ok(login_redirect)
            } else {
                transaction.commit().await?;
                to_login()
            }
        },
    }
}

const DASHBOARD_PATH: &str = "/manage/app/dashboard/";
const LOGIN_PATH: &str = "/manage/login";

pub fn app(ctx: StaticContext) -> filter!((Response,)) {
    let handled_here = router! {
        in ctx;
        recover handle_endpoint_rejections_human;

        let manage_login = / "manage" / "login" {
            #[head, get] !any .map(|_| redirect::see_other(Uri::from_static(DASHBOARD_PATH)));
            #%form<LoginForm> =ctx do handle_login(2);
        };

        let manage_logout = / "manage" / "logout" {
            #post .and(with_login_unchecked()) .and_then(move |login| auto_reject(async move {
                Ok(with_cookies(
                    [logout(ctx, login).await?],
                    redirect::see_other(Uri::from_static(LOGIN_PATH)),
                ))
            }));
        };

        let manage_signup = / "manage" / "signup" ? SignupParams {
            #%form<SignupForm> =ctx do handle_signup(3);
        };

        let manage_reset_password = / "manage" / "setup-reset-password" {
            #post %form<LoginForm> .and_then(move |form: LoginForm| auto_reject(async move {
                reset_password(
                    ctx,
                    &mut *ctx.db.acquire().await?,
                    None,
                    Some(&form.email),
                    ResetPasswordReason::Forgotten
                ).await?;

                ctx.html_template("manage/password_reset.html", &json!({ "email": form.email }))
            }));
        };

        let manage_finalize_password_reset = / "manage" / "reset-password" ? ResetParams {
            #%form<ResetForm> =ctx do handle_finalize_reset(3);
        };
    };

    any_route_of![
        handled_here,
        // NOTE: If this needs to be changed, `DASHBOARD_PATH` needs to be changed also
        warp::path!("manage" / "app" / ..)
            .and(with_login(ctx).map(|_| ()))
            .and(
                warp::fs::dir(&ctx.cfg.manage_dir)
                    .or(warp::fs::file(ctx.full_manage_fallback()))
                    .unify()
            )
            .map(|(), out| out),
        warp::path!("manage" / ..)
            .and(maybe(with_login(ctx)))
            .map(
                move |login| redirect::see_other(Uri::from_static(match login {
                    None => LOGIN_PATH,
                    Some(_) => DASHBOARD_PATH,
                }))
            ),
    ]
}
