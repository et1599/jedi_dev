use std::{fmt, str::FromStr};

use sqlx::Type;

use crate::{
    email_debug,
    event::EventOverview,
    helpers::{Email, NonEmptyString, Password},
    manage::auth::LoginInfo,
    prelude::*,
    section::SectionOverview,
};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, Type, Debug, TS)]
#[sqlx(type_name = "USER_KIND")]
#[sqlx(rename_all = "kebab-case")]
#[serde(rename_all = "kebab-case")]
#[ts(export)]
pub enum UserKind {
    Normal,
    Organizer,
    Admin,
    SuperAdmin,
}

#[derive(Clone, Debug)]
// #[derive(Type)]
// #[sqlx(type_name = "BASIC_USER_INFO")]
pub struct BasicUserInfo {
    pub id: UserId,
    pub complete: bool,
    pub email: Email,
    pub display_name: Option<NonEmptyString>,
    pub kind: UserKind,
    pub admin_for: Vec<SectionId>,
}

#[allow(clippy::needless_borrows_for_generic_args)] // Not going to mess with SQLX internals
impl sqlx::encode::Encode<'_, sqlx::Postgres> for BasicUserInfo {
    fn encode_by_ref(&self, buf: &mut sqlx::postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
        let mut encoder = sqlx::postgres::types::PgRecordEncoder::new(buf);
        encoder.encode(&self.id);
        encoder.encode(&self.complete);
        encoder.encode(&self.email);
        encoder.encode(&self.display_name);
        encoder.encode(&self.kind);
        encoder.encode(&self.admin_for);
        encoder.finish();
        sqlx::encode::IsNull::No
    }

    fn size_hint(&self) -> usize {
        6usize * (4 + 4)
            + <UserId as sqlx::encode::Encode<sqlx::Postgres>>::size_hint(&self.id)
            + <bool as sqlx::encode::Encode<sqlx::Postgres>>::size_hint(&self.complete)
            + <Email as sqlx::encode::Encode<sqlx::Postgres>>::size_hint(&self.email)
            + <Option<NonEmptyString> as sqlx::encode::Encode<sqlx::Postgres>>::size_hint(
                &self.display_name,
            )
            + <UserKind as sqlx::encode::Encode<sqlx::Postgres>>::size_hint(&self.kind)
            + <Vec<SectionId> as sqlx::encode::Encode<sqlx::Postgres>>::size_hint(&self.admin_for)
    }
}

impl<'r> sqlx::decode::Decode<'r, sqlx::Postgres> for BasicUserInfo {
    fn decode(
        value: sqlx::postgres::PgValueRef<'r>,
    ) -> Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
        let mut decoder = sqlx::postgres::types::PgRecordDecoder::new(value)?;
        let id = decoder.try_decode::<UserId>()?;
        let complete = decoder.try_decode::<bool>()?;
        let email = decoder.try_decode::<Email>()?;
        let display_name = decoder.try_decode::<Option<NonEmptyString>>()?;
        let kind = decoder.try_decode::<UserKind>()?;
        let admin_for = decoder.try_decode::<Vec<SectionId>>()?;
        Ok(BasicUserInfo {
            id,
            complete,
            email,
            display_name,
            kind,
            admin_for,
        })
    }
}

impl Type<sqlx::Postgres> for BasicUserInfo {
    fn type_info() -> sqlx::postgres::PgTypeInfo {
        sqlx::postgres::PgTypeInfo::with_name("BASIC_USER_INFO")
    }
}

pub fn split_users(
    users: impl IntoIterator<Item = BasicUserInfo>,
    login_user_kind: UserKind,
) -> (Vec<UserOverview>, Vec<PartialUserOverview>) {
    let mut complete = Vec::new();
    let mut partial = Vec::new();
    for i in users {
        let base = BaseUserOverview {
            id: i.id,
            email: Some(i.email).filter(|_| login_user_kind == UserKind::SuperAdmin),
            kind: i.kind,
            admin_for: i.admin_for,
        };
        if i.complete {
            complete.push(UserOverview {
                base,
                display_name: i
                    .display_name
                    .expect("Missing display_name in complete user"),
            });
        } else {
            partial.push(PartialUserOverview {
                base,
                display_name: i.display_name,
            });
        }
    }

    (complete, partial)
}

#[derive(Deserialize, TS)]
#[ts(export)]
pub struct Invitation {
    #[serde(default)]
    pub display_name: Option<NonEmptyString>,
    pub email: Email,
}

#[derive(Serialize)]
#[serde(rename_all = "kebab-case", tag = "reason")]
pub enum SignupReason {
    AddedBySuperAdmin,
    NewOrganizer {
        event: EventOverview,
    },
    NewHelper {
        event: EventOverview,
        section: SectionOverview,
    },
    NewHead {
        section: SectionOverview,
    },
}

#[derive(Deserialize, Serialize)]
pub struct SignupParams {
    pub email: Email,
    pub secret: NonEmptyString,
}

#[derive(Serialize)]
pub struct SignupEmailContext {
    pub signup_params: SignupParams,
    pub display_name: Option<NonEmptyString>,
    #[serde(flatten)]
    pub reason: SignupReason,
}

pub async fn invite(
    transaction: &mut sqlx::Transaction<'_, sqlx::Postgres>,
    mut invitations: Vec<Invitation>,
    kind: UserKind,
) -> Result<Vec<UserId>, Error> {
    let display_names = invitations
        .iter_mut()
        .map(|i| i.display_name.take())
        .collect::<Vec<_>>();
    let emails = invitations.into_iter().map(|i| i.email).collect::<Vec<_>>();

    let ids = sqlx::query_scalar!(
        r#"
            -- We're getting VEEERY lucky here in this use case.
            INSERT INTO management_users(email, kind, partial_secret, display_name)
            SELECT
                email,
                $3 AS kind,
                encode(gen_random_bytes(32), 'hex') AS partial_secret,
                display_name
            FROM UNNEST(CAST($1 AS TEXT[]), CAST($2 AS TEXT[])) AS input(display_name, email)
            -- because we do actually have an update we want to do on conflicts, and
            ON CONFLICT (email) DO UPDATE
            SET kind = CASE  -- upgrade user kind if necessary
                    WHEN management_users.kind = 'normal' THEN EXCLUDED.kind
                    WHEN management_users.kind = 'organizer' AND EXCLUDED.kind <> 'normal'
                        THEN EXCLUDED.kind
                    WHEN management_users.kind = 'admin' AND EXCLUDED.kind = 'super-admin'
                        THEN EXCLUDED.kind
                    ELSE management_users.kind
                END,
                display_name = COALESCE(management_users.display_name, EXCLUDED.display_name)
            -- because `complete` is all we need to know regarding sending emails, as we
            -- *want* to send multiple *signup* emails for multiple events.
            -- If that wasn't the case we'd need some way of knowing whether we actually
            -- added a new user here, which is a little difficult in postgresql.
            RETURNING id AS "id: UserId"

            -- If we didn't do an update, `RETURNING` would not include those rows.
            -- (though that can be hacked away by setting a row to its current value
            -- again)
            -- If we split this up into separate queries (which would be the simple
            -- solution to wanting to know which ones we actually added), we'd have the
            -- problem of the whole operation spanning multiple queries, which could end
            -- up making other transactions potentially fail, and we're not retrying any
            -- currently. The other option would just be locking the full table, but
            -- that's also not ideal.
        "#,
        &display_names as &[Option<NonEmptyString>],
        &emails as &[Email],
        kind as UserKind,
    )
    .fetch_all(&mut **transaction)
    .await?;

    Ok(ids)
}

#[derive(Serialize, TS, Macrow)]
pub struct BaseUserOverview {
    pub id: UserId,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<Email>,
    pub kind: UserKind,
    pub admin_for: Vec<SectionId>,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct UserOverview {
    #[serde(flatten)]
    #[macrow(flatten)]
    pub base: crate::management_user::BaseUserOverview,
    pub display_name: NonEmptyString,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct PartialUserOverview {
    #[serde(flatten)]
    #[macrow(flatten)]
    pub base: crate::management_user::BaseUserOverview,
    pub display_name: Option<NonEmptyString>,
}

#[derive(Deserialize, TS)]
#[ts(export)]
pub struct CreateUser {
    pub email: Email,
    #[serde(default)]
    pub display_name: Option<NonEmptyString>,
    pub kind: UserKind,
    #[serde(default)]
    pub admin_for: Vec<SectionId>,
}

#[derive(Deserialize)]
#[serde(try_from = "String")]
pub enum UserIdOrSelf {
    Self_,
    Other(UserId),
}

impl UserIdOrSelf {
    pub fn id_in(self, login: &LoginInfo) -> UserId {
        match self {
            UserIdOrSelf::Self_ => login.user_id,
            UserIdOrSelf::Other(other) => other,
        }
    }
}

impl fmt::Display for UserIdOrSelf {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            UserIdOrSelf::Self_ => write!(f, "self"),
            UserIdOrSelf::Other(id) => write!(f, "{id}"),
        }
    }
}

impl FromStr for UserIdOrSelf {
    type Err = <UserId as FromStr>::Err;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        if value == "self" {
            Ok(UserIdOrSelf::Self_)
        } else {
            Ok(UserIdOrSelf::Other(value.parse()?))
        }
    }
}

impl TryFrom<String> for UserIdOrSelf {
    type Error = <UserId as FromStr>::Err;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        value.parse()
    }
}

#[derive(Serialize, TS)]
#[ts(export)]
#[serde(rename_all = "kebab-case")]
pub enum UserEventSectionReason {
    Head,
    Helper,
}

impl TryFrom<String> for UserEventSectionReason {
    type Error = sqlx::Error;

    fn try_from(value: String) -> Result<Self, sqlx::Error> {
        Ok(if value == "head" {
            UserEventSectionReason::Head
        } else {
            UserEventSectionReason::Helper
        })
    }
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct UserEventSectionOverview {
    #[macrow(flatten)]
    pub event: crate::event::EventOverview,
    #[macrow(flatten)]
    pub section: crate::section::SectionOverview,
    #[macrow(try_from)]
    pub reason: UserEventSectionReason,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct DbUser {
    pub id: UserId,
    pub email: Email,
    pub display_name: Option<NonEmptyString>,
    pub kind: UserKind,
    pub admin_for: Vec<SectionId>,
}

#[derive(Serialize, TS)]
#[ts(export)]
pub struct User {
    #[serde(flatten)]
    pub db_user: DbUser,
    pub relevant_events: Vec<EventOverview>,
    pub relevant_event_sections: Vec<UserEventSectionOverview>,
}

diff! {
    pub struct UserDiff {
        pub email: Option<Email>,
        pub display_name: Option<NonEmptyString>,
        pub password: Option<Password>,
        pub kind: Option<UserKind>,
        pub admin_for: Vec<SectionId>,
    }
}

#[derive(Deserialize, Serialize, Macrow)]
pub struct ResetParams {
    pub email: Email,
    pub secret: NonEmptyString,
}

#[derive(Clone, Copy, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum ResetPasswordReason {
    Forgotten,
    RemovedBySuperUser,
}

pub async fn reset_password(
    ctx: StaticContext,
    db: &mut sqlx::PgConnection,
    user_filter: Option<UserId>,
    email_filter: Option<&Email>,
    reason: ResetPasswordReason,
) -> Result<usize, Error> {
    #[derive(Serialize, Macrow)]
    struct TemplateContextDb {
        #[macrow(flatten)]
        reset_params: crate::management_user::ResetParams,
        display_name: NonEmptyString,
    }

    let reset_info = sqlx::query!(
        r#"
            UPDATE management_users
            SET password_reset_secret = encode(gen_random_bytes(32), 'hex'),
                password_hash = CASE WHEN $3 THEN NULL ELSE password_hash END
            WHERE email = COALESCE($1, email) AND id = COALESCE($2, id)
            RETURNING
                email AS "reset_params_email: Email",
                password_reset_secret AS "reset_params_secret!: NonEmptyString",
                display_name AS "display_name!: NonEmptyString"
        "#,
        email_filter.map(|x| &**x),
        user_filter as Option<UserId>,
        matches!(reason, ResetPasswordReason::RemovedBySuperUser),
    )
    .try_map(|x| TemplateContextDb!(x))
    .fetch_all(db)
    .await?;

    let reset_info_len = reset_info.len();

    ctx.spawn_and_send_emails(
        "email/manage/reset-password.html",
        reset_info.into_iter().map(move |row| {
            #[derive(Serialize)]
            struct TemplateContext {
                #[serde(flatten)]
                db: TemplateContextDb,
                reason: ResetPasswordReason,
            }

            email_debug::log(
                &row.reset_params.email,
                "password-reset",
                &row.reset_params.secret,
            );

            Ok((
                row.reset_params.email.0.parse()?,
                TemplateContext { db: row, reason },
                None,
            ))
        }),
    );

    Ok(reset_info_len)
}
