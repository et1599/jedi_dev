use validator::ValidateEmail;

use crate::{
    helpers::{Email, NonEmptyString},
    markdown::{RenderedMarkdown, UnrenderedMarkdown},
    prelude::*,
    DATE_FORMAT,
};

#[derive(Serialize, Deserialize, TS)]
#[serde(rename_all = "kebab-case", tag = "type")]
#[ts(export)]
pub enum FormOutput {
    Markdown(UnrenderedMarkdown),
}

#[derive(Serialize, Deserialize, TS)]
#[serde(rename_all = "kebab-case", tag = "type")]
#[ts(export)]
pub enum FormInputType {
    CheckBox {
        default: bool,
        required: bool,
    },
    Date {
        #[ts(type = "string | null")]
        default: Option<Date>,
        required: bool,
    },
    Email {
        default: Option<Email>,
        required: bool,
    },
    Number {
        default: Option<i32>,
        required: bool,
    },
    Radio {
        // implicitly required through usability concerns
        values: Vec<NonEmptyString>,
        default: Option<NonEmptyString>,
    },
    Select {
        // implicitly required because it doesn't work any other way
        values: Vec<NonEmptyString>,
        default: NonEmptyString,
    },
    Telephone {
        default: Option<NonEmptyString>,
        required: bool,
    },
    Text {
        default: Option<NonEmptyString>,
        required: bool,
    },
}

impl FormInputType {
    pub(super) fn required(&self) -> bool {
        #[allow(clippy::match_same_arms)] // for better readability due to different reasons
        match *self {
            // there is no "unset" version of a checkbox that doesn't just map to `false`
            FormInputType::CheckBox { .. } => true,
            // these are both implicitly required
            FormInputType::Radio { .. } | FormInputType::Select { .. } => true,
            FormInputType::Date { required, .. }
            | FormInputType::Email { required, .. }
            | FormInputType::Number { required, .. }
            | FormInputType::Telephone { required, .. }
            | FormInputType::Text { required, .. } => required,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct FormInput {
    pub label: NonEmptyString,
    pub can_edit: bool,
    #[serde(flatten)]
    pub description: FormInputType,
}

impl TS for FormInput {
    const EXPORT_TO: Option<&'static str> = Some("bindings/FormInput.ts");

    fn decl() -> String {
        format!("type FormInput = {}", Self::inline())
    }

    fn name() -> String {
        "FormInput".to_owned()
    }

    fn inline() -> String {
        "{ label: string, can_edit: boolean } & FormInputType".to_owned()
    }

    fn dependencies() -> Vec<ts_rs::Dependency> {
        vec![ts_rs::Dependency::from_ty::<FormInputType>().unwrap()]
    }

    fn transparent() -> bool {
        false
    }
}

#[cfg(test)]
#[test]
fn export_bindings_forminput() {
    <FormInput as TS>::export().expect("could not export type");
}

#[derive(Serialize, Deserialize, TS)]
#[serde(untagged)]
#[ts(export)]
pub enum FormItem {
    Output(FormOutput),
    Input(FormInput),
}

macro_rules! json_list {
    ($ty:ident) => {
        impl TryFrom<Vec<serde_json::Value>> for $ty {
            type Error = sqlx::Error;

            fn try_from(value: Vec<serde_json::Value>) -> Result<Self, sqlx::Error> {
                value
                    .into_iter()
                    .map(serde_json::from_value)
                    .collect::<Result<_, _>>()
                    .map_err(|x| $crate::db::sqlx_err(&x))
                    .map($ty)
            }
        }

        impl<'a> TryFrom<&'a $ty> for Vec<serde_json::Value> {
            type Error = $crate::Error;

            fn try_from(value: &'a $ty) -> Result<Self, $crate::Error> {
                value
                    .0
                    .iter()
                    .map(serde_json::to_value)
                    .collect::<Result<_, _>>()
                    .map_err($crate::Error::GeneratedJson)
            }
        }
    };
}

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
pub struct Form(pub Vec<FormItem>);

impl Form {
    pub fn inputs(&self) -> impl Iterator<Item = &FormInput> {
        self.0.iter().filter_map(|i| match *i {
            FormItem::Output(_) => None,
            FormItem::Input(ref i) => Some(i),
        })
    }
}

json_list!(Form);

#[derive(Clone, PartialEq, Eq, Serialize, Deserialize, TS)]
#[serde(untagged)]
#[ts(export)]
pub enum Value {
    Boolean(bool),
    String(String),
    Number(i64),
}

#[derive(Serialize, Deserialize, TS)]
#[ts(export)]
pub struct FilledForm(pub Vec<Option<Value>>);

json_list!(FilledForm);

#[derive(Clone, Serialize, Debug)]
#[serde(rename_all = "kebab-case", tag = "type")]
pub enum FormValidationErrorKind {
    Missing,
    InvalidValue { reason: String },
    ChangeForbidden,
}

#[derive(Clone, Serialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct FormValidationError {
    pub label: NonEmptyString,
    #[serde(flatten)]
    pub kind: FormValidationErrorKind,
}

impl FilledForm {
    pub fn validate_form(
        form: &Form,
        change_from: Option<&FilledForm>,
        html_form: &HashMap<usize, String>,
    ) -> Result<Self, Vec<FormValidationError>> {
        let mut state = Ok(Vec::with_capacity(form.0.len()));

        for (idx, i) in form.inputs().enumerate() {
            let value = html_form.get(&idx).map(|x| &**x).filter(|x| !x.is_empty());
            let original_value = change_from
                .as_ref()
                .and_then(|change_from| change_from.0[idx].as_ref());

            macro_rules! stringy {
                ($parse:expr) => {{
                    #[allow(clippy::string_to_string)]
                    let out = value
                        .map(|v| $parse(v).map(|_| Value::String(v.to_owned())))
                        .transpose()
                        .map_err(|x| x.to_string());
                    out
                }};
            }

            let value = match i.description {
                FormInputType::CheckBox { required, .. } => match value {
                    Some("on") => Ok(Some(Value::Boolean(true))),
                    Some(_) => Err("Checkbox value couldn't be converted".to_owned()),
                    None if !required => Ok(Some(Value::Boolean(false))),
                    None => Ok(None),
                },
                FormInputType::Date { .. } => stringy!(|date| Date::parse(date, &DATE_FORMAT)),
                FormInputType::Email { .. } => stringy!(|email: &str| email
                    .validate_email()
                    .then_some(())
                    .ok_or_else(|| format!("Email couldn't be validated: {email}"))),
                FormInputType::Number { .. } => value
                    .map(|v| v.parse().map(Value::Number))
                    .transpose()
                    .map_err(|x| x.to_string()),
                FormInputType::Radio { ref values, .. }
                | FormInputType::Select { ref values, .. } => {
                    stringy!(|option| values
                        .iter()
                        .any(|valid_value| option == &**valid_value)
                        .then_some(())
                        .ok_or_else(|| format!("Value should be one of: {values:?}")))
                },
                FormInputType::Telephone { .. } | FormInputType::Text { .. } => {
                    Ok(value.map(|x| Value::String(x.to_owned())))
                },
            };

            let value = if i.description.required() && matches!(value, Ok(None)) {
                original_value
                    .filter(|_| !i.can_edit)
                    .map(|original| Some(original.clone()))
                    .ok_or(FormValidationErrorKind::Missing)
            } else {
                value.map_err(|reason| FormValidationErrorKind::InvalidValue { reason })
            };

            let value = match (original_value, value) {
                (Some(orig), Ok(Some(val))) if !i.can_edit && *orig != val => {
                    Err(FormValidationErrorKind::ChangeForbidden)
                },
                (_, value) => value,
            };

            let value = value.map_err(|kind| FormValidationError {
                label: i.label.clone(),
                kind,
            });

            match (&mut state, value) {
                (&mut Ok(ref mut inputs), Ok(value)) => inputs.push(value),
                (&mut Ok(_), Err(err)) => state = Err(vec![err]),
                (&mut Err(_), Ok(_)) => (),
                (&mut Err(ref mut errs), Err(err)) => errs.push(err),
            };
        }

        state.map(FilledForm)
    }
}

#[derive(Serialize)]
#[serde(rename_all = "kebab-case", tag = "type")]
pub enum RenderedFormOutput {
    Markdown(RenderedMarkdown),
}

#[derive(Serialize)]
pub struct RenderedFormInput {
    pub data_id: usize,
    #[serde(flatten)]
    pub inner: FormInput,
}

#[derive(Serialize)]
#[serde(untagged)]
pub enum RenderedFormItem {
    Output(RenderedFormOutput),
    Input(RenderedFormInput),
}

#[derive(Serialize)]
pub struct RenderedForm(Vec<RenderedFormItem>);

impl From<Form> for RenderedForm {
    fn from(value: Form) -> Self {
        let mut data_id = 0;

        RenderedForm(
            value
                .0
                .into_iter()
                .map(|i| match i {
                    FormItem::Output(FormOutput::Markdown(unrendered)) => RenderedFormItem::Output(
                        RenderedFormOutput::Markdown(RenderedMarkdown::render(unrendered.source)),
                    ),
                    FormItem::Input(input) => {
                        data_id += 1;
                        RenderedFormItem::Input(RenderedFormInput {
                            data_id: data_id - 1,
                            inner: input,
                        })
                    },
                })
                .collect(),
        )
    }
}

impl TryFrom<Vec<JsonValue>> for RenderedForm {
    type Error = sqlx::Error;

    fn try_from(value: Vec<JsonValue>) -> Result<Self, sqlx::Error> {
        Form::try_from(value).map(RenderedForm::from)
    }
}

pub fn form_data_map(form_ext: HashMap<String, String>) -> HashMap<usize, String> {
    form_ext
        .into_iter()
        .filter_map(|(k, v)| {
            k.strip_prefix("form[")
                .and_then(|k| k.strip_suffix(']'))
                .and_then(|k| k.parse().ok())
                .map(|k| (k, v))
        })
        .collect()
}
