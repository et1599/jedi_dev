use std::{
    borrow::Cow,
    collections::HashMap,
    convert::Infallible,
    fmt,
    str::FromStr,
    sync::{Arc, RwLock},
};

use cookie::Cookie;
use either::{Either, Left, Right};
use percent_encoding::{percent_decode_str, utf8_percent_encode, NON_ALPHANUMERIC};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use warp::{
    filters::method,
    hyper::{
        self,
        header::{self, HeaderValue},
    },
    Filter, Rejection, Reply,
};

use crate::{prelude::*, OriginMode};

#[cfg(any(debug_assertions, feature = "debug"))]
pub fn maybe_box_filter<O: Send + 'static, E: Into<Rejection>>(
    filter: filter!((O,), E),
) -> filter!((O,), Rejection) {
    filter.boxed()
}

#[cfg(not(any(debug_assertions, feature = "debug")))]
#[allow(opaque_hidden_inferred_bound)]
#[inline(always)]
pub fn maybe_box_filter<O: Send + 'static, E: Into<Rejection>>(
    filter: filter!((O,), E),
) -> filter!((O,), E) {
    filter
}

macro_rules! any_route_of {
    [@split [] [] [$a:expr, $b:expr, $($rest:expr,)*]] => {
        any_route_of![@split [$a,] [$b,] [$($rest,)*]]
    };
    [
        @split
            [$($left:expr,)*] [$head:expr, $($tail:expr,)*] [$a:expr, $b:expr, $($rest:expr,)*]
    ] => {
        any_route_of![@split [$($left,)* $head,] [$($tail,)* $a, $b,] [$($rest,)*]]
    };
    [@split [$($left:expr,)*] [$($right:expr,)*] [$($last:expr,)?]] => {
        any_route_of![$($left,)*].or(any_route_of![$($right,)* $($last,)?]).unify()
    };
    [$route:expr $(,)?] => {
        $crate::warp_ext::maybe_box_filter(
            $route.map(|response| warp::reply::Reply::into_response(response))
        )
    };
    [$($route:expr),+ $(,)?] => {
        any_route_of![@split [] [] [$($route,)+]]
    };
}

#[derive(PartialEq, Eq)]
pub enum Sensitivity {
    Sensitive,
    NotSensitive,
}

pub use Sensitivity::*;

pub fn remove_cookie<'c>(name: impl Into<Cow<'c, str>>) -> Cookie<'c> {
    Cookie::build(name.into()).removal().build()
}

#[allow(single_use_lifetimes)] // doesn't work otherwise
pub fn with_cookies<'a, R: Reply>(
    cookies: impl IntoIterator<Item = (Cookie<'a>, Sensitivity)>,
    inner: R,
) -> impl Reply {
    let mut out = inner.into_response();
    let headers = out.headers_mut();

    let cookies = cookies.into_iter();
    headers.reserve(cookies.size_hint().0);

    for (mut i, sensitivity) in cookies {
        i.set_path("/");
        let mut header = HeaderValue::from_str(&i.encoded().to_string()).unwrap();
        if sensitivity == Sensitive {
            header.set_sensitive(true);
        }

        headers.append(header::SET_COOKIE, header);
    }

    out
}

#[derive(Debug, Serialize, Deserialize)]
#[repr(transparent)]
pub struct PercentEncoded<T>(pub T);

impl<T: FromStr> FromStr for PercentEncoded<T> {
    type Err = Either<std::str::Utf8Error, T::Err>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let encoded = percent_decode_str(s).decode_utf8().map_err(Left)?;
        Ok(PercentEncoded(T::from_str(&encoded).map_err(Right)?))
    }
}

impl<T: fmt::Display> fmt::Display for PercentEncoded<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use fmt::Write;

        struct FmtHelper<'a, 'b>(&'a mut fmt::Formatter<'b>);

        impl Write for FmtHelper<'_, '_> {
            fn write_str(&mut self, s: &str) -> fmt::Result {
                for i in utf8_percent_encode(s, NON_ALPHANUMERIC) {
                    self.0.write_str(i)?;
                }
                Ok(())
            }
        }

        write!(FmtHelper(f), "{}", self.0)
    }
}

pub trait UriHelperFn:
    Fn(OriginMode, &[handlebars::PathAndJson<'_>]) -> Option<String> + Send + Sync
{
}

impl<F> UriHelperFn for F where
    F: Fn(OriginMode, &[handlebars::PathAndJson<'_>]) -> Option<String> + Send + Sync
{
}

#[derive(Default, Clone)]
pub struct Uris {
    endpoints: Arc<RwLock<HashMap<&'static str, Box<dyn UriHelperFn>>>>,
}

impl Uris {
    pub fn new() -> Self {
        Uris::default()
    }

    pub fn insert(&self, key: &'static str, func: impl UriHelperFn + 'static) {
        self.endpoints.write().unwrap().insert(key, Box::new(func));
    }
}

pub struct UrisWithOrigin(pub Uris);

mod uris_impls {
    use handlebars::{
        Context, Handlebars, Helper, HelperDef, RenderContext, RenderError, RenderErrorReason,
        ScopedJson,
    };

    use super::{OriginMode, Uris, UrisWithOrigin};

    #[allow(clippy::significant_drop_in_scrutinee)]
    fn inner<'rc>(
        this: &Uris,
        h: &Helper<'rc>,
        fill_origin: OriginMode,
    ) -> Result<ScopedJson<'rc>, RenderError> {
        let name = h
            .param(0)
            .and_then(|v| v.value().as_str())
            .ok_or_else(|| RenderErrorReason::Other("No URI name given".into()))?;

        match this.endpoints.read().unwrap().get(name) {
            None => {
                Err(RenderErrorReason::Other(format!("The endpoint {name} does not exist")).into())
            },
            Some(func) => match func(fill_origin, &h.params()[1..]) {
                None => Err(RenderErrorReason::Other(format!(
                    "The arguments ({:?}) do not work for the endpoint {}",
                    &h.params()[1..],
                    name,
                ))
                .into()),
                Some(uri) => Ok(serde_json::to_value(uri)
                    .map_err(RenderErrorReason::SerdeError)?
                    .into()),
            },
        }
    }

    impl HelperDef for Uris {
        fn call_inner<'reg: 'rc, 'rc>(
            &self,
            h: &Helper<'rc>,
            _: &'reg Handlebars<'reg>,
            _: &'rc Context,
            _: &mut RenderContext<'reg, 'rc>,
        ) -> Result<ScopedJson<'rc>, RenderError> {
            inner(self, h, OriginMode::IfPresent)
        }
    }

    impl HelperDef for UrisWithOrigin {
        fn call_inner<'reg: 'rc, 'rc>(
            &self,
            h: &Helper<'rc>,
            _: &'reg Handlebars<'reg>,
            _: &'rc Context,
            _: &mut RenderContext<'reg, 'rc>,
        ) -> Result<ScopedJson<'rc>, RenderError> {
            inner(&self.0, h, OriginMode::OrLocalhost)
        }
    }
}

macro_rules! router {
    (@hbs $params:ident, $out:ident, (index) $(? $($rest:tt)*)?) => {
        #[allow(unused_imports)]
        use std::fmt::Write as _;
        write!(&mut $out, "/").unwrap();
        router!(@hbs $params, $out, $(? $($rest)*)?);
    };
    (@hbs $params:ident, $out:ident, ($first:literal) $($rest:tt)*) => {
        #[allow(unused_imports)]
        use std::fmt::Write as _;
        write!(&mut $out, "/{}", $first).unwrap();
        router!(@hbs $params, $out, $($rest)*);
    };
    (@hbs $params:ident, $out:ident, (($first:ty)) $($rest:tt)*) => {
        router!(@hbs $params, $out, ($first) $($rest)*)
    };
    (@hbs $params:ident, $out:ident, ($first:ty) $($rest:tt)*) => {
        match $params {
            &[ref first, ref rest @ ..] => {
                #[allow(unused_imports)]
                use std::fmt::Write as _;
                let Ok(first): Result<$first, _> = serde_json::from_value(first.value().clone())
                else {
                    return None;
                };
                $params = rest;
                write!(&mut $out, "/{}", first).unwrap();
                router!(@hbs $params, $out, $($rest)*);
            },
            &[] => return None,
        }
    };
    (@hbs $params:expr, $out:ident, ? (($query:ty))) => { router!(@hbs $params, $out, ($query)) };
    (@hbs $params:expr, $out:ident, ? ($query:ty)) => {
        match $params {
            &[ref query] => {
                #[allow(unused_imports)]
                use std::fmt::Write as _;
                let Ok(query): Result<$query, _> = serde_json::from_value(query.value().clone())
                else {
                    return None;
                };
                $params = &[];
                // TODO: Maybe avoid allocation
                write!(&mut $out, "?{}", serde_qs::to_string(&query).ok()?).unwrap();
            },
            _ => return None,
        }
    };
    (@hbs $params:expr, $out:ident,) => { };
    (@path $($fragment:tt)/+ /) => { router!(@path $($fragment)/+) };
    (@path index) => { warp::path::end() };
    (@path $($fragment:tt)/+) => { warp::path!($($fragment)/+) };
    (@routeinit in $ctx:ident; $path:expr, $($query:expr)?; $($route:tt)*) => {
        router!(@route in $ctx; $path, $($query)?, [] {$path} noquery; $($route)*)
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; ;
    ) => {
        any_route_of![$($variant,)* $current]
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; ; $($rest:tt)+
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant,)* $current]
            {$path} noquery; $($rest)+
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} $state:ident; #[$first:ident $(, $method:ident)* $(,)?] $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and(
                ::warp::filters::method::$first()
                $(.or(::warp::filters::method::$method()).unify())*
            )} $state; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} $state:ident; #$method:ident $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current} $state; #[$method] $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $query:expr, [$($variant:expr),*]
        {$current:expr} noquery; !email_user $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $query, [$($variant),*]
            {$current.and(
                $query.and_then(move |params: $crate::email_user::Credentials| params.check($ctx))
            )} done; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; !any $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and($crate::manage::auth::with_login($ctx))} done; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; !$kind:ident $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and(
                $crate::manage::auth::with_login_kind(
                    $ctx,
                    $crate::management_user::UserKind::$kind
                )
            )} done; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; %$body:ident < $ty:ty > $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and(::warp::body::$body::<$ty>())} done; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; #%form < $ty:ty > $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and($crate::warp_ext::get_or_post_form::<$ty>())} done; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} $state:ident; =$const:tt $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and(::warp::any().map(move || $const))} $state; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} noquery; <.$method:ident($($args:tt)*) $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.$method($($args)*)} noquery; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; .$method:ident($($args:tt)*) $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.$method($($args)*)} done; $($rest)*
        )
    };
    (@doargs $handler:expr, 0) => { move || auto_reject($handler()) };
    (@doargs $handler:expr, 1) => { move |a| auto_reject($handler(a)) };
    (@doargs $handler:expr, 2) => { move |a, b| auto_reject($handler(a, b)) };
    (@doargs $handler:expr, 3) => { move |a, b, c| auto_reject($handler(a, b, c)) };
    (@doargs $handler:expr, 4) => { move |a, b, c, d| auto_reject($handler(a, b, c, d)) };
    (@doargs $handler:expr, 5) => { move |a, b, c, d, e| auto_reject($handler(a, b, c, d, e)) };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} done; do $handler:ident $(::$handler_path_ext:ident)* ($len:tt) $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current.and_then(
                router!(@doargs $handler $(::$handler_path_ext)*, $len)
            )} done; $($rest)*
        )
    };
    (
        @route in $ctx:ident; $path:expr, $($query:expr)?, [$($variant:expr),*]
        {$current:expr} noquery; $($rest:tt)*
    ) => {
        router!(
            @route in $ctx; $path, $($query)?, [$($variant),*]
            {$current $(.and($query))?} done; $($rest)*
        )
    };
    {
        in $ctx:ident; recover $recover:ident;
        $(let $name:ident = $(/ $frag:tt)+ $(? $query:tt)? {$($route:tt)*};)*
    } => {{
        $(
            #[allow(unused_mut)]
            $ctx.uris.insert(
                stringify!($name),
                |fill_origin, mut params| {
                    let mut out = $ctx.maybe_origin(fill_origin).to_owned();
                    router!(@hbs params, out, $(($frag))+ $(? ($query))?);
                    if params.len() != 0 {
                        return None;
                    }
                    Some(out)
                },
            );

            #[allow(unused_parens)]
            let $name = router!(
                @routeinit in $ctx; router!(@path $($frag)/+), $(::warp::query::<$query>())?;
                $($route)*
            )
            .recover(move |rejection| $crate::rejections::$recover($ctx, rejection));
        )*

        any_route_of![$($name),*]
    }};
}

pub fn method_get_ish() -> filter!(()) {
    method::get().or(method::head()).unify()
}

pub fn maybe<O: 'static>(f: filter!((O,))) -> filter!((Option<O>,), Infallible) {
    f.map(Some).or(warp::any().map(|| None)).unify()
}

pub fn percent_encode(s: &str) -> impl fmt::Display + '_ {
    utf8_percent_encode(s, NON_ALPHANUMERIC)
}

pub fn csv_response(filename: &str, output: &[u8]) -> EndpointResult {
    // https://web.archive.org/web/20160321175210/https://blog.fastmail.com/2011/06/24/download-non-english-filenames/
    let filename = percent_encode(filename).to_string();
    Ok(hyper::Response::builder()
        .header(header::CONTENT_TYPE, "text/csv")
        .header(
            header::CONTENT_DISPOSITION,
            format!("attachment; filename=\"{filename}\" filename*=UTF-8''{filename}"),
        )
        .body(String::from_utf8_lossy(output).into_owned())?
        .into_response())
}

pub fn get_or_post_form<T: DeserializeOwned + Send + Sync + 'static>() -> filter!((Option<T>,)) {
    method_get_ish()
        .map(|| None)
        .or(method::post().and(warp::body::form::<T>().map(Some)))
        .unify()
}
