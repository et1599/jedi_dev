use std::{
    collections::HashMap,
    env, fs,
    net::SocketAddr,
    num::NonZeroU32,
    path::{Path, PathBuf},
    process,
    sync::RwLock,
    time::Duration,
};

use bytes::Bytes;
use clap::Parser;
#[cfg(any(debug_assertions, feature = "debug"))]
use cookie::Cookie;
use handlebars::Handlebars;
use lettre::{
    message::{Mailbox, MultiPart, SinglePart},
    AsyncFileTransport as AsyncEmailFileTransport, AsyncSendmailTransport, AsyncSmtpTransport,
    AsyncTransport, Tokio1Executor,
};
use serde::Serialize;
use serde_json::Value;
use sqlx::postgres::PgPool;
use time::{OffsetDateTime, Time};
use time_tz::{system::get_timezone, timezones, OffsetDateTimeExt, Tz};
use tokio::{task::JoinHandle, time::sleep};
use uuid as _; // required to enable the `serde` feature
use warp::{
    body,
    filters::method,
    header,
    hyper::{
        header::{
            HeaderValue, ACCESS_CONTROL_ALLOW_ORIGIN, CONTENT_SECURITY_POLICY, REFERRER_POLICY,
            X_CONTENT_TYPE_OPTIONS, X_FRAME_OPTIONS,
        },
        Body, Method, StatusCode,
    },
    reject,
    reply::{self, Reply, Response},
    Filter,
};

macro_rules! filter {
    ($extract:ty, $error:ty $(, $($extra:tt)*)?) => {
        impl ::warp::Filter<Extract = $extract, Error = $error> + Clone + Send + Sync + 'static
            $(+ $($extra)*)?
    };
    ($extract:ty) => {
        filter!($extract, ::warp::Rejection)
    };
}

macro_rules! diff {
    {
        $(#[$attr:meta])*
        $vis:vis struct $name:ident {
            $(
                $(#[$($field_attr:tt)*])? $fieldvis:vis $field:ident: $ty:ty $(= $default:literal)?
            ),* $(,)?
        }
    } => {
        $(#[$attr])*
        #[derive(Deserialize, TS)]
        #[ts(export)]
        $vis struct $name {
            $(
                #[serde(default $(= $default)?)]
                $(#[$($field_attr)*])?
                $fieldvis $field: $ty,
            )*
        }
    };
}

macro_rules! forward_ts {
    {impl TS for $ty:ident = $inner:ty} => {
        impl ts_rs::TS for $ty {
            const EXPORT_TO: Option<&'static str> = <$inner>::EXPORT_TO;
            fn name() -> String { <$inner>::name() }
            fn dependencies() -> Vec<ts_rs::Dependency> { <$inner>::dependencies() }
            fn transparent() -> bool { <$inner>::transparent() }
            fn decl() -> String { <$inner>::decl() }
            fn name_with_type_args(args: Vec<String>) -> String {
                <$inner>::name_with_type_args(args)
            }
            fn inline() -> String { <$inner>::inline() }
            fn inline_flattened() -> String { <$inner>::inline_flattened() }
            fn export() -> Result<(), ts_rs::ExportError> { <$inner>::export() }
            fn export_to(path: impl AsRef<std::path::Path>) -> Result<(), ts_rs::ExportError> {
                <$inner>::export_to(path)
            }
            fn export_to_string() -> Result<String, ts_rs::ExportError> {
                <$inner>::export_to_string()
            }
        }
    };
}

mod prelude {
    pub use std::collections::HashMap;

    pub use macrow::Macrow;
    pub use serde::{Deserialize, Serialize};
    pub use serde_json::{json, Value as JsonValue};
    pub use time::Date;
    pub use ts_rs::TS;

    pub use crate::{
        db::{EventId, RequestId, SectionId, SessionId, UserId},
        event::form::Value as FormValue,
        rejections::{Error, UserError},
    };

    pub type StaticContext = &'static crate::AppContext;
    pub type EndpointResult = Result<warp::reply::Response, Error>;
}
use prelude::*;

// Support
#[macro_use]
mod warp_ext;

mod helpers;
use helpers::{Email, NonEmptyString};

mod rejections;

#[cfg(any(debug_assertions, feature = "debug"))]
mod email_debug;

#[cfg(not(any(debug_assertions, feature = "debug")))]
mod email_debug {
    pub fn log(_email: &crate::helpers::Email, _kind: &'static str, _payload: &str) {}
}

// Database
#[macro_use]
mod db;

#[macro_use]
mod markdown;
#[macro_use]
mod management_user;
#[macro_use]
mod event;
#[macro_use]
mod section;
#[macro_use]
mod event_section;

// Subapplications
#[macro_use]
mod manage;
#[macro_use]
mod api;
#[macro_use]
mod email_user;

use rejections::{
    auto_reject, handle_final_rejections, handle_task_error, CrossOriginFailure, Error,
    LettreSendError,
};

const REPEAT_HREF: &str = "%%REPEATHREF%%";

#[allow(clippy::empty_structs_with_brackets)]
#[derive(Serialize)]
struct EmptyStruct {}

#[cfg(windows)]
fn root_or_admin() -> bool {
    is_elevated::is_elevated()
}

#[cfg(not(windows))]
fn root_or_admin() -> bool {
    nix::unistd::Uid::effective().is_root()
}

const DATE_FORMAT: &[time::format_description::FormatItem] =
    time::macros::format_description!("[year]-[month]-[day]");

fn local_tz() -> &'static Tz {
    static TZ: RwLock<Option<&'static Tz>> = RwLock::new(None);

    {
        // optimistic
        let tz = TZ.read().unwrap();
        if let Some(tz) = *tz {
            return tz;
        }
    }

    {
        // try setting
        let mut tz = TZ.write().unwrap();

        tz.get_or_insert_with(|| {
            let out = match env::var("OVERRIDE_TZ") {
                Ok(name) => timezones::get_by_name(&name)
                    .ok_or_else(|| "Timezone in `OVERRIDE_TZ` wasn't found".to_owned()),
                Err(env::VarError::NotPresent) => get_timezone().map_err(|err| err.to_string()),
                Err(err @ env::VarError::NotUnicode(_)) => Err(err.to_string()),
            };

            out.unwrap_or_else(|err| {
                log::error!("Failed to get the local timezone: {err}");
                process::abort();
            })
        })
    }
}

fn local_now() -> OffsetDateTime {
    OffsetDateTime::now_utc().to_timezone(local_tz())
}

fn default_bind_addr() -> SocketAddr {
    if root_or_admin() {
        SocketAddr::new([0, 0, 0, 0].into(), 80)
    } else {
        SocketAddr::new([0, 0, 0, 0].into(), 8080)
    }
}

#[derive(Parser)]
struct Config {
    #[arg(long, env, default_value = "./static/")]
    static_dir: PathBuf,
    #[arg(long, env, default_value = "./templates/")]
    template_dir: PathBuf,
    #[arg(long, env, default_value = "./management/build/")]
    manage_dir: PathBuf,
    #[arg(long, env, default_value = "fallback.html")]
    manage_fallback: PathBuf,
    #[arg(long, env, default_value_t = true)]
    append_fallback: bool,
    #[arg(short = 'b', long, env, default_value_t = default_bind_addr())]
    bind_addr: SocketAddr,
    #[arg(short = 'H', long, env)]
    host: Option<String>,
    #[arg(short = 's', long, env)]
    scheme: Option<String>,
    #[arg(short = 'd', long, env)]
    database_url: String,
    #[arg(short = 'S', long, env)]
    smtp_url: Option<String>,
    #[arg(long, env, default_value_t = false)]
    use_sendmail: bool,
    #[arg(long, env)]
    sendmail_command: Option<String>,
    #[arg(long, env)]
    save_emls_to: Option<String>,
    #[arg(short = 'm', long, env)]
    emails_from: Option<String>,
}

#[derive(Default)]
struct Attachments {
    for_user: Vec<SinglePart>,
    for_html: Vec<SinglePart>,
}

struct EmailSender {
    smtp: Option<AsyncSmtpTransport<Tokio1Executor>>,
    sendmail: Option<AsyncSendmailTransport<Tokio1Executor>>,
    file: Option<AsyncEmailFileTransport<Tokio1Executor>>,
    from: Mailbox,
}

pub struct AppContext {
    cfg: Config,
    hbs: Handlebars<'static>,
    uris: warp_ext::Uris,
    db: PgPool,
    email: Option<EmailSender>,
    origin: String,
    auto_filled_origin: bool,
}

#[derive(PartialEq, Eq)]
pub enum OriginMode {
    OrLocalhost,
    IfPresent,
}

impl AppContext {
    fn maybe_origin(&self, mode: OriginMode) -> &str {
        match (self.auto_filled_origin, mode) {
            (_, OriginMode::OrLocalhost) | (false, OriginMode::IfPresent) => &self.origin,
            (true, OriginMode::IfPresent) => "",
        }
    }

    fn html_template<T: Serialize>(&self, name: &str, data: &T) -> Result<Response, Error> {
        Ok(reply::html(self.hbs.render(name, data)?).into_response())
    }

    async fn send_email<T: Serialize + Sync>(
        &self,
        name: &str,
        to: Mailbox,
        data: &T,
        attachments: Option<Attachments>,
    ) -> Result<Result<(), Vec<LettreSendError>>, Error> {
        use handlebars::Context;
        use scraper::{Html, Node, Selector};

        let Some(ref email_cfg) = self.email else {
            return Ok(Ok(()));
        };

        let attachments = attachments.unwrap_or_default();

        let mut context = Context::wraps(data)?;
        let set_in_map = |context: &mut Context, key: &str, value| match *context.data_mut() {
            Value::Object(ref mut out) => out.insert(key.to_owned(), value),
            _ => panic!("The context should serialize to an object"),
        };

        set_in_map(&mut context, "host", (&*self.origin).into());

        let (text, title) = {
            // we need to make sure `body` goes out of scope, otherwise this future isn't
            // `Send` because `body` isn't; and `mem::drop` doesn't work because
            // `body` is `Copy`.
            set_in_map(&mut context, "html", false.into());
            let uncleaned_text = self.hbs.render_with_context(name, &context)?;
            let text_document = Html::parse_document(&uncleaned_text);
            let select_or = |error: fn() -> _, selector: &'static str| {
                text_document
                    .select(&Selector::parse(selector).unwrap())
                    .next()
                    .ok_or_else(error)
            };
            let title = select_or(|| Error::MissingEmailTitle, "title")?
                .text()
                .fold(String::new(), |acc, x| {
                    format!("{acc}{}{x}", if acc.is_empty() { "" } else { " " })
                });
            let body = select_or(|| Error::MissingEmailBody, "body")?;

            let mut text = String::new();

            // 0 = don't ignore, otherwise the amount of `Close`s needed to stop ignoring
            let mut ignore_level = None;

            let mut hrefs = Vec::new();

            for i in body.traverse() {
                use ego_tree::iter::Edge::{Close, Open};
                match (i, ignore_level) {
                    (Open(node), None) => match *node.value() {
                        Node::Text(ref part) => {
                            let initial_whitespace =
                                part.chars().next().map_or(false, char::is_whitespace);
                            let final_whitespace =
                                part.chars().next_back().map_or(false, char::is_whitespace);

                            let part = part.trim();

                            if initial_whitespace {
                                let previous_whitespace =
                                    text.is_empty() || text.ends_with(' ') || text.ends_with('\n');
                                if !previous_whitespace {
                                    text.push(' ');
                                }
                            }

                            for (idx, i) in part.split_whitespace().enumerate() {
                                if idx != 0 {
                                    text.push(' ');
                                }

                                if let Some(&href) = hrefs.last() {
                                    text += &*i.replace(REPEAT_HREF, href);
                                } else {
                                    text += i;
                                }
                            }

                            if final_whitespace && !part.is_empty() {
                                text.push(' ');
                            }
                        },
                        Node::Element(ref elem) => match elem.name() {
                            "br" => {
                                if text.ends_with(' ') {
                                    text.pop();
                                }

                                text.push('\n');
                            },
                            "a" => {
                                if let Some(href) = elem.attr("href") {
                                    hrefs.push(href);
                                }
                            },
                            "style" | "script" => ignore_level = Some(NonZeroU32::new(1).unwrap()),
                            _ => (),
                        },
                        _ => (),
                    },
                    (Open(_), Some(ref mut level)) => *level = level.checked_add(1).unwrap(),
                    (Close(node), None) => match *node.value() {
                        Node::Element(ref elem) if elem.name() == "a" => {
                            if elem.attr("href").is_some() {
                                hrefs.pop();
                            }
                        },
                        _ => (),
                    },
                    (Close(_), Some(level)) => ignore_level = NonZeroU32::new(level.get() - 1),
                }
            }

            if text.ends_with(' ') {
                text.pop();
            }

            (text, title)
        };

        set_in_map(&mut context, "html", true.into());
        let html = self.hbs.render_with_context(name, &context)?;

        let mut inner_multipart = MultiPart::alternative().singlepart(SinglePart::plain(text));

        let html = SinglePart::html(html);
        if attachments.for_html.is_empty() {
            inner_multipart = inner_multipart.singlepart(html);
        } else {
            let mut html_multipart = MultiPart::related().singlepart(html);

            for i in attachments.for_html {
                html_multipart = html_multipart.singlepart(i);
            }

            inner_multipart = inner_multipart.multipart(html_multipart);
        }

        let multipart = if attachments.for_user.is_empty() {
            inner_multipart
        } else {
            let mut multipart = MultiPart::mixed().multipart(inner_multipart);

            for i in attachments.for_user {
                multipart = multipart.singlepart(i);
            }

            multipart
        };

        let email = lettre::Message::builder()
            .from(email_cfg.from.clone())
            .to(to)
            .subject(&*title)
            .multipart(multipart)
            .unwrap(); // TODO

        let mut errors = Vec::new();

        macro_rules! send_on {
            ($($transport:ident),*) => {
                $(
                    if let Some(ref transport) = email_cfg.$transport {
                        if let Err(error) = transport.send(email.clone()).await {
                            errors.push(error.into());
                        }
                    }
                )*
            }
        }

        send_on!(smtp, sendmail, file);

        if errors.is_empty() {
            Ok(Ok(()))
        } else {
            Ok(Err(errors))
        }
    }

    fn spawn_and_send_email<T: Serialize + Send + Sync + 'static>(
        &'static self,
        name: &'static str,
        to: Mailbox,
        data: T,
        attachments: Option<Attachments>,
    ) -> JoinHandle<()> {
        tokio::spawn(handle_task_error(async move {
            Ok(self.send_email(name, to, &data, attachments).await??)
        }))
    }

    fn spawn_and_send_emails<T, I>(&'static self, name: &'static str, emails: I) -> JoinHandle<()>
    where
        T: Serialize + Send + Sync + 'static,
        I: IntoIterator<Item = Result<(Mailbox, T, Option<Attachments>), Error>>,
        I::IntoIter: Send + 'static,
    {
        let emails = emails.into_iter();
        tokio::spawn(async move {
            for i in emails {
                handle_task_error(async move {
                    let (to, data, attachments) = i?;
                    Ok(self.send_email(name, to, &data, attachments).await??)
                })
                .await;
            }
        })
    }

    fn full_manage_fallback(&self) -> PathBuf {
        if self.cfg.append_fallback {
            self.cfg.manage_dir.join(&self.cfg.manage_fallback)
        } else {
            self.cfg.manage_fallback.clone()
        }
    }
}

#[allow(clippy::str_to_string)] // from `handlebars_helper!`
fn setup_handlebars(cfg: &Config) -> (Handlebars<'static>, warp_ext::Uris) {
    use handlebars::{handlebars_helper, DirectorySourceOptions, RenderErrorReason};
    use time::format_description as format;

    #[derive(Deserialize)]
    #[serde(untagged)]
    enum DateOrDateTime {
        Date(Date),
        DateTime(OffsetDateTime),
    }

    fn register(cfg: &Config, dir: &str, mut f: impl FnMut(&str, &Path)) {
        let entries = fs::read_dir(cfg.template_dir.join(dir))
            .unwrap_or_else(|_| panic!("Expected {dir} directory in the template directory"))
            .map(Result::unwrap)
            .filter(|i| i.file_type().unwrap().is_file());

        for i in entries {
            let file_name = i.file_name().into_string().unwrap();
            let bind_name = file_name
                .rsplit_once('.')
                .map_or(&*file_name, |(name, _)| name);
            f(bind_name, &i.path());
        }
    }

    let mut hbs = Handlebars::new();
    hbs.set_strict_mode(true);

    if cfg!(any(debug_assertions, feature = "debug")) {
        hbs.set_dev_mode(true);
    }

    let mut rhai = rhai::Engine::new();
    rhai.set_fail_on_invalid_map_property(true);
    hbs.set_engine(rhai);

    hbs.register_templates_directory(&cfg.template_dir, DirectorySourceOptions {
        tpl_extension: ".hbs".to_owned(),
        hidden: false,
        temporary: false,
    })
    .unwrap();

    macro_rules! try_in_hbs {
        ($x:expr, $descr:literal $(,)?) => {
            $x.map_err(|err| RenderErrorReason::Other(format!("{}: {}", $descr, err)))?
        };
    }

    register(cfg, "helpers", |name, path| {
        hbs.register_script_helper_file(name, path).unwrap();
    });

    handlebars_helper!(
        datetime: |x: DateOrDateTime, format: String| {
            let format = try_in_hbs!(format::parse(&format), "Couldn't parse the format");
            try_in_hbs!(
                match x {
                    DateOrDateTime::Date(x) => x.format(&format),
                    DateOrDateTime::DateTime(x) => x.format(&format),
                },
                "Couldn't format the date/time with the given format"
            )
        }
    );
    hbs.register_helper("datetime", Box::new(datetime));

    handlebars_helper!(
        now: | | serde_json::to_value(local_now()).map_err(RenderErrorReason::SerdeError)?
    );
    hbs.register_helper("now", Box::new(now));

    handlebars_helper!(
        parse_datetime: |x: String, format: String| serde_json::to_value(try_in_hbs!(
            OffsetDateTime::parse(
                &x,
                &try_in_hbs!(format::parse(&format), "Couldn't parse the format"),
            ),
            "Couldn't parse the date/time with the given format"
        )).map_err(RenderErrorReason::SerdeError)?
    );
    hbs.register_helper("parse_datetime", Box::new(parse_datetime));

    handlebars_helper!(
        timezone: |x: OffsetDateTime, tz: String| serde_json::to_value(x.to_timezone(
            timezones::get_by_name(&tz).ok_or_else(|| {
                RenderErrorReason::Other(format!("Couldn't find the timezone {tz}"))
            })?
        )).map_err(RenderErrorReason::SerdeError)?
    );
    hbs.register_helper("timezone", Box::new(timezone));

    handlebars_helper!(
        repeat_href: | | serde_json::to_value(REPEAT_HREF).map_err(RenderErrorReason::SerdeError)?
    );
    hbs.register_helper("repeat_href", Box::new(repeat_href));

    let uris = warp_ext::Uris::new();
    hbs.register_helper("uri", Box::new(uris.clone()));
    hbs.register_helper(
        "uri_with_origin",
        Box::new(warp_ext::UrisWithOrigin(uris.clone())),
    );

    register(cfg, "partials", |name, path| {
        // templates in handlebars are the same as partials
        hbs.register_template_file(name, path).unwrap();
    });

    (hbs, uris)
}

async fn trigger_daily_update(ctx: &'static AppContext) {
    #[derive(sqlx::Type, Debug)]
    struct Organizer {
        email: Email,
        display_name: NonEmptyString,
    }

    let completed_events = loop {
        let completed_events = async move {
            #[derive(Macrow)]
            struct CompletedEvent {
                #[macrow(flatten)]
                event: crate::event::EventOverview,
                guest_count: i32,
                positioned_count: i32,
                organizers: Vec<Organizer>,
                email_users: Vec<email_user::auth::Credentials>,
            }

            let mut transaction = ctx.db.begin().await?;

            let completed_events = sqlx::query!(
                r#"
                    UPDATE events
                    SET state = 'done',
                        signup_until = NULL, decisions_until = NULL,
                        archived_until = (SELECT NOW() + archive_length FROM meta)
                    WHERE state = 'active' AND NOW() > decisions_until
                    RETURNING
                        id AS "event_id: EventId",
                        name AS "event_name: NonEmptyString", description AS event_description,
                        state AS "event_state: event::EventState",
                        signup_until AS event_signup_until,
                        decisions_until AS event_decisions_until,
                        archived_until AS event_archived_until,
                        (
                            SELECT CAST(COUNT(requests.*) AS INTEGER)
                            FROM requests
                            WHERE filled AND event = events.id AND accepted_as_guest
                        ) AS "guest_count!",
                        (
                            SELECT CAST(COUNT(requests.*) AS INTEGER)
                            FROM requests
                            WHERE filled AND event = events.id AND position <> NULL
                        ) AS "positioned_count!",
                        ARRAY(
                            SELECT ROW(email, display_name)
                            FROM management_users JOIN event_organizers
                                ON management_users.id = event_organizers.organizer
                            WHERE event_organizers.event = events.id
                        ) AS "organizers!: Vec<Organizer>",
                        ARRAY(
                            SELECT DISTINCT ROW(email_users.email, email_users.secret)
                            FROM requests JOIN email_users ON requests.email = email_users.email
                            WHERE filled AND requests.event = events.id
                        ) AS "email_users!: Vec<email_user::auth::Credentials>"
                "#
            )
            .try_map(|x| CompletedEvent!(x))
            .fetch_all(&mut *transaction)
            .await?;

            sqlx::query!(r#"DELETE FROM events WHERE state = 'done' AND NOW() > archived_until"#)
                .execute(&mut *transaction)
                .await?;

            transaction.commit().await?;

            Ok::<_, sqlx::Error>(completed_events)
        }
        .await;

        match completed_events {
            Ok(completed_events) => break completed_events,
            Err(err) => {
                log::error!("Daily state updates: {err}");
                sleep(Duration::from_secs(10)).await;
                continue;
            },
        }
    };

    for i in completed_events {
        let event = i.event.clone();
        ctx.spawn_and_send_emails(
            "email/manage/organized-event-done.html",
            i.organizers.into_iter().map(move |row| {
                email_debug::log(
                    &row.email,
                    "organized-event-done",
                    &format!("{}:{}:{}", event.id, i.guest_count, i.positioned_count),
                );

                Ok((
                    row.email.parse()?,
                    json!({ "event": &event, "display_name": row.display_name }),
                    None,
                ))
            }),
        );

        let event = i.event;
        ctx.spawn_and_send_emails(
            "email/user/event-with-request-done.html",
            i.email_users.into_iter().map(move |row| {
                email_debug::log(&row.email, "event-with-request-done", &row.secret);

                Ok((
                    row.email.parse()?,
                    json!({ "event": &event, "login": &row }),
                    None,
                ))
            }),
        );
    }
}

async fn daily_updates(ctx: &'static AppContext) {
    let mut next_update_in = Duration::ZERO;
    loop {
        sleep(next_update_in).await;
        tokio::spawn(trigger_daily_update(ctx));

        let now = local_now();
        let next_update_system =
            now.replace_time(Time::from_hms(1, 0, 0).unwrap()) + time::Duration::DAY;
        next_update_in = Duration::try_from(next_update_system - now).unwrap();
    }
}

#[cfg(any(debug_assertions, feature = "debug"))]
async fn time_tick(ctx: &'static AppContext) -> EndpointResult {
    sqlx::query!(
        r#"
            UPDATE events
            SET signup_until = signup_until - CAST('1 day' AS INTERVAL),
                decisions_until = decisions_until - CAST('1 day' AS INTERVAL),
                archived_until = archived_until - CAST('1 day' AS INTERVAL);
        "#
    )
    .execute(&ctx.db)
    .await?;

    trigger_daily_update(ctx).await;

    Ok(StatusCode::OK.into_response())
}

fn force_same_origin_unless_get_ish(cfg: &'static AppContext) -> filter!(((),)) {
    method::get()
        .or(method::head())
        .or(header::optional::<String>("Origin")
            .and(header::optional::<String>("Referer"))
            .and_then(
                move |origin: Option<String>, referer: Option<String>| async move {
                    if !origin.as_ref().map_or(true, |x| x == &cfg.origin)
                        || !referer.as_ref().map_or(true, |x| {
                            x.starts_with(&cfg.origin)
                                && x.as_bytes().get(cfg.origin.len()) == Some(&b'/')
                        })
                    {
                        #[cfg(any(debug_assertions, feature = "debug"))]
                        {
                            log::warn!(
                                "Not the same origin: origin={origin:?} / referer={referer:?}"
                            );
                        }

                        Err(reject::custom(CrossOriginFailure))
                    } else {
                        Ok(())
                    }
                },
            ))
        .map(|_| ())
}

#[tokio::main]
async fn main() {
    // This is necessary to handle SIGINT & SIGTERM in docker containers where jedi
    // is run as the init process.
    ctrlc::set_handler(|| std::process::exit(130)).unwrap();

    pretty_env_logger::init_timed();

    match dotenvy::dotenv() {
        Ok(_) => (),
        Err(e) if e.not_found() => (),
        res @ Err(_) => {
            res.unwrap();
        },
    }

    let cfg = Config::parse();

    #[cfg(any(debug_assertions, feature = "debug"))]
    log::error!(target: "jedi", "RUNNING A DEV ENVIRONMENT, THIS IS NOT SAFE FOR PRODUCTION");

    let (hbs, uris) = setup_handlebars(&cfg);
    log::debug!(target: "jedi", "Handlebars initialised");

    let db = PgPool::connect(&cfg.database_url).await.unwrap();
    log::debug!(target: "jedi", "Database initialised");

    #[allow(clippy::if_then_some_else_none)] // there's an `.await` in there
    let email = if cfg.smtp_url.is_some() || cfg.use_sendmail || cfg.save_emls_to.is_some() {
        // TODO: Don't panic
        let from = cfg
            .emails_from
            .as_deref()
            .expect("`EMAILS_FROM` is required for sending emails")
            .parse()
            .expect("Invlaid value for `EMAILS_FROM`");
        Some(EmailSender {
            smtp: match cfg.smtp_url {
                Some(ref url) => {
                    // TODO: Maybe do connection pool stuff?

                    let transport = AsyncSmtpTransport::<Tokio1Executor>::from_url(url)
                        .unwrap()
                        .build(); // TODO
                    transport.test_connection().await.unwrap();

                    log::debug!(target: "jedi", "SMTP initialised");
                    Some(transport)
                },
                None => None,
            },
            sendmail: if cfg.use_sendmail {
                let transport = cfg
                    .sendmail_command
                    .as_ref()
                    .map_or_else(AsyncSendmailTransport::new, |command| {
                        AsyncSendmailTransport::new_with_command(&**command)
                    });

                log::debug!(target: "jedi", "Sendmail initialised");
                Some(transport)
            } else {
                if cfg.sendmail_command.is_some() {
                    log::warn!(
                        target: "jedi",
                        "`USE_SENDMAIL` is not set, but `SMTP_SENDMAIL_COMMAND`"
                    );
                }

                None
            },
            file: cfg.save_emls_to.as_deref().map(|directory| {
                let transport = AsyncEmailFileTransport::new(directory);
                log::debug!(target: "jedi", "File transport for emails initialised");
                transport
            }),
            from,
        })
    } else {
        None
    };
    log::debug!(target: "jedi", "Emails initialised");

    let (origin, auto_filled_origin) = if cfg!(any(debug_assertions, feature = "debug")) {
        (
            format!(
                "{}://{}",
                cfg.scheme.as_deref().unwrap_or("http"),
                cfg.host
                    .clone()
                    .unwrap_or_else(|| format!("localhost:{}", cfg.bind_addr.port()))
            ),
            cfg.host.is_none(),
        )
    } else {
        match (&cfg.scheme, &cfg.host) {
            (&Some(ref scheme), &Some(ref host)) => (format!("{scheme}://{host}"), false),
            _ => panic!("Both the scheme & host have to be specified in release builds."),
        }
    };
    log::debug!(target: "jedi", "Origin is {origin}");

    let ctx: &'static _ = Box::leak(Box::new(AppContext {
        cfg,
        hbs,
        uris,
        db,
        email,
        origin,
        auto_filled_origin,
    }));

    tokio::spawn(daily_updates(ctx));
    log::debug!(target: "jedi", "Daily updates initialised");

    let app = any_route_of![
        router! {
            in ctx;
            recover handle_endpoint_rejections_human;

            let data_protection = / "data-protection" {
                #get .and_then(|| auto_reject(async {
                    ctx.html_template("data_protection.html", &EmptyStruct { })
                }));
            };

            let index = / index {
                #get .and_then(|| auto_reject(async {
                    ctx.html_template("index.html", &EmptyStruct { })
                }));
            };
        },
        warp::path!("api" / "v1" / ..).and(api::api(ctx)),
        manage::app(ctx),
        email_user::app(ctx),
        warp::path("static").and(warp::fs::dir(ctx.cfg.static_dir.clone())),
    ];

    #[cfg(any(debug_assertions, feature = "debug"))]
    let app = app
        .or(email_debug::return_log())
        .unify()
        .or(warp::path!("log").and(body::bytes()).map(|body: Bytes| {
            log::warn!(target: "jedi::remotelog", "{}", String::from_utf8_lossy(&body));
            StatusCode::OK.into_response()
        }))
        .unify()
        .or(warp::path!("time-tick").and_then(move || auto_reject(time_tick(ctx))))
        .unify();

    let app = force_same_origin_unless_get_ish(ctx)
        .and(app)
        .map(|(), response| response)
        .recover(move |rejection| handle_final_rejections(ctx, rejection))
        .unify()
        .and(warp::method())
        .map(|mut response: Response, method: Method| {
            let headers = response.headers_mut();
            headers.insert(REFERRER_POLICY, HeaderValue::from_static("same-origin"));
            headers.insert(X_CONTENT_TYPE_OPTIONS, HeaderValue::from_static("nosniff"));
            headers.insert(X_FRAME_OPTIONS, HeaderValue::from_static("DENY"));
            headers.insert(
                CONTENT_SECURITY_POLICY,
                HeaderValue::from_static(
                    "default-src 'none'; \
                     connect-src 'self'; \
                     font-src 'self'; \
                     img-src 'self' https: data:; \
                     script-src 'self' 'unsafe-inline' 'unsafe-eval'; \
                     style-src 'self' 'unsafe-inline'; \
                     form-action 'self'; \
                     base-uri 'none';",
                ),
            );
            headers.insert(
                ACCESS_CONTROL_ALLOW_ORIGIN,
                HeaderValue::from_str(&ctx.origin).unwrap(),
            );

            if method == Method::HEAD {
                *response.body_mut() = Body::empty();
            }

            response
        })
        .with(warp::log("jedi"));

    #[cfg(any(debug_assertions, feature = "debug"))]
    let app = app.with(warp::log::custom(|info| {
        let mut cookies = HashMap::new();
        for i in &info.request_headers().get_all("Cookie") {
            let i = match i.to_str() {
                Ok(i) => i,
                Err(err) => {
                    log::info!(
                        target: "jedi::cookie",
                        "Invalid cookie header (couldn't get str) {i:?}: {err}",
                    );
                    continue;
                },
            };
            match Cookie::parse_encoded(i) {
                Ok(cookie) => cookies
                    .entry(cookie.name().to_owned())
                    .or_insert_with(Vec::new)
                    .push(cookie.value().to_owned()),
                Err(err) => log::info!(
                    target: "jedi::cookie",
                    "Invalid cookie header (couldn't parse) {i:?}: {err}",
                ),
            }
        }

        if cookies.is_empty() {
            log::info!(target: "jedi::cookies", "No cookies sent");
        } else {
            log::info!(target: "jedi::cookies", "Cookies: {cookies:?}");
        }
    }));

    log::debug!(target: "jedi", "App built, serving...");

    warp::serve(app).run(ctx.cfg.bind_addr).await;
}
