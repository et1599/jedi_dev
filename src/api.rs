use std::collections::{hash_map::Entry, HashMap, HashSet};

use macrow::Macrow;
use serde::{Deserialize, Serialize};
use serde_json::Value as JsonValue;
use ts_rs::TS;
use warp::{hyper::StatusCode, reply::json, Filter, Reply};

use crate::{
    email_debug,
    event::{
        add_event_sections_without_updating_delegate_count, event_state_to_error,
        form::{FilledForm, Form, FormItem},
        update_delegate_count, BeforeArchival, CreateEvent, DbBasicEventOverview, Event, EventDiff,
        EventOverview, EventState, FullEventState,
    },
    event_section::{
        event_section_uri, DbRequest, EventExportRow, EventSection, EventSectionDiff,
        EventSectionInfo, EventSectionInfoParams, EventSectionInner, MoveRequestTarget,
    },
    helpers::{Email, NonEmptyString, Password},
    manage::auth::{AuthorizationError, Basic, CheckPermissions, LoginInfo},
    management_user::{
        invite, reset_password, split_users, BaseUserOverview, BasicUserInfo, CreateUser, DbUser,
        PartialUserOverview, ResetPasswordReason::RemovedBySuperUser, SignupEmailContext,
        SignupParams, SignupReason, User, UserDiff, UserEventSectionOverview, UserIdOrSelf,
        UserKind, UserOverview,
    },
    prelude::*,
    rejections::{auto_reject, Error, UserError},
    section::{
        ExtendedSectionOverview, Section, SectionDiff, SectionHeadsRow, SectionOverview,
        SectionPut, SectionStats,
    },
    warp_ext::csv_response,
};

#[allow(clippy::struct_field_names)]
#[derive(Serialize, TS)]
#[ts(export)]
struct Config {
    signin_length: f64,
    archive_length: f64,
    passive_update_length: f64,
    active_update_length: f64,
}

async fn get_config(mut login: LoginInfo) -> EndpointResult {
    let config = sqlx::query_as!(
        Config,
        r#"
            SELECT
                CAST(EXTRACT(EPOCH FROM signin_length) AS REAL) AS "signin_length!",
                CAST(EXTRACT(EPOCH FROM archive_length) AS REAL) AS "archive_length!",
                CAST(EXTRACT(EPOCH FROM passive_update_length) AS REAL) AS "passive_update_length!",
                CAST(EXTRACT(EPOCH FROM active_update_length) AS REAL) AS "active_update_length!"
            FROM meta
        "#,
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&config).into_response())
}

diff! {
    #[allow(clippy::struct_field_names)]
    struct ConfigDiff {
        signin_length: Option<f64>,
        archive_length: Option<f64>,
        passive_update_length: Option<f64>,
        active_update_length: Option<f64>,
    }
}

async fn patch_config(body: ConfigDiff, mut login: LoginInfo) -> EndpointResult {
    sqlx::query!(
        r#"
            UPDATE meta
            SET signin_length = COALESCE(MAKE_INTERVAL(secs => $1), signin_length),
                archive_length = COALESCE(MAKE_INTERVAL(secs => $2), archive_length),
                passive_update_length = COALESCE(MAKE_INTERVAL(secs => $3), passive_update_length),
                active_update_length = COALESCE(MAKE_INTERVAL(secs => $4), active_update_length)
        "#,
        body.signin_length,
        body.archive_length,
        body.passive_update_length,
        body.active_update_length,
    )
    .execute(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

#[derive(Deserialize, TS)]
#[ts(export)]
struct PushView {
    uri: Option<NonEmptyString>,
}

async fn post_push_view(body: PushView, mut login: LoginInfo) -> EndpointResult {
    sqlx::query!(
        r#"
            UPDATE sessions
            SET active_view_uri = CAST($2 AS TEXT),
                active_view_until = CASE
                    WHEN $2 IS NULL THEN NULL
                    ELSE NOW() + (SELECT passive_update_length * 2 FROM meta)
                END
            WHERE id = $1
        "#,
        login.session as SessionId,
        body.uri.as_deref(),
    )
    .execute(&mut *login.transaction)
    .await?;

    let out = &sqlx::query_scalar!(
        r#"
            SELECT u.display_name AS "display_name: NonEmptyString"
            FROM
                sessions AS s1 JOIN sessions AS s2 ON s1.active_view_uri = s2.active_view_uri
                JOIN management_users AS u ON s2.user_id = u.id
            WHERE s1.id = $1 AND s2.id <> s1.id AND NOW() < s2.active_view_until
        "#,
        login.session as SessionId,
    )
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&out).into_response())
}

async fn get_all_events(mut login: LoginInfo) -> EndpointResult {
    let events = sqlx::query!(
        r#"
            SELECT
                id AS "id: EventId",
                name AS "name: NonEmptyString", description,
                state AS "state: EventState", signup_until, decisions_until, archived_until
            FROM events
        "#
    )
    .try_map(|x| EventOverview!(x))
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&events).into_response())
}

async fn post_new_event(body: CreateEvent, mut login: LoginInfo) -> EndpointResult {
    body.state.assert_correct(&mut login.transaction).await?;

    let (state, signup_until, decisions_until, archived_until) = match body.state {
        FullEventState::Setup => (EventState::Setup, None, None, None),
        FullEventState::Active {
            signup_until,
            decisions_until,
        } => (
            EventState::Active,
            Some(signup_until),
            Some(decisions_until),
            None,
        ),
        FullEventState::Done { archived_until } => {
            (EventState::Done, None, None, Some(archived_until))
        },
    };

    let form = body
        .form
        .0
        .into_iter()
        .map(serde_json::to_value)
        .collect::<Result<Vec<_>, _>>()
        .map_err(UserError::Json)?;

    let min_max_delegates = sqlx::query_scalar!(
        r#"
            SELECT CAST(COUNT(*) AS INTEGER) AS "x!" FROM sections WHERE id = ANY($1) AND NOT guest
        "#,
        &body.sections as &[SectionId],
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    if body.max_total_delegates < min_max_delegates {
        return Err(UserError::EventMaxDelegatesTooFew {
            value: body.max_total_delegates,
            min: min_max_delegates,
        }
        .into());
    }

    let id: EventId = sqlx::query_scalar!(
        r#"
            INSERT INTO events(
                name, description, state, signup_until,
                decisions_until, archived_until, form, can_users_leave,
                can_users_edit_name, max_total_delegates
            )
            VALUES (
                $1, $2, $3, $4,
                $5, $6, $7, $8,
                $9, $10
            )
            RETURNING id AS "id: EventId";
        "#,
        &body.name,
        body.description.source,
        state as EventState,
        signup_until,
        decisions_until,
        archived_until,
        &form,
        body.can_users_leave,
        body.can_users_edit_name,
        body.max_total_delegates,
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    sqlx::query!(
        r#"INSERT INTO event_organizers(event, organizer) VALUES ($1, $2)"#,
        id as EventId,
        login.user_id as UserId,
    )
    .execute(&mut *login.transaction)
    .await
    .unwrap();

    add_event_sections_without_updating_delegate_count(&mut login.transaction, id, &body.sections)
        .await?;
    update_delegate_count(&mut login.transaction, id).await?;

    login.transaction.commit().await?;

    Ok(json(&id).into_response())
}

async fn get_event(event: EventId, mut login: LoginInfo) -> EndpointResult {
    let data = sqlx::query!(
        r#"
            SELECT
                id AS "overview_id!: EventId",
                name AS "overview_name!: NonEmptyString", description AS "overview_description!",
                state AS "overview_state!: EventState",
                signup_until AS overview_signup_until, decisions_until AS overview_decisions_until,
                archived_until AS overview_archived_until,
                organizers AS "organizers!: Vec<BasicUserInfo>",
                form AS "form!",
                can_users_leave AS "can_users_leave!",
                can_users_edit_name AS "can_users_edit_name!",
                sections AS "sections!: Vec<SectionOverview>",
                max_total_delegates AS "max_total_delegates!"
            FROM events_with_misc
            WHERE id = $1
        "#,
        event as EventId,
    )
    .try_map(|x| Event!(x; user_kind = login.user_kind))
    .fetch_optional(&mut *login.transaction)
    .await?
    .ok_or_else(|| UserError::DoesntExist)?;

    login.transaction.commit().await?;

    Ok(json(&data).into_response())
}

async fn patch_event(
    event: EventId,
    body: EventDiff,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    event.authorized(&mut login).await?;

    let old_state = sqlx::query_scalar!(
        r#"SELECT state AS "state!: EventState" FROM events WHERE id = $1"#,
        event as EventId,
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    let new_simple_state = body.state.map(EventState::from);
    if old_state == EventState::Done || new_simple_state.map_or(false, |x| x < old_state) {
        return Err(match old_state {
            EventState::Setup => unreachable!(),
            EventState::Active => UserError::EventAlreadyActive.into(),
            EventState::Done => UserError::EventAlreadyDone.into(),
        });
    }

    if old_state != EventState::Setup && body.form.is_some() {
        return Err(UserError::EventAlreadyActive.into());
    }

    macro_rules! any_some {
        ($field:ident $(,)?) => { body.$field.is_some() };
        ($field:ident , $($rest:tt)+) => { any_some!($field) || any_some!($($rest)+) };
    }

    let has_to_update_counts =
        any_some!(sections, max_total_delegates) || body.update_section_stats;

    // main update
    if any_some!(
        name,
        description,
        state,
        form,
        can_users_leave,
        can_users_edit_name,
        max_total_delegates
    ) {
        if let Some(ref state) = body.state {
            state.assert_correct(&mut login.transaction).await?;
        }

        let form = body
            .form
            .map(|x| {
                x.0.into_iter()
                    .map(serde_json::to_value)
                    .collect::<Result<Vec<_>, _>>()
            })
            .transpose()
            .map_err(UserError::Json)?;

        sqlx::query!(
            r#"
                UPDATE events
                SET name = COALESCE($1, name), description = COALESCE($2, description),
                    state = COALESCE($3, state),
                    signup_until = CASE WHEN $3 IS NULL THEN signup_until ELSE $4 END,
                    decisions_until = CASE WHEN $3 IS NULL THEN decisions_until ELSE $5 END,
                    archived_until = CASE WHEN $3 IS NULL THEN archived_until ELSE $6 END,
                    form = COALESCE(CAST($7 AS JSON[]), form),
                    can_users_leave = COALESCE($8, can_users_leave),
                    can_users_edit_name = COALESCE($9, can_users_edit_name),
                    max_total_delegates = COALESCE($10, max_total_delegates)
                WHERE id = $11
            "#,
            body.name.as_deref(),
            body.description.map(|markdown| markdown.source),
            match body.state {
                Some(FullEventState::Setup) => Some(EventState::Setup),
                Some(FullEventState::Active { .. }) => Some(EventState::Active),
                Some(FullEventState::Done { .. }) => Some(EventState::Done),
                None => None,
            } as Option<EventState>,
            match body.state {
                Some(FullEventState::Active { signup_until, .. }) => Some(signup_until),
                _ => None,
            },
            match body.state {
                Some(FullEventState::Active {
                    decisions_until, ..
                }) => Some(decisions_until),
                _ => None,
            },
            match body.state {
                Some(FullEventState::Done { archived_until }) => Some(archived_until),
                _ => None,
            },
            form.as_deref(),
            body.can_users_leave,
            body.can_users_edit_name,
            body.max_total_delegates
                .map(|x| i32::try_from(x))
                .transpose()
                .map_err(UserError::IntConv)?,
            event as EventId,
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    // section update
    if let Some(ref sections) = body.sections {
        // TODO: Maybe don't send the list twice, but it should be short so it's
        // *probably* okay ¯\_(ツ)_/¯

        // Remove
        sqlx::query!(
            r#"DELETE FROM event_sections WHERE event = $1 AND section <> ALL($2)"#,
            event as EventId,
            sections as &[SectionId],
        )
        .execute(&mut *login.transaction)
        .await?;

        add_event_sections_without_updating_delegate_count(&mut login.transaction, event, sections)
            .await?;
    }

    // section stat update
    if body.update_section_stats {
        sqlx::query!(
            r#"
                UPDATE event_sections
                SET last_section_stats = stats
                FROM sections
                WHERE event_sections.section = sections.id AND event = $1
            "#,
            event as EventId,
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    let missing_added = if !body.add_organizers.is_empty() {
        sqlx::query_scalar!(
            r#"
                SELECT user_id AS "user_id!: UserId"
                FROM UNNEST(CAST($1 AS UUID[])) AS input(user_id)
                WHERE user_id NOT IN (SELECT id FROM management_users)
            "#,
            &body.add_organizers as &[UserId],
        )
        .fetch_all(&mut *login.transaction)
        .await?
    } else {
        Vec::new()
    };

    let tried_to_remove_self = body.remove_organizers.contains(&login.user_id);
    if !missing_added.is_empty() || tried_to_remove_self {
        return Err(UserError::EventInvalidOrganizerUpdates {
            doesnt_exist: missing_added,
            tried_to_remove_self,
        }
        .into());
    }

    // organizers
    if !body.remove_organizers.is_empty() {
        sqlx::query!(
            r#"DELETE FROM event_organizers WHERE event = $1 AND organizer = ANY($2)"#,
            event as EventId,
            &body.remove_organizers as &[UserId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !body.add_organizers.is_empty() {
        // Make people organizers if they aren't already
        sqlx::query_scalar!(
            r#"
                UPDATE management_users
                SET kind = 'organizer'
                FROM UNNEST(CAST($1 AS UUID[])) AS input(user_id)
                WHERE input.user_id = management_users.id AND kind = 'normal'
            "#,
            &body.add_organizers as &[UserId],
        )
        .execute(&mut *login.transaction)
        .await?;

        sqlx::query!(
            r#"
                INSERT INTO event_organizers(event, organizer)
                SELECT $1 AS event, organizer
                FROM UNNEST(CAST($2 AS UUID[])) AS input(organizer)
                ON CONFLICT (event, organizer) DO NOTHING
            "#,
            event as EventId,
            &body.add_organizers as &[UserId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !body.invite_organizers.is_empty() {
        let ids = invite(
            &mut login.transaction,
            body.invite_organizers,
            UserKind::Organizer,
        )
        .await?;

        let added = sqlx::query!(
            r#"
                -- We're getting lucky here again, as we want the emails of those we added
                -- only anyway. Note that this does include all people that would require a
                -- new signup email, too.
                INSERT INTO event_organizers(event, organizer)
                SELECT $1 AS event, organizer
                FROM UNNEST(CAST($2 AS UUID[])) AS input(organizer)
                ON CONFLICT (event, organizer) DO NOTHING
                RETURNING
                    organizer AS "id: UserId",
                    -- Less lucky here because we can't access other rows/tables from the
                    -- `FROM` clause
                    (SELECT email FROM management_users WHERE id = organizer) AS "email!: Email",
                    -- `IS NULL` implies `complete`, `IS NOT NULL` implies `NOT complete`
                    (SELECT partial_secret FROM management_users WHERE id = organizer)
                        AS "partial_secret: NonEmptyString",
                    (SELECT display_name FROM management_users WHERE id = organizer)
                        AS "display_name: NonEmptyString"
            "#,
            event as EventId,
            &ids as &[UserId],
        )
        .fetch_all(&mut *login.transaction)
        .await?;

        let event_info = sqlx::query_as!(
            DbBasicEventOverview,
            r#"
                SELECT
                    id AS "id: EventId",
                    name AS "name: NonEmptyString", description,
                    state AS "state: EventState", signup_until, decisions_until, archived_until
                FROM events
                WHERE id = $1
            "#,
            event as EventId,
        )
        .try_map(EventOverview::try_from)
        .fetch_one(&mut *login.transaction)
        .await?;

        for row in added {
            email_debug::log(&row.email, "new-organizer", &event.to_string());

            if let Some(secret) = row.partial_secret {
                email_debug::log(&row.email, "signup", &secret);

                ctx.spawn_and_send_email(
                    "email/manage/signup.html",
                    row.email.parse()?,
                    SignupEmailContext {
                        signup_params: SignupParams {
                            email: row.email,
                            secret,
                        },
                        display_name: row.display_name,
                        reason: SignupReason::NewOrganizer {
                            event: event_info.clone(),
                        },
                    },
                    None,
                );
            } else {
                #[derive(Serialize)]
                struct TemplateContext {
                    display_name: Option<NonEmptyString>,
                    event: EventOverview,
                }

                ctx.spawn_and_send_email(
                    "email/manage/new-organizer.html",
                    row.email.parse()?,
                    TemplateContext {
                        display_name: row.display_name,
                        event: event_info.clone(),
                    },
                    None,
                );
            }
        }
    }

    if has_to_update_counts {
        update_delegate_count(&mut login.transaction, event).await?;
    }

    login.transaction.commit().await?;
    Ok(StatusCode::OK.into_response())
}

async fn delete_event(event: EventId, mut login: LoginInfo) -> EndpointResult {
    event.authorized(&mut login).await?;
    sqlx::query!(r#"DELETE FROM events WHERE id = $1"#, event as EventId)
        .execute(&mut *login.transaction)
        .await?;
    login.transaction.commit().await?;
    Ok(StatusCode::OK.into_response())
}

async fn get_event_all_info(
    event: EventId,
    params: EventSectionInfoParams,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    event.authorized(&mut login).await?;

    let infos = sqlx::query!(
        r#"
            SELECT
                s.id AS "section_id: SectionId", s.name AS "section_name: NonEmptyString",
                es.secret AS "secret: NonEmptyString",
                es.max_delegates AS "max_delegates",
                (
                    SELECT CAST(COUNT(*) AS INTEGER)
                    FROM requests AS r
                    WHERE r.event = es.event AND r.section = es.section AND filled
                ) AS "requests!",
                (
                    SELECT CAST(COUNT(*) AS INTEGER)
                    FROM requests AS r
                    WHERE r.event = es.event AND r.section = es.section AND filled
                        AND position IS NOT NULL
                ) AS "positioned_requests!",
                (
                    SELECT CAST(COUNT(*) AS INTEGER)
                    FROM requests AS r
                    WHERE r.event = es.event AND r.section = es.section AND filled
                        AND accepted_as_guest
                ) AS "guest_requests!"
            FROM sections AS s JOIN event_sections AS es ON s.id = es.section
            WHERE es.event = $1
        "#,
        event as EventId,
    )
    .try_map(|row| EventSectionInfo!(row; link = event_section_uri(ctx, event, &row.section_id, &row.secret)))
    .fetch_all(&mut *login.transaction)
    .await?;

    Ok(if params.as_csv {
        let event_name = sqlx::query_scalar!(
            r#"SELECT name AS "x: NonEmptyString" FROM events WHERE id = $1"#,
            event as EventId,
        )
        .fetch_one(&mut *login.transaction)
        .await?;

        login.transaction.commit().await?;

        let mut out = Vec::new();

        {
            let mut writer = csv::WriterBuilder::new()
                .has_headers(false)
                .from_writer(&mut out);

            // Manually write the header because we want different names than in
            // the JSON case:
            writer
                .write_record(EventSectionInfo::HEADER)
                .map_err(Error::GeneratedCsv)?;

            for i in infos {
                writer.serialize(i).map_err(Error::GeneratedCsv)?;
            }
        }

        csv_response(&format!("{event_name}-infos.csv"), &out)?.into_response()
    } else {
        login.transaction.commit().await?;

        json(&infos).into_response()
    })
}

async fn get_event_all_export(event: EventId, mut login: LoginInfo) -> EndpointResult {
    event.authorized(&mut login).await?;

    let event_info = sqlx::query!(
        r#"SELECT form, name AS "name: NonEmptyString" FROM events WHERE id = $1"#,
        event as EventId,
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    let form = Form::try_from(event_info.form)?;

    let stat_rows = sqlx::query!(
        r#"
            SELECT
                event_sections.section AS "section_id: SectionId",
                sections.name AS "section_name: NonEmptyString",
                event_sections.max_delegates AS allowed_count,
                (
                    SELECT COUNT(*)
                    FROM requests AS r
                    WHERE r.event = event_sections.event AND r.section = event_sections.section
                        AND filled AND r.position IS NOT NULL
                ) AS "ordered_count!",
                (
                    SELECT COUNT(*)
                    FROM requests AS r
                    WHERE r.event = event_sections.event AND r.section = event_sections.section
                        AND filled AND r.accepted_as_guest
                ) AS "guest_count!",
                requests.first_name AS "first_name?: NonEmptyString",
                requests.last_name AS "last_name?",
                requests.email AS "email?: Email",
                requests.position AS "uncompressed_position?",
                CASE
                    WHEN requests.position IS NULL THEN NULL
                    ELSE (
                        SELECT CAST(COUNT(r.position) + 1 AS INTEGER)
                        FROM requests AS r
                        WHERE r.event = requests.event AND r.section = requests.section
                            AND r.position < requests.position
                    )
                END AS "position?",
                -- `included_as_delegate` is computed on the Rust side
                FALSE AS "included_as_delegate?",
                accepted_as_guest AS "included_as_guest?",
                CASE
                    WHEN requests.first_name IS NULL THEN ARRAY[]::JSON[]
                    ELSE requests.filled_form
                END AS "form!: Vec<JsonValue>"
            FROM event_sections
                JOIN sections ON event_sections.section = sections.id
                LEFT OUTER JOIN (SELECT * FROM requests WHERE filled) AS requests
                    ON event_sections.event = requests.event
                        AND event_sections.section = requests.section
            WHERE event_sections.event = $1
            ORDER BY event_sections.section, COALESCE(requests.position, 0)
        "#,
        event as EventId,
    )
    .try_map(|x| EventExportRow!(x))
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    let mut out = Vec::new();

    {
        let mut writer = csv::Writer::from_writer(&mut out);

        let form_fields: Vec<_> = form
            .0
            .iter()
            .filter_map(|i| match *i {
                FormItem::Output(_) => None,
                FormItem::Input(ref inp) => Some(&*inp.label),
            })
            .collect();

        writer
            .write_record(EventExportRow::header(form_fields.iter().copied()))
            .map_err(Error::GeneratedCsv)?;

        for mut i in stat_rows {
            i.included_as_delegate = i
                .first_name
                .is_some()
                .then_some(i.position)
                .map(|p| p.map_or(false, |p| p < i.allowed_count));
            if i.first_name.is_none() {
                // TODO: Change macrow so that `Option`s work in conversions like these or do
                // something else to make this not allocate the same thing again and again for
                // empty sections. Not a high priority though as this code path only rarely occurs
                // anyway.
                i.form = FilledForm(form_fields.iter().map(|_| None).collect());
            }
            writer
                .write_record(i.into_record())
                .map_err(Error::GeneratedCsv)?;
        }
    }

    csv_response(&format!("{}.csv", &event_info.name), &out)
}

async fn post_event_all_notify_heads(
    event: EventId,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    event.authorized(&mut login).await?;

    let event_data = sqlx::query!(
        r#"
            SELECT
                id AS "overview_id!: EventId",
                name AS "overview_name!: NonEmptyString", description AS "overview_description!",
                state AS "overview_state!: EventState",
                signup_until AS overview_signup_until, decisions_until AS overview_decisions_until,
                archived_until AS overview_archived_until,
                organizers AS "organizers!: Vec<BasicUserInfo>",
                form AS "form!",
                can_users_leave AS "can_users_leave!",
                can_users_edit_name AS "can_users_edit_name!",
                sections AS "sections!: Vec<SectionOverview>",
                max_total_delegates AS "max_total_delegates!"
            FROM events_with_misc
            WHERE id = $1
        "#,
        event as EventId,
    )
    .try_map(|x| Event!(x; user_kind = login.user_kind))
    .fetch_one(&mut *login.transaction)
    .await?;

    event_state_to_error(event_data.overview.state, BeforeArchival)?;

    let heads = sqlx::query!(
        r#"
            SELECT
                management_users.email AS "email: Email",
                management_users.display_name AS "display_name!: NonEmptyString",
                sections.id AS "section_id: SectionId",
                sections.name AS "section_name: NonEmptyString",
                event_sections.secret AS "event_section_secret: NonEmptyString"
            FROM event_sections JOIN sections ON event_sections.section = sections.id
                JOIN section_heads ON sections.id = section_heads.section
                JOIN management_users ON section_heads.head = management_users.id
            WHERE event_sections.event = $1
        "#,
        event as EventId,
    )
    .fetch_all(&mut *login.transaction)
    .await?;

    ctx.spawn_and_send_emails(
        "email/manage/new-relevant-event.html",
        heads.into_iter().map(move |row| {
            email_debug::log(
                &row.email,
                "new-relevant-event",
                &event_data.overview.id.to_string(),
            );

            let uri = event_section_uri(ctx, event, &row.section_id, &row.event_section_secret);
            Ok((
                row.email.parse()?,
                json!({
                    "event": &event_data,
                    "section": { "id": &row.section_id, "name": &row.section_name },
                    "display_name": row.display_name,
                    "signup_uri": uri,
                }),
                None,
            ))
        }),
    );

    Ok(StatusCode::OK.into_response())
}

async fn get_all_users(mut login: LoginInfo) -> EndpointResult {
    let users = sqlx::query!(
        r#"
            SELECT
                id AS "base_id!: UserId",
                display_name AS "display_name!: NonEmptyString",
                CASE WHEN $1 THEN email ELSE NULL END AS "base_email: Email",
                kind AS "base_kind!: UserKind", admin_for AS "base_admin_for!: Vec<SectionId>"
            FROM management_users_with_admin_for
            WHERE complete
        "#,
        login.user_kind == UserKind::SuperAdmin,
    )
    .try_map(|x| UserOverview!(x))
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&users).into_response())
}

async fn get_all_partial_users(mut login: LoginInfo) -> EndpointResult {
    let partial_users = sqlx::query!(
        r#"
            SELECT
                id AS "base_id!: UserId",
                display_name AS "display_name: NonEmptyString",
                CASE WHEN $1 THEN email ELSE NULL END AS "base_email: Email",
                kind AS "base_kind!: UserKind", admin_for AS "base_admin_for!: Vec<SectionId>"
            FROM management_users_with_admin_for
            WHERE NOT complete
        "#,
        login.user_kind == UserKind::SuperAdmin,
    )
    .try_map(|x| PartialUserOverview!(x))
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&partial_users).into_response())
}

async fn post_user_reset_password(
    user: UserId,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    if reset_password(
        ctx,
        &mut login.transaction,
        Some(user),
        None,
        RemovedBySuperUser,
    )
    .await?
        == 1
    {
        Ok(StatusCode::OK.into_response())
    } else {
        Err(UserError::DoesntExist.into())
    }
}

async fn post_user_all_mass_reset_password(
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    reset_password(ctx, &mut login.transaction, None, None, RemovedBySuperUser).await?;
    Ok(StatusCode::OK.into_response())
}

async fn post_new_user(
    user: CreateUser,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    let created = sqlx::query!(
        r#"
            INSERT INTO management_users(email, kind, partial_secret, display_name)
            VALUES ($1, $2, encode(gen_random_bytes(32), 'hex'), $3)
            RETURNING id AS "id: UserId", partial_secret AS "partial_secret!: NonEmptyString"
        "#,
        &*user.email,
        user.kind as UserKind,
        user.display_name.as_deref(),
    )
    .fetch_one(&mut *login.transaction)
    .await;

    if let Err(sqlx::Error::Database(ref err)) = created {
        if err.constraint() == Some("management_users_email_key") {
            return Err(UserError::ManageUserEmailExistsAlready.into());
        }
    }

    let created = created?;

    if user.kind == UserKind::Admin && !user.admin_for.is_empty() {
        sqlx::query!(
            r#"
                INSERT INTO management_user_admin_for(user_id, section)
                SELECT $1 AS user_id, section
                FROM UNNEST(CAST($2 AS TEXT[])) AS input(section)
            "#,
            created.id as UserId,
            &user.admin_for as &[SectionId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    login.transaction.commit().await?;

    email_debug::log(&user.email, "signup", &created.partial_secret);

    ctx.spawn_and_send_email(
        "email/manage/signup.html",
        user.email.parse()?,
        SignupEmailContext {
            signup_params: SignupParams {
                email: user.email,
                secret: created.partial_secret,
            },
            display_name: user.display_name,
            reason: SignupReason::AddedBySuperAdmin,
        },
        None,
    );

    Ok(json(&created.id).into_response())
}

async fn get_user(user: UserIdOrSelf, mut login: LoginInfo) -> EndpointResult {
    let user = user.id_in(&login);

    // Funky (but unimportant) edge case: 404 on /user/self when the user was
    // removed in between the login check of the endpoint and this.
    let user_data = sqlx::query!(
        r#"
            SELECT
                id AS "id!: UserId",
                email AS "email!: Email",
                display_name AS "display_name: NonEmptyString",
                kind AS "kind!: UserKind", admin_for AS "admin_for!: Vec<SectionId>"
            FROM management_users_with_admin_for
            WHERE id = $1
        "#,
        user as UserId,
    )
    .fetch_optional(&mut *login.transaction)
    .await?
    .ok_or_else(|| UserError::DoesntExist)?;

    if user == login.user_id || login.user_kind == UserKind::SuperAdmin {
        let relevant_events = sqlx::query!(
            r#"
                SELECT
                    id AS "id: EventId",
                    name AS "name: NonEmptyString", description,
                    state AS "state: EventState", signup_until, decisions_until, archived_until
                FROM events JOIN event_organizers ON id = event
                WHERE organizer = $1
            "#,
            user as UserId,
        )
        .try_map(|x| EventOverview!(x))
        .fetch_all(&mut *login.transaction)
        .await?;

        let relevant_event_sections = sqlx::query!(
            r#"
                SELECT
                    events.id AS "event_id: EventId",
                    events.name AS "event_name: NonEmptyString",
                    events.description AS event_description,
                    events.state AS "event_state: EventState",
                    events.signup_until AS event_signup_until,
                    events.decisions_until AS event_decisions_until,
                    events.archived_until AS event_archived_until,
                    sections.id AS "section_id: SectionId",
                    sections.name AS "section_name: NonEmptyString",
                    sections.guest AS section_guest,
                    relevant_event_sections.reason AS "reason!"
                FROM events JOIN relevant_event_sections ON events.id = event
                    JOIN sections ON sections.id = section
                WHERE relevant_event_sections.user = $1 AND events.state <> 'setup'
            "#,
            user as UserId,
        )
        .try_map(|x| UserEventSectionOverview!(x))
        .fetch_all(&mut *login.transaction)
        .await?;

        login.transaction.commit().await?;

        Ok(json(&User {
            db_user: DbUser!(user_data)?,
            relevant_events,
            relevant_event_sections,
        })
        .into_response())
    } else {
        login.transaction.commit().await?;

        Ok(json(&PartialUserOverview {
            base: BaseUserOverview {
                id: user_data.id,
                email: None,
                kind: user_data.kind,
                admin_for: user_data.admin_for,
            },
            display_name: user_data.display_name,
        })
        .into_response())
    }
}

async fn patch_user(
    user: UserIdOrSelf,
    mut body: UserDiff,
    mut login: LoginInfo,
) -> EndpointResult {
    let user = user.id_in(&login);

    if user != login.user_id {
        // TODO: Throw an error instead
        body.password = None;
    }

    user.authorized(&mut login).await?;

    let pw_hash = body.password.as_ref().map(Password::hash).transpose()?;
    let kind = sqlx::query_scalar!(
        r#"
            UPDATE management_users
            SET email = COALESCE($1, management_users.email),
                display_name = COALESCE($2, management_users.display_name),
                password_hash = COALESCE($3, management_users.password_hash),
                kind = COALESCE($4, management_users.kind)
            WHERE id = $5
            RETURNING kind AS "kind: UserKind"
        "#,
        body.email.as_deref(),
        body.display_name.as_deref(),
        pw_hash.as_deref(),
        body.kind as Option<UserKind>,
        user as UserId,
    )
    .fetch_optional(&mut *login.transaction)
    .await;

    if let Err(sqlx::Error::Database(ref err)) = kind {
        if err.constraint() == Some("management_users_email_key") {
            // `unwrap` is safe because we can only get a conflict here if we changed the
            // email, which only happens if `body.email` is set
            return Err(UserError::ManageUserEmailExistsAlready.into());
        }
    }

    let kind = kind?.ok_or_else(|| UserError::DoesntExist)?;

    if kind == UserKind::Admin && !body.admin_for.is_empty() {
        // Just deleting all & readding them is fine because there's no other fields,
        // and this is happening in a transaction
        sqlx::query!(
            r#"DELETE FROM management_user_admin_for WHERE user_id = $1"#,
            user as UserId,
        )
        .execute(&mut *login.transaction)
        .await?;

        sqlx::query!(
            r#"
                INSERT INTO management_user_admin_for(user_id, section)
                SELECT $1 AS user_id, section
                FROM UNNEST(CAST($2 AS TEXT[])) AS input(section)
            "#,
            user as UserId,
            &body.admin_for as &[SectionId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

async fn delete_user(user: UserIdOrSelf, mut login: LoginInfo) -> EndpointResult {
    let user = user.id_in(&login);

    user.authorized(&mut login).await?;

    sqlx::query!(
        r#"DELETE FROM management_users WHERE id = $1"#,
        user as UserId,
    )
    .execute(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

async fn get_event_section(
    event: EventId,
    section: SectionId,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    Basic((event, &section)).authorized(&mut login).await?;

    let is_organizer = match event.authorized(&mut login).await {
        Ok(()) => true,
        Err(AuthorizationError::NotAuthorized) => false,
        Err(err) => return Err(err.into()),
    };

    let inner = sqlx::query!(
        r#"
            SELECT
                ARRAY(
                    SELECT CAST(
                        (u.id, u.complete, u.email, u.display_name, u.kind, u.admin_for)
                        AS BASIC_USER_INFO
                    )
                    FROM event_section_helpers AS esh JOIN management_users_with_admin_for AS u
                        ON esh.helper = u.id
                    WHERE esh.event = es.event AND esh.section = es.section
                ) AS "helpers!: Vec<BasicUserInfo>",
                ARRAY(
                    SELECT CAST(
                        (u.id, u.complete, u.email, u.display_name, u.kind, u.admin_for)
                        AS BASIC_USER_INFO
                    )
                    FROM section_heads AS sh JOIN management_users_with_admin_for AS u
                        ON sh.head = u.id
                    WHERE sh.section = es.section
                ) AS "heads!: Vec<BasicUserInfo>",
                es.max_delegates,
                es.last_section_stats AS "last_section_stats: SectionStats",
                sections.guest AS is_guest_section,
                es.secret AS "secret: NonEmptyString"
            FROM event_sections AS es JOIN sections ON es.section = sections.id
            WHERE es.event = $1 AND es.section = $2
        "#,
        event as EventId,
        &section as &SectionId,
    )
    .try_map(|x| EventSectionInner!(x; user_kind = login.user_kind))
    .fetch_optional(&mut *login.transaction)
    .await?
    .ok_or_else(|| UserError::DoesntExist)?;

    let requests = sqlx::query!(
        r#"
            SELECT
                id AS "request_id: RequestId",
                first_name AS "request_first_name!: NonEmptyString",
                last_name AS "request_last_name!",
                CASE WHEN $3 THEN email ELSE NULL END AS "request_email: Email",
                filled_form AS "request_form!",
                position, accepted_as_guest
            FROM requests
            WHERE event = $1 AND section = $2 AND filled
        "#,
        event as EventId,
        &section as &SectionId,
        is_organizer,
    )
    .try_map(|x| DbRequest!(x))
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    let mut delegates = Vec::new();
    for i in &requests {
        if let Some(idx) = i.position {
            let idx = usize::try_from(idx).map_err(Error::GeneratedIntConv)?;
            delegates.resize(delegates.len().max(idx + 1), None);
            delegates[idx] = Some(i.request.id);
        }
    }

    Ok(json(&EventSection {
        uri: event_section_uri(ctx, event, &section, &inner.secret),
        inner,
        delegates,
        guests: requests
            .iter()
            .filter(|i| i.accepted_as_guest)
            .map(|i| i.request.id)
            .collect(),
        requests: requests
            .into_iter()
            .map(|i| Ok((i.request.id, i.request)))
            .collect::<Result<_, Error>>()?,
    })
    .into_response())
}

async fn patch_event_section(
    event: EventId,
    section: SectionId,
    body: EventSectionDiff,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    {
        // Check for overlap between "request actions"
        let mut overlap = Vec::new();
        let mut implicated = HashSet::new();
        let mut positions = HashMap::new();
        let mut target_sections = HashSet::new();

        let mut implicate = |overlap: &mut Vec<_>, request| {
            if implicated.contains(&request) {
                overlap.push(request);
            } else {
                implicated.insert(request);
            }
        };

        for i in &body.move_requests {
            implicate(&mut overlap, i.request);

            match i.target {
                MoveRequestTarget::Position(p) => match positions.entry(p) {
                    Entry::Vacant(vacant) => {
                        vacant.insert(Some(i.request));
                    },
                    Entry::Occupied(mut inner) => {
                        if let Some(other) = inner.get_mut().take() {
                            overlap.push(other);
                        }

                        overlap.push(i.request);
                    },
                },
                MoveRequestTarget::Section { ref section } => {
                    target_sections.insert(section.clone());
                },
                MoveRequestTarget::NotAccepted | MoveRequestTarget::Guest => (),
            }
        }

        for i in &body.remove_requests {
            implicate(&mut overlap, *i);
        }

        if !overlap.is_empty() {
            return Err(UserError::EventSectionRequestActionOverlap { overlap }.into());
        }

        if !implicated.is_empty() {
            let missing_requests = sqlx::query_scalar!(
                r#"
                    SELECT target_request AS "target_request!: RequestId"
                    FROM UNNEST(CAST($1 AS UUID[])) AS target_requests(target_request)
                    WHERE target_request NOT IN (
                        SELECT id FROM requests WHERE event = $2 AND section = $3 AND filled
                    )
                "#,
                &implicated.into_iter().collect::<Vec<_>>() as &[RequestId],
                event as EventId,
                &section as &SectionId,
            )
            .fetch_all(&mut *login.transaction)
            .await?;

            if !missing_requests.is_empty() {
                return Err(UserError::EventSectionRequestDoesntExist {
                    ids: missing_requests,
                }
                .into());
            }
        }

        if !target_sections.is_empty() {
            let missing_sections = sqlx::query_scalar!(
                r#"
                    SELECT target_section AS "target_section!: SectionId"
                    FROM UNNEST(CAST($1 AS TEXT[])) AS target_sections(target_section)
                    WHERE target_section NOT IN (
                        SELECT section FROM event_sections WHERE event = $2
                    )
                "#,
                &target_sections.into_iter().collect::<Vec<_>>() as &[SectionId],
                event as EventId,
            )
            .fetch_all(&mut *login.transaction)
            .await?;

            if !missing_sections.is_empty() {
                return Err(UserError::EventSectionTargetDoesntExist {
                    ids: missing_sections,
                }
                .into());
            }
        }
    }

    if !body.remove_helpers.is_empty() || !body.invite_helpers.is_empty() {
        (event, &section).authorized(&mut login).await?;
    } else {
        Basic((event, &section)).authorized(&mut login).await?;
    }

    let archived = sqlx::query_scalar!(
        r#"SELECT state = 'done' AS "x!" FROM events WHERE id = $1"#,
        event as EventId,
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    if archived {
        return Err(UserError::EventAlreadyDone.into());
    }

    if !body.remove_helpers.is_empty() {
        sqlx::query!(
            r#"
                DELETE FROM event_section_helpers
                WHERE event = $1 AND section = $2 AND helper = ANY($3)
            "#,
            event as EventId,
            &section as &SectionId,
            &body.remove_helpers as &[UserId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !body.invite_helpers.is_empty() {
        let ids = invite(
            &mut login.transaction,
            body.invite_helpers,
            UserKind::Normal,
        )
        .await?;

        let added = sqlx::query!(
            r#"
                INSERT INTO event_section_helpers(event, section, helper)
                SELECT $1, $2, helper
                FROM UNNEST(CAST($3 AS UUID[])) AS input(helper)
                ON CONFLICT (event, section, helper) DO NOTHING
                RETURNING
                    helper AS "id: UserId",
                    (SELECT email FROM management_users WHERE id = helper) AS "email!: Email",
                    (SELECT partial_secret FROM management_users WHERE id = helper)
                        AS "partial_secret: NonEmptyString",
                    (SELECT display_name FROM management_users WHERE id = helper)
                        AS "display_name: NonEmptyString"
            "#,
            event as EventId,
            &section as &SectionId,
            &ids as &[UserId],
        )
        .fetch_all(&mut *login.transaction)
        .await?;

        let event_info = sqlx::query_as!(
            DbBasicEventOverview,
            r#"
                SELECT
                    id AS "id: EventId",
                    name AS "name: NonEmptyString", description,
                    state AS "state: EventState", signup_until, decisions_until, archived_until
                FROM events
                WHERE id = $1
            "#,
            event as EventId,
        )
        .try_map(EventOverview::try_from)
        .fetch_one(&mut *login.transaction)
        .await?;

        let section_info = sqlx::query_as!(
            SectionOverview,
            r#"
                SELECT id AS "id: SectionId", name AS "name: NonEmptyString", guest
                FROM sections
                WHERE id = $1
            "#,
            &section as &SectionId,
        )
        .fetch_one(&mut *login.transaction)
        .await?;

        for row in added {
            email_debug::log(&row.email, "new-helper", &event.to_string());

            if let Some(secret) = row.partial_secret {
                email_debug::log(&row.email, "signup", &secret);

                ctx.spawn_and_send_email(
                    "email/manage/signup.html",
                    row.email.parse()?,
                    SignupEmailContext {
                        signup_params: SignupParams {
                            email: row.email,
                            secret,
                        },
                        display_name: row.display_name,
                        reason: SignupReason::NewHelper {
                            event: event_info.clone(),
                            section: section_info.clone(),
                        },
                    },
                    None,
                );
            } else {
                #[derive(Serialize)]
                struct TemplateContext {
                    display_name: Option<NonEmptyString>,
                    event: EventOverview,
                    section: SectionOverview,
                }

                ctx.spawn_and_send_email(
                    "email/manage/new-helper.html",
                    row.email.parse()?,
                    TemplateContext {
                        display_name: row.display_name,
                        event: event_info.clone(),
                        section: section_info.clone(),
                    },
                    None,
                );
            }
        }
    }

    if !body.remove_requests.is_empty() {
        sqlx::query!(
            r#"DELETE FROM requests WHERE id = ANY($1)"#,
            &body.remove_requests as &[RequestId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    let mut not_accepted = Vec::new();
    let mut positioned_ids = Vec::new();
    let mut positioned_positions = Vec::new();
    let mut guests = Vec::new();
    let mut moved_section_ids = Vec::new();
    let mut moved_section_sections = Vec::new();

    for i in body.move_requests {
        let target_vec = match i.target {
            MoveRequestTarget::NotAccepted => &mut not_accepted,
            MoveRequestTarget::Position(pos) => {
                positioned_positions.push(i32::from(pos));
                &mut positioned_ids
            },
            MoveRequestTarget::Guest => &mut guests,
            MoveRequestTarget::Section { section } => {
                moved_section_sections.push(section);
                &mut moved_section_ids
            },
        };

        target_vec.push(i.request);
    }

    if !not_accepted.is_empty() {
        sqlx::query!(
            r#"UPDATE requests SET position = NULL, accepted_as_guest = FALSE WHERE id = ANY($1)"#,
            &not_accepted as &[RequestId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !guests.is_empty() {
        sqlx::query!(
            r#"UPDATE requests SET position = NULL, accepted_as_guest = TRUE WHERE id = ANY($1)"#,
            &guests as &[RequestId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !positioned_ids.is_empty() {
        let is_guest_section = sqlx::query_scalar!(
            r#"SELECT guest FROM sections WHERE id = $1"#,
            &section as &SectionId,
        )
        .fetch_one(&mut *login.transaction)
        .await?;

        if is_guest_section {
            return Err(UserError::EventSectionPositioningInGuestSection.into());
        }

        sqlx::query!(
            r#"UPDATE requests SET position = NULL WHERE id = ANY($1)"#,
            &positioned_ids as &[RequestId],
        )
        .execute(&mut *login.transaction)
        .await?;

        sqlx::query!(
            r#"
                UPDATE requests
                SET position = updated_position, accepted_as_guest = FALSE
                FROM UNNEST(CAST($1 AS UUID[]), CAST($2 AS INTEGER[]))
                    AS updated(updated_id, updated_position)
                WHERE id = updated_id
            "#,
            &positioned_ids as &[RequestId],
            &positioned_positions as &[i32],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !moved_section_ids.is_empty() {
        sqlx::query!(
            r#"
                UPDATE requests
                SET section = updated_section, position = NULL, accepted_as_guest = FALSE
                FROM UNNEST(CAST($1 AS UUID[]), CAST($2 AS TEXT[]))
                    AS updated(updated_id, updated_section)
                WHERE id = updated_id AND event = $3 AND section = $4 AND filled
            "#,
            &moved_section_ids as &[RequestId],
            &moved_section_sections as &[SectionId],
            event as EventId,
            &section as &SectionId,
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

async fn get_all_sections(mut login: LoginInfo) -> EndpointResult {
    let sections = sqlx::query!(
        r#"
            SELECT
                id AS "basic_id!: SectionId",
                name AS "basic_name!: NonEmptyString",
                guest AS "basic_guest!",
                tags AS "tags!: Vec<NonEmptyString>",
                stats AS "stats!: SectionStats"
            FROM sections_with_tags
        "#
    )
    .try_map(|x| ExtendedSectionOverview!(x))
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&sections).into_response())
}

async fn get_section_heads(mut login: LoginInfo) -> EndpointResult {
    let section_heads = sqlx::query_as!(
        SectionHeadsRow,
        r#"
            SELECT
                section AS "section!: SectionId",
                head AS "head_id!: UserId", email AS "head_email: Email"
            FROM section_heads JOIN management_users ON head = id
        "#
    )
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(json(&section_heads).into_response())
}

async fn update_tags(
    login: &mut LoginInfo,
    section: &SectionId,
    tags: &[NonEmptyString],
) -> Result<(), sqlx::Error> {
    // Okay since there's no other fields and this is happening in a transaction
    sqlx::query!(
        r#"DELETE FROM section_tags WHERE section = $1"#,
        &section as &SectionId,
    )
    .execute(&mut *login.transaction)
    .await?;

    sqlx::query!(
        r#"
            INSERT INTO section_tags(section, tag)
            SELECT $1 AS section, tag
            FROM UNNEST(CAST($2 AS TEXT[])) AS input(tag)
        "#,
        section as &SectionId,
        tags as &[NonEmptyString],
    )
    .execute(&mut *login.transaction)
    .await?;

    Ok(())
}

async fn put_section(section: SectionId, body: SectionPut, mut login: LoginInfo) -> EndpointResult {
    if body.guest && (body.stats.youth_leaders != 0i32 || body.stats.member_count != 0i32) {
        return Err(UserError::GuestSectionStatsNotZero.into());
    }

    let guest = sqlx::query_scalar!(
        r#"
            INSERT INTO sections(id, name, stats, guest) VALUES ($1, $2, ($3, $4, $5), $6)
            ON CONFLICT (id) DO UPDATE
            SET name = EXCLUDED.name, stats = EXCLUDED.stats
            RETURNING guest
        "#,
        &section as &SectionId,
        &body.name,
        body.stats.youth_leaders,
        body.stats.member_count,
        body.stats.last_stat_update,
        body.guest,
    )
    .fetch_one(&mut *login.transaction)
    .await?;

    if guest != body.guest {
        login.transaction.rollback().await?;
        return Err(UserError::SectionGuestChange.into());
    }

    update_tags(&mut login, &section, &body.tags).await?;

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

async fn get_section(section: SectionId, mut login: LoginInfo) -> EndpointResult {
    #[derive(Macrow)]
    struct DbSection {
        #[macrow(flatten)]
        overview: crate::section::ExtendedSectionOverview,
        heads: Vec<BasicUserInfo>,
    }

    let data = sqlx::query!(
        r#"
            SELECT
                id AS "overview_basic_id!: SectionId",
                name AS "overview_basic_name!: NonEmptyString",
                guest AS "overview_basic_guest!",
                tags AS "overview_tags!: Vec<NonEmptyString>",
                stats AS "overview_stats!: SectionStats",
                ARRAY(
                    SELECT CAST(
                        (u.id, u.complete, u.email, u.display_name, u.kind, u.admin_for)
                            AS BASIC_USER_INFO
                    )
                    FROM management_users_with_admin_for AS u JOIN section_heads AS sh
                        ON u.id = sh.head
                    WHERE sh.section = sections_with_tags.id
                ) AS "heads!: Vec<BasicUserInfo>"
            FROM sections_with_tags
            WHERE id = $1
        "#,
        &section as &SectionId,
    )
    .try_map(|x| DbSection!(x))
    .fetch_optional(&mut *login.transaction)
    .await?
    .ok_or_else(|| UserError::DoesntExist)?;

    #[allow(clippy::if_then_some_else_none)] // `.await` doesn't with with `.then`
    let relevant_events = if login.user_kind == UserKind::SuperAdmin {
        Some(
            sqlx::query!(
                r#"
                    SELECT
                        id AS "id: EventId",
                        name AS "name: NonEmptyString", description,
                        state AS "state: EventState", signup_until, decisions_until, archived_until
                    FROM events JOIN event_sections ON events.id = event_sections.event
                    WHERE section = $1
                "#,
                &section as &SectionId,
            )
            .try_map(|x| EventOverview!(x))
            .fetch_all(&mut *login.transaction)
            .await?,
        )
    } else {
        None
    };

    login.transaction.commit().await?;

    let (heads, partial_heads) = split_users(data.heads, login.user_kind);

    Ok(json(&Section {
        overview: data.overview,
        heads,
        partial_heads,
        relevant_events,
    })
    .into_response())
}

async fn patch_section(
    section: SectionId,
    body: SectionDiff,
    mut login: LoginInfo,
    ctx: StaticContext,
) -> EndpointResult {
    section.authorized(&mut login).await?;

    sqlx::query!(
        r#"
            UPDATE sections
            SET name = COALESCE($1, name),
                stats.youth_leaders = COALESCE($2, (stats).youth_leaders),
                stats.member_count = COALESCE($3, (stats).member_count),
                stats.last_update = COALESCE($4, (stats).last_update)
            WHERE id = $5
        "#,
        body.name.as_deref(),
        body.youth_leaders
            .map(|i| i32::try_from(i))
            .transpose()
            .map_err(UserError::IntConv)?,
        body.member_count
            .map(|i| i32::try_from(i))
            .transpose()
            .map_err(UserError::IntConv)?,
        body.last_stat_update,
        &section as &SectionId,
    )
    .execute(&mut *login.transaction)
    .await?;

    if let Some(ref tags) = body.tags {
        update_tags(&mut login, &section, tags).await?;
    }

    // TODO: Overlap between add_heads and remove_heads?

    if !body.remove_heads.is_empty() {
        sqlx::query!(
            r#"DELETE FROM section_heads WHERE section = $1 AND head = ANY($2)"#,
            &section as &SectionId,
            &body.remove_heads as &[UserId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !body.add_heads.is_empty() {
        // TODO: email sending & such
        sqlx::query!(
            r#"
                INSERT INTO section_heads(section, head)
                SELECT $1 AS section, head
                FROM UNNEST(CAST($2 AS UUID[])) AS input(head)
                ON CONFLICT (section, head) DO NOTHING
            "#,
            &section as &SectionId,
            &body.add_heads as &[UserId],
        )
        .execute(&mut *login.transaction)
        .await?;
    }

    if !body.invite_heads.is_empty() {
        let ids = invite(&mut login.transaction, body.invite_heads, UserKind::Normal).await?;

        let added = sqlx::query!(
            r#"
                INSERT INTO section_heads(section, head)
                SELECT $1 AS section, head
                FROM UNNEST(CAST($2 AS UUID[])) AS input(head)
                ON CONFLICT (section, head) DO NOTHING
                RETURNING
                    head AS "id: UserId",
                    (SELECT email FROM management_users WHERE id = head) AS "email!: Email",
                    (SELECT partial_secret FROM management_users WHERE id = head)
                        AS "partial_secret: NonEmptyString",
                    (SELECT display_name FROM management_users WHERE id = head)
                        AS "display_name: NonEmptyString"
            "#,
            &section as &SectionId,
            &ids as &[UserId],
        )
        .fetch_all(&mut *login.transaction)
        .await?;

        let section_info = sqlx::query_as!(
            SectionOverview,
            r#"
                SELECT id AS "id: SectionId", name AS "name: NonEmptyString", guest
                FROM sections WHERE id = $1
            "#,
            &section as &SectionId,
        )
        .fetch_one(&mut *login.transaction)
        .await?;

        for row in added {
            email_debug::log(&row.email, "new-head", &section.0);

            if let Some(secret) = row.partial_secret {
                email_debug::log(&row.email, "signup", &secret);

                ctx.spawn_and_send_email(
                    "email/manage/signup.html",
                    row.email.parse()?,
                    SignupEmailContext {
                        signup_params: SignupParams {
                            email: row.email,
                            secret,
                        },
                        display_name: row.display_name,
                        reason: SignupReason::NewHead {
                            section: section_info.clone(),
                        },
                    },
                    None,
                );
            } else {
                #[derive(Serialize)]
                struct TemplateContext {
                    display_name: Option<NonEmptyString>,
                    section: SectionOverview,
                }

                ctx.spawn_and_send_email(
                    "email/manage/new-head.html",
                    row.email.parse()?,
                    TemplateContext {
                        display_name: row.display_name,
                        section: section_info.clone(),
                    },
                    None,
                );
            }
        }
    }

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

async fn delete_section(section: SectionId, mut login: LoginInfo) -> EndpointResult {
    section.authorized(&mut login).await?;

    sqlx::query!(
        r#"DELETE FROM sections WHERE id = $1"#,
        &section as &SectionId,
    )
    .execute(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    Ok(StatusCode::OK.into_response())
}

pub fn api(ctx: StaticContext) -> filter!((impl Reply,)) {
    router! {
        in ctx;
        recover handle_endpoint_rejections_api;

        let api_config = / "config" {
            #get !any do get_config(1);
            #patch %json<ConfigDiff> !SuperAdmin do patch_config(2);
        };

        let api_push_view = / "push-view" { #post %json<PushView> !any do post_push_view(2); };

        let api_all_events = / "events" { #get !Admin do get_all_events(1); };

        let api_new_event = / "event" { #post %json<CreateEvent> !Organizer do post_new_event(2); };

        let api_event = / "event" / EventId {
            #get !any do get_event(2);
            #patch %json<EventDiff> !Organizer =ctx do patch_event(4);
            #delete !Organizer do delete_event(2);
        };

        let api_event_all_info = / "event" / EventId / "all" / "info" ? EventSectionInfoParams {
            #get !Organizer =ctx do get_event_all_info(4);
        };

        let api_event_all_export = / "event" / EventId / "all" / "export" {
            #get !Organizer do get_event_all_export(2);
        };

        let api_event_all_notify_heads = / "event" / EventId / "all" / "notify-heads" {
            #post !Organizer =ctx do post_event_all_notify_heads(3);
        };

        let api_all_users = / "users" { #get !Organizer do get_all_users(1); };

        let api_all_partial_users = / "partial-users" {
            #get !Organizer do get_all_partial_users(1);
        };

        let api_user_reset_password = / "user" / UserId / "reset-password" {
            #post !SuperAdmin =ctx do post_user_reset_password(3);
        };

        let api_reset_all_passwords = / "user" / "mass-reset-password" {
            #post !SuperAdmin =ctx do post_user_all_mass_reset_password(2);
        };

        let api_create_user = / "user" {
            #post %json<CreateUser> !SuperAdmin =ctx do post_new_user(3);
        };

        let api_user = / "user" / UserIdOrSelf {
            #get !any do get_user(2);
            #patch %json<UserDiff> !any do patch_user(3);
            #delete !any do delete_user(2);
        };

        let api_event_section = / "event" / EventId / SectionId {
            #get !any =ctx do get_event_section(4);
            #patch %json<EventSectionDiff> !any =ctx do patch_event_section(5);
        };

        let api_all_sections = / "sections" { #get !Organizer do get_all_sections(1); };

        let api_section_heads = / "section-heads" { #get !SuperAdmin do get_section_heads(1); };

        let api_section = / "section" / SectionId {
            #put %json<SectionPut> !SuperAdmin do put_section(3);
            #get !Organizer do get_section(2);
            #patch %json<SectionDiff> !Admin =ctx do patch_section(4);
            #delete !Admin do delete_section(2);
        };
    }
}
