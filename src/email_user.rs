use std::{collections::HashMap, future::Future};

use macrow::Macrow;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value as JsonValue};
use validator::ValidateEmail;
use warp::{
    hyper::{self, header, StatusCode, Uri},
    redirect,
    reply::{with_status, Response},
    Filter, Reply,
};

use crate::{
    db::sqlx_err,
    email_debug,
    event::{
        event_state_to_error,
        form::{form_data_map, FilledForm, Form},
        BeforeArchival, BeforeSignupDeadline, EventOverview, EventState, FullEventState,
    },
    event_section::{MaybePartialTemplateRequest, TemplatePartialRequest, TemplateRequest},
    helpers::{Email, NonEmptyString},
    prelude::*,
    rejections::{auto_reject, with_err_status, Error, UserError},
    EmptyStruct,
};

pub mod auth;
use auth::{Credentials, LoginInfo};

#[derive(Serialize, Deserialize)]
struct SignupSecret {
    secret: NonEmptyString,
}

#[derive(Deserialize)]
struct SignupForm {
    email: NonEmptyString,
}

async fn handle_signup(
    event: EventId,
    section: SectionId,
    params: SignupSecret,
    form: Option<SignupForm>,
    ctx: StaticContext,
) -> EndpointResult {
    let error_and_event_section = sqlx::query!(
        r#"
            SELECT
                events.id AS "id: EventId",
                events.name AS "name: NonEmptyString", description,
                state AS "state!: EventState", signup_until, decisions_until, archived_until,
                sections.name AS "section_name: NonEmptyString"
            FROM events JOIN event_sections ON events.id = event_sections.event
                JOIN sections ON event_sections.section = sections.id
            WHERE event = $1 AND section = $2 AND secret = $3
        "#,
        event as EventId,
        &section as &SectionId,
        &params.secret,
    )
    .try_map(|row| {
        let event = EventOverview!(row)?;
        let now = crate::local_now().date();
        Ok((
            event_state_to_error(event.state, BeforeSignupDeadline { now }).err(),
            (event, row.section_name),
        ))
    });

    let signup_template = |error, (event_overview, section_name)| {
        with_err_status(
            ctx.html_template(
                "user/signup.html",
                &json!({
                    "event": event_overview,
                    "section": { "id": &section, "name": section_name },
                    "event_section_secret": &params,
                    "error": &error,
                }),
            ),
            error,
        )
    };

    match form {
        None => {
            let (error, event_section) = error_and_event_section
                .fetch_optional(&ctx.db)
                .await?
                .ok_or_else(|| UserError::DoesntExist)?;
            signup_template(error, event_section)
        },
        Some(SignupForm { email }) => {
            let mut transaction = ctx.db.begin().await?;

            let (error, event_section) = error_and_event_section
                .fetch_optional(&mut *transaction)
                .await?
                .ok_or_else(|| UserError::DoesntExist)?;

            let error = error.or_else(|| {
                if email.0.validate_email() {
                    None
                } else {
                    Some(UserError::EmailUserInvalidEmail)
                }
            });

            if error.is_some() {
                transaction.commit().await?;
                signup_template(error, event_section)
            } else {
                let dashboard_secret = sqlx::query_scalar!(
                    r#"
                        INSERT INTO email_users(email, secret)
                        VALUES ($1, encode(gen_random_bytes(32), 'hex'))
                        ON CONFLICT (email) DO UPDATE
                        SET secret = email_users.secret  -- so that returning works
                        RETURNING secret AS "x: NonEmptyString"
                    "#,
                    &email,
                )
                .fetch_one(&mut *transaction)
                .await?;

                let insert_result = sqlx::query_scalar!(
                    r#"
                        INSERT INTO requests(event, section, email, filled)
                        SELECT event, section, email, FALSE AS filled
                        FROM event_sections, email_users
                        WHERE event = $1 AND section = $2 AND email = $3
                            AND NOT EXISTS(
                                SELECT 1
                                FROM requests
                                WHERE requests.event = event_sections.event
                                    AND requests.section = event_sections.section
                                    AND requests.email = email_users.email
                                    AND NOT filled
                            )
                    "#,
                    event as EventId,
                    &section as &SectionId,
                    &email,
                )
                .execute(&mut *transaction)
                .await?;

                if insert_result.rows_affected() != 0 {
                    #[derive(Serialize, Macrow)]
                    struct InnerContext {
                        #[macrow(flatten)]
                        event: crate::event::TemplateEvent,
                        #[macrow(flatten)]
                        section: crate::section::TemplateSection,
                    }

                    #[derive(Serialize)]
                    struct TemplateContext {
                        login: Credentials,
                        #[serde(flatten)]
                        inner: InnerContext,
                    }

                    let email = Email(email.0);
                    email_debug::log(&email, "new-request", &dashboard_secret);

                    let event_section_data = sqlx::query!(
                        r#"
                            SELECT
                                events.id AS "event_overview_id: EventId",
                                events.name AS "event_overview_name: NonEmptyString",
                                events.description AS event_overview_description,
                                events.state AS "event_overview_state: EventState",
                                events.signup_until AS event_overview_signup_until,
                                events.decisions_until AS event_overview_decisions_until,
                                events.archived_until AS event_overview_archived_until,
                                events.form AS "event_form!",
                                can_users_leave AS event_can_users_leave,
                                can_users_edit_name AS event_can_users_edit_name,
                                sections.id AS "section_id: SectionId",
                                sections.name AS "section_name: NonEmptyString"
                            FROM events, sections
                            WHERE events.id = $1 AND sections.id = $2
                        "#,
                        event as EventId,
                        &section as &SectionId,
                    )
                    .fetch_one(&mut *transaction)
                    .await?;

                    ctx.spawn_and_send_email(
                        "email/user/signup.html",
                        email.parse()?,
                        TemplateContext {
                            login: Credentials {
                                email,
                                secret: dashboard_secret,
                            },
                            inner: InnerContext!(event_section_data)?,
                        },
                        None,
                    );
                }

                transaction.commit().await?;

                ctx.html_template("user/signup_result.html", &json!(EmptyStruct {}))
            }
        },
    }
}

macro_rules! form_or_redirect {
    (redirect via $fn:ident $(, $arg:expr)*) => {{
        return Ok(redirect::see_other($fn($($arg),*)).into_response());
    }};
    ($form:ident, $($redirect:tt)*) => {
        let Some($form) = $form else { form_or_redirect!(redirect via $($redirect)*); };
    };
}

async fn handle_reset_secret(
    event: EventId,
    section: SectionId,
    params: SignupSecret,
    form: Option<SignupForm>,
    ctx: StaticContext,
) -> EndpointResult {
    form_or_redirect!(form, fmt_signup, event, &section, &params);

    let new_dashboard_secret = sqlx::query_scalar!(
        r#"
            UPDATE email_users
            SET secret = encode(gen_random_bytes(32), 'hex')
            FROM event_sections
            WHERE event = $1 AND section = $2 AND event_sections.secret = $3 AND email = $4
            RETURNING email_users.secret AS "secret: NonEmptyString"
        "#,
        event as EventId,
        &section as &SectionId,
        &params.secret,
        &form.email,
    )
    .fetch_optional(&ctx.db)
    .await?;

    if let Some(new_dashboard_secret) = new_dashboard_secret {
        #[derive(Serialize)]
        struct TemplateContext {
            login: Credentials,
        }

        let email = Email(form.email.0);
        email_debug::log(&email, "secret-reset", &new_dashboard_secret);

        ctx.spawn_and_send_email(
            "email/user/dashboard-reset.html",
            email.parse()?,
            TemplateContext {
                login: Credentials {
                    email,
                    secret: new_dashboard_secret,
                },
            },
            None,
        );
    }

    ctx.html_template("user/reset_secret.html", &EmptyStruct {})
}

#[derive(Serialize)]
struct TemplateView {
    partial_requests: Vec<TemplatePartialRequest>,
    requests: Vec<TemplateRequest>,
}

#[derive(Serialize)]
struct TemplateContext {
    view: TemplateView,
    login: Credentials,
}

async fn dashboard_context(mut login: LoginInfo) -> Result<TemplateContext, Error> {
    let now = crate::local_now().date();

    sqlx::query!(
        r#"
            DELETE FROM requests
            USING events
            WHERE requests.event = events.id
                AND (events.state <> 'active' OR events.signup_until < $2) AND NOT requests.filled
                AND requests.email = $1
        "#,
        &login.params.email,
        now,
    )
    .execute(&mut *login.transaction)
    .await?;

    let all_requests = sqlx::query!(
        r#"
            SELECT
                requests.id AS "partial_id: RequestId",
                events.id AS "partial_event_overview_id: EventId",
                events.name AS "partial_event_overview_name: NonEmptyString",
                events.description AS partial_event_overview_description,
                events.state AS "partial_event_overview_state: EventState",
                events.signup_until AS partial_event_overview_signup_until,
                events.decisions_until AS partial_event_overview_decisions_until,
                events.archived_until AS partial_event_overview_archived_until,
                events.form AS "partial_event_form!",
                can_users_leave AS partial_event_can_users_leave,
                can_users_edit_name AS partial_event_can_users_edit_name,
                sections.id AS "partial_section_id: SectionId",
                sections.name AS "partial_section_name: NonEmptyString",
                event_sections.max_delegates AS partial_max_delegates,
                requests.first_name AS "first_name: NonEmptyString",
                requests.last_name AS last_name,
                filled_form,
                position AS current_placing,
                (
                    CASE WHEN position IS NULL
                        THEN NULL
                        ELSE (
                            SELECT CAST(COUNT(r2.id) AS INTEGER)
                            FROM requests AS r2
                            WHERE r2.event = requests.event AND r2.section = requests.section
                                AND r2.position IS NOT NULL AND r2.position < requests.position
                        )
                    END
                ) AS current_compressed_placing,
                accepted_as_guest
            FROM requests JOIN events ON requests.event = events.id
                JOIN sections ON requests.section = sections.id
                JOIN event_sections ON (
                    requests.event = event_sections.event
                        AND requests.section = event_sections.section
                )
            WHERE email = $1
        "#,
        &login.params.email,
    )
    .try_map(|request| {
        let request = MaybePartialTemplateRequest!(request)?;

        match (request.first_name, request.last_name, request.filled_form) {
            (None, None, None) => Ok(Err(request.partial)),
            (Some(first_name), Some(last_name), Some(filled_form)) => Ok(Ok(TemplateRequest {
                signup_allowed: match request.partial.event.overview.state {
                    FullEventState::Active { signup_until, .. } => now <= signup_until,
                    _ => false,
                },
                partial: request.partial,
                first_name,
                last_name,
                form: filled_form.try_into()?,
                current_placing: request.current_placing,
                current_compressed_placing: request.current_compressed_placing,
                accepted_as_guest: request.accepted_as_guest,
            })),
            (_, _, _) => Err(sqlx_err("Name and form not coherent")),
        }
    })
    .fetch_all(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    let mut partial_requests = Vec::new();
    let mut requests = Vec::new();

    for i in all_requests {
        match i {
            Ok(filled) => requests.push(filled),
            Err(partial) => partial_requests.push(partial),
        }
    }

    Ok(TemplateContext {
        view: TemplateView {
            partial_requests,
            requests,
        },
        login: login.params,
    })
}

async fn dashboard_action<'a>(
    task: &'static str,
    params: &'a Credentials,
    value: impl Future<Output = Result<Option<JsonValue>, Error>> + Send + 'a,
    ctx: StaticContext,
) -> Result<Response, Error> {
    #[derive(Serialize)]
    struct DashboardActionContext<'a> {
        task: &'static str,
        error: Option<&'a UserError<'static>>,
        login: &'a Credentials,
        #[serde(skip_serializing_if = "Option::is_none")]
        #[serde(flatten)]
        rest: Option<JsonValue>,
    }

    match value.await {
        Ok(out) => ctx.html_template("user/dashboard_action.html", &DashboardActionContext {
            task,
            error: None,
            login: params,
            rest: out,
        }),
        Err(err @ Error::UserError(UserError::EmailUserLoginFailure)) => Err(err),
        Err(Error::UserError(UserError::RedirectToEmailUserDashboard)) => Ok(redirect::see_other(
            Uri::try_from(format!(
                "/user/dashboard?{}",
                serde_qs::to_string(params).unwrap()
            ))
            .unwrap(),
        )
        .into_response()),
        Err(ref full_err @ Error::UserError(ref err)) => Ok(with_status(
            ctx.html_template("user/dashboard_action.html", &DashboardActionContext {
                task,
                error: Some(err),
                login: params,
                rest: None,
            })?,
            full_err.status(),
        )
        .into_response()),
        Err(err) => Err(err),
    }
}

macro_rules! dashboard_action {
    {
        struct $form_ty:ident { $($(#[$attr:meta])* $field:ident: $field_ty:ty),* $(,)? }
        async fn $name:ident / $lit:literal($login:ident, $form:ident, $ctx:ident) { $($body:tt)* }
    } => {
        #[derive(Deserialize)]
        struct $form_ty {
            $($(#[$attr])* $field: $field_ty,)*
        }

        async fn $name(
            mut $login: LoginInfo,
            $form: Option<$form_ty>,
            $ctx: StaticContext,
        ) -> EndpointResult {
            form_or_redirect!($form, fmt_dashboard, &$login.params);
            dashboard_action($lit, &$login.params, async { $($body)* }, $ctx).await
        }
    }
}

dashboard_action! {
    struct RemovePartialForm {
        event: EventId,
        section: SectionId,
    }

    async fn handle_remove_partial / "remove" (login, form, ctx) {
        let delete_result = sqlx::query_scalar!(
            r#"
                DELETE FROM requests WHERE NOT filled AND event = $1 AND section = $2 AND email = $3
            "#,
            form.event as EventId,
            &form.section as &SectionId,
            &login.params.email,
        )
        .execute(&mut *login.transaction)
        .await?;

        if delete_result.rows_affected() == 0 {
            return Err(UserError::RequestGone.into());
        }

        login.transaction.commit().await?;

        Ok(None)
    }
}

dashboard_action! {
    struct FinalizeRequestForm {
        event: EventId,
        section: SectionId,
        first_name: String,
        last_name: String,
        #[serde(flatten)]
        form: HashMap<String, String>,
    }

    async fn handle_finalize_request / "finalize" (login, form, ctx) {
        let (event_form, event_state) = sqlx::query!(
            r#"
                SELECT
                    form,
                    state AS "state: EventState", signup_until, decisions_until, archived_until
                FROM events
                WHERE id = $1
            "#,
            form.event as EventId,
        )
        .try_map(|x| Ok((x.form, FullEventState!(x)?)))
        .fetch_optional(&mut *login.transaction)
        .await?
        .ok_or_else(|| UserError::RequestGone)?;

        let now = crate::local_now().date();
        event_state_to_error(event_state, BeforeSignupDeadline { now })?;

        let filled_form_result = FilledForm::validate_form(
            &Form::try_from(event_form)?,
            None,
            &form_data_map(form.form),
        );

        let empty_name = form.first_name.is_empty();

        let filled_form = match (filled_form_result, empty_name) {
            (Ok(filled_form), false) => filled_form,
            (filled_form_result, empty_name) => {
                return Err(UserError::RequestFormError {
                    empty_name,
                    name_change_not_allowed: false,  // inserting new
                    form: filled_form_result.err().unwrap_or_else(Vec::new),
                }.into());
            },
        };

        let request_id = sqlx::query_scalar!(
            r#"
                UPDATE requests
                SET first_name = $1, last_name = $2, filled_form = $3, filled = TRUE
                WHERE NOT filled AND event = $4 AND section = $5 AND email = $6
                RETURNING id AS "id: RequestId"
            "#,
            &form.first_name,
            &form.last_name,
            &(<Vec<serde_json::Value>>::try_from(&filled_form)?),
            form.event as EventId,
            &form.section as &SectionId,
            &login.params.email,
        )
        .fetch_optional(&mut *login.transaction)
        .await?
        .ok_or_else(|| UserError::RequestGone)?;

        login.transaction.commit().await?;

        Ok(Some(json!({ "created_request": request_id })))
    }
}

dashboard_action! {
    struct CopyToPartialForm {
        request: RequestId,
        redirect: bool,
    }

    async fn handle_copy_to_partial / "copy" (login, form, ctx) {
        let now = crate::local_now().date();

        let insert_result = sqlx::query_scalar!(
            r#"
                INSERT INTO requests(event, section, email, filled)
                SELECT event, section, email, FALSE AS filled
                FROM requests JOIN events ON requests.event = events.id
                WHERE requests.id = $1 AND email = $2 AND filled AND state = 'active'
                    AND $3 <= signup_until
                    AND NOT EXISTS(
                        SELECT 1
                        FROM requests AS r
                        WHERE r.event = requests.event
                            AND r.section = requests.section AND r.email = requests.email
                            AND NOT filled
                    )
            "#,
            form.request as RequestId,
            &login.params.email,
            now,
        )
        .execute(&mut *login.transaction)
        .await?;

        // If we didn't, we need to find the reason.
        // Possible reasons are:
        // 1. `id = $1 AND email = $2 AND filled` never matched
        // 2. `state = 'active' AND {now} <= signup_until` never matched
        // 3. `AND NOT EXISTS` never matched

        if insert_result.rows_affected() == 0 {
            let state = sqlx::query!(
                r#"
                    SELECT
                        state AS "state: EventState", signup_until, decisions_until, archived_until
                    FROM requests JOIN events ON requests.event = events.id
                    WHERE requests.id = $1 AND email = $2 AND filled
                "#,
                form.request as RequestId,
                &login.params.email,
            )
            .try_map(|x| FullEventState!(x))
            .fetch_optional(&mut *login.transaction)
            .await?
            .ok_or_else(|| UserError::RequestGone)?;

            event_state_to_error(state, BeforeSignupDeadline { now })?;

            // Could have only been #3 which we also count as a success
        }

        login.transaction.commit().await?;

        if form.redirect {
            Err(UserError::RedirectToEmailUserDashboard.into())
        } else {
            Ok(None)
        }
    }
}

dashboard_action! {
    struct UpdateRequestForm {
        request: RequestId,
        first_name: String,
        last_name: String,
        #[serde(flatten)]
        form: HashMap<String, String>,
    }

    async fn handle_update_request / "update" (login, form, ctx) {
        // This can theoretically have a race condition where our "original" form we get isn't
        // actually the one it was before; however that doesn't matter as we only want to make sure
        // the fields that are supposed to stay constant aren't changed, which this does guarantee
        // either way.

        let info = sqlx::query!(
            r#"
                SELECT
                    form,
                    state AS "state: EventState", signup_until, decisions_until, archived_until,
                    can_users_edit_name,
                    filled_form AS "filled_form!",
                    requests.first_name AS "request_first_name!: NonEmptyString",
                    requests.last_name AS "request_last_name!"
                FROM events JOIN requests ON events.id = requests.event
                WHERE requests.id = $1 AND requests.filled AND requests.email = $2
            "#,
            form.request as RequestId,
            &login.params.email,
        )
        .fetch_optional(&mut *login.transaction)
        .await?
        .ok_or_else(|| UserError::RequestGone)?;

        event_state_to_error(FullEventState!(info)?, BeforeArchival)?;

        let filled_form_result = FilledForm::validate_form(
            &info.form.try_into()?,
            None,
            &form_data_map(form.form),
        );

        let empty_name = form.first_name.is_empty();

        let name_change_not_allowed = !info.can_users_edit_name && (
            form.first_name != info.request_first_name.0 || form.last_name != info.request_last_name
        );

        let filled_form = match (filled_form_result, empty_name, name_change_not_allowed) {
            (Ok(filled_form), false, false) => filled_form,
            (filled_form_result, _, _) => {
                return Err(UserError::RequestFormError {
                    empty_name,
                    name_change_not_allowed,
                    form: filled_form_result.err().unwrap_or_else(Vec::new),
                }.into());
            },
        };

        let result = sqlx::query!(
            r#"
                UPDATE requests SET first_name = $1, last_name = $2, filled_form = $3 WHERE id = $4
            "#,
            &form.first_name,
            &form.last_name,
            &(<Vec<JsonValue>>::try_from(&filled_form)?),
            form.request as RequestId,
        )
        .execute(&mut *login.transaction)
        .await?;

        if result.rows_affected() == 0 {
            return Err(UserError::RequestGone.into());
        }

        login.transaction.commit().await?;

        Ok(None)
    }
}

dashboard_action! {
    struct RemoveRequestForm {
        request: RequestId,
    }

    async fn handle_remove_request / "remove" (login, form, ctx) {
        let result = sqlx::query!(
            r#"
                DELETE FROM requests
                USING events
                WHERE events.id = requests.event AND filled AND requests.id = $1
                    AND email = $2 AND can_users_leave AND state = 'active'
            "#,
            form.request as RequestId,
            &login.params.email,
        )
        .execute(&mut *login.transaction)
        .await?;

        if result.rows_affected() == 0 {
            let state = sqlx::query!(
                r#"
                    SELECT
                        state AS "state: EventState", signup_until, decisions_until, archived_until
                    FROM requests JOIN events ON requests.event = events.id
                    WHERE filled AND requests.id = $1 AND email = $2
                "#,
                form.request as RequestId,
                &login.params.email,
            )
            .try_map(|x| FullEventState!(x))
            .fetch_optional(&mut *login.transaction)
            .await?
            .ok_or_else(|| UserError::RequestGone)?;

            event_state_to_error(state, BeforeArchival)?;

            // the only real reason can be that users couldn't leave

            return Err(UserError::RequestRemovalNotAllowed.into());
        }

        login.transaction.commit().await?;

        Ok(None)
    }
}

async fn handle_gdpr(login: LoginInfo) -> EndpointResult {
    Ok(hyper::Response::builder()
        .header(header::CONTENT_TYPE, "application/json")
        .header(
            header::CONTENT_DISPOSITION,
            "attachment; filename=\"jedi-gdpr-export.json\"",
        )
        .body(
            serde_json::to_string_pretty(&dashboard_context(login).await?)
                .map_err(Error::GeneratedJson)?,
        )?
        .into_response())
}

async fn handle_remove_user(mut login: LoginInfo, ctx: StaticContext) -> EndpointResult {
    let result = sqlx::query!(
        r#"
            DELETE FROM email_users
            WHERE email = $1 AND NOT EXISTS(
                SELECT 1
                FROM requests JOIN events ON requests.event = events.id
                WHERE requests.email = email_users.email AND NOT can_users_leave
            )
        "#,
        &login.params.email,
    )
    .execute(&mut *login.transaction)
    .await?;

    login.transaction.commit().await?;

    let (error, status) = if result.rows_affected() == 0 {
        (
            Some(UserError::RequestRemovalNotAllowed),
            StatusCode::FORBIDDEN,
        )
    } else {
        (None, StatusCode::OK)
    };

    #[allow(clippy::items_after_statements)]
    #[derive(Serialize)]
    struct TemplateContext {
        login: Credentials,
        error: Option<UserError<'static>>,
    }

    Ok(with_status(
        ctx.html_template("user/remove_result.html", &TemplateContext {
            login: login.params,
            error,
        })?,
        status,
    )
    .into_response())
}

fn fmt_signup(event: EventId, section: &SectionId, params: &SignupSecret) -> Uri {
    let query = serde_qs::to_string(params).unwrap();
    format!("/user/event/{event}/{section}/signup?{query}")
        .parse()
        .unwrap()
}

fn fmt_dashboard(params: &Credentials) -> Uri {
    let query = serde_qs::to_string(params).unwrap();
    format!("/user/dashboard/signup?{query}").parse().unwrap()
}

pub fn app(ctx: StaticContext) -> filter!((Response,)) {
    router! {
        in ctx;
        recover handle_endpoint_rejections_human;

        let user_signup = / "user" / "event" / EventId / SectionId / "signup" ? SignupSecret {
            #%form<SignupForm> =ctx do handle_signup(5);
        };

        let user_reset_secret =
            / "user" / "event" / EventId / SectionId / "reset-secret" ? SignupSecret {
                #%form<SignupForm> =ctx do handle_reset_secret(5);
            };

        let user_dashboard = / "user" / "dashboard" ? Credentials {
            #[head, get] !email_user .and_then(move |login| auto_reject(async move {
                ctx.html_template("user/dashboard.html", &dashboard_context(login).await?)
            }));
        };

        let user_remove_partial = / "user" / "remove-partial" ? Credentials {
            !email_user #%form<RemovePartialForm> =ctx do handle_remove_partial(3);
        };

        let user_finalize_request = / "user" / "finalize-request" ? Credentials {
            !email_user #%form<FinalizeRequestForm> =ctx do handle_finalize_request(3);
        };

        let user_copy_to_partial = / "user" / "copy-to-partial" ? Credentials {
            !email_user #%form<CopyToPartialForm> =ctx do handle_copy_to_partial(3);
        };

        let user_update_request = / "user" / "update-request" ? Credentials {
            !email_user #%form<UpdateRequestForm> =ctx do handle_update_request(3);
        };

        let user_remove_request = / "user" / "remove-request" ? Credentials {
            !email_user #%form<RemoveRequestForm> =ctx do handle_remove_request(3);
        };

        let user_gdpr = / "user" / "gdpr" ? Credentials {
            #[head, get] !email_user do handle_gdpr(1);
        };

        let user_remove_user = / "user" / "remove-user" ? Credentials {
            #[head, get] !email_user
                .map(|login: LoginInfo| redirect::see_other(fmt_dashboard(&login.params)));
            #post !email_user =ctx do handle_remove_user(2);
        };
    }
}
