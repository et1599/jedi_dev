use pulldown_cmark::{html, Options, Parser};

use crate::prelude::*;

#[derive(Clone, Serialize, Deserialize, Debug, TS)]
#[ts(export)]
pub struct UnrenderedMarkdown {
    pub source: String,
}

#[derive(Clone, Serialize, TS)]
#[ts(export)]
pub struct RenderedMarkdown {
    source: String,
    html: String,
}

impl RenderedMarkdown {
    pub fn render(source: String) -> RenderedMarkdown {
        let parser = Parser::new_ext(
            &source,
            Options::ENABLE_FOOTNOTES
                | Options::ENABLE_STRIKETHROUGH
                | Options::ENABLE_TABLES
                | Options::ENABLE_TASKLISTS,
        );

        let mut uncleaned = String::new();
        html::push_html(&mut uncleaned, parser);

        RenderedMarkdown {
            html: ammonia::clean(&uncleaned),
            source,
        }
    }
}

impl TryFrom<String> for RenderedMarkdown {
    type Error = sqlx::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(Self::render(value))
    }
}
