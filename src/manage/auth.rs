use async_trait::async_trait;
use cookie::Cookie;
use warp::{Filter, Rejection};

use crate::{
    management_user::UserKind,
    prelude::*,
    rejections::{auto_reject, Error, UserError},
    warp_ext::{remove_cookie, NotSensitive, Sensitive, Sensitivity},
    AppContext,
};

pub enum AuthorizationError {
    DoesntExist,
    NotAuthorized,
    SqlError(sqlx::Error),
}

impl AuthorizationError {
    pub fn from_bool_option(value: Option<bool>) -> Result<(), AuthorizationError> {
        match value {
            None => Err(AuthorizationError::DoesntExist),
            Some(false) => Err(AuthorizationError::NotAuthorized),
            Some(true) => Ok(()),
        }
    }
}

impl From<sqlx::Error> for AuthorizationError {
    fn from(error: sqlx::Error) -> Self {
        AuthorizationError::SqlError(error)
    }
}

impl From<AuthorizationError> for Error {
    fn from(error: AuthorizationError) -> Self {
        match error {
            AuthorizationError::DoesntExist => UserError::DoesntExist.into(),
            AuthorizationError::NotAuthorized => UserError::NotAllowed.into(),
            AuthorizationError::SqlError(error) => error.into(),
        }
    }
}

#[async_trait]
pub trait CheckPermissions {
    async fn explicitly_authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError>;
    async fn exists(&self, login: &mut LoginInfo) -> Result<bool, sqlx::Error>;

    async fn authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError> {
        if login.user_kind == UserKind::SuperAdmin {
            if self.exists(login).await? {
                Ok(())
            } else {
                Err(AuthorizationError::DoesntExist)
            }
        } else {
            self.explicitly_authorized(login).await
        }
    }
}

#[async_trait]
impl CheckPermissions for UserId {
    async fn explicitly_authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError> {
        if *self == login.user_id {
            Ok(())
        } else if self.exists(login).await? {
            Err(AuthorizationError::NotAuthorized)
        } else {
            Err(AuthorizationError::DoesntExist)
        }
    }

    async fn exists(&self, login: &mut LoginInfo) -> Result<bool, sqlx::Error> {
        sqlx::query_scalar!(
            r#"SELECT EXISTS(SELECT 1 FROM management_users WHERE id = $1) AS "exists!""#,
            *self as UserId,
        )
        .fetch_one(&mut *login.transaction)
        .await
    }
}

#[async_trait]
impl CheckPermissions for EventId {
    async fn explicitly_authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError> {
        AuthorizationError::from_bool_option(
            sqlx::query_scalar!(
                r#"
                    SELECT $1 IN (SELECT organizer FROM event_organizers WHERE event = events.id)
                        AS "allowed!"
                    FROM events
                    WHERE id = $2
                "#,
                login.user_id as UserId,
                *self as EventId,
            )
            .fetch_optional(&mut *login.transaction)
            .await?,
        )
    }

    async fn exists(&self, login: &mut LoginInfo) -> Result<bool, sqlx::Error> {
        sqlx::query_scalar!(
            r#"SELECT EXISTS(SELECT 1 FROM events WHERE id = $1) AS "exists!""#,
            *self as EventId,
        )
        .fetch_one(&mut *login.transaction)
        .await
    }
}

#[async_trait]
impl CheckPermissions for SectionId {
    async fn explicitly_authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError> {
        AuthorizationError::from_bool_option(
            sqlx::query_scalar!(
                r#"
                    SELECT $1 IN (
                        SELECT user_id FROM management_user_admin_for WHERE section = sections.id
                    ) AS "allowed!"
                    FROM sections WHERE id = $2
                "#,
                login.user_id as UserId,
                self as &SectionId,
            )
            .fetch_optional(&mut *login.transaction)
            .await?,
        )
    }

    async fn exists(&self, login: &mut LoginInfo) -> Result<bool, sqlx::Error> {
        sqlx::query_scalar!(
            r#"SELECT EXISTS(SELECT 1 FROM sections WHERE id = $1) AS "exists!""#,
            self as &SectionId,
        )
        .fetch_one(&mut *login.transaction)
        .await
    }
}

pub async fn event_section_explicitly_authorized(
    (event, section): (EventId, &SectionId),
    login: &mut LoginInfo,
    include_helpers: bool,
) -> Result<(), AuthorizationError> {
    // organizers, admins, heads & *maybe* helpers can do things
    AuthorizationError::from_bool_option(
        sqlx::query_scalar!(
            r#"
                SELECT $1 IN (
                    SELECT user_id FROM management_user_admin_for WHERE section = es.section
                    UNION SELECT organizer FROM event_organizers WHERE event = es.event
                    UNION SELECT head FROM section_heads WHERE section = es.section
                    UNION
                        SELECT helper
                        FROM event_section_helpers
                        WHERE event = es.event AND section = es.section AND $4
                ) AS "allowed!"
                FROM event_sections AS es
                WHERE event = $2 AND section = $3
            "#,
            login.user_id as UserId,
            event as EventId,
            section as &SectionId,
            include_helpers,
        )
        .fetch_optional(&mut *login.transaction)
        .await?,
    )
}

pub async fn event_section_exists(
    (event, section): (EventId, &SectionId),
    login: &mut LoginInfo,
) -> Result<bool, sqlx::Error> {
    sqlx::query_scalar!(
        r#"
            SELECT EXISTS(SELECT 1 FROM event_sections WHERE event = $1 AND section = $2) AS "out!"
        "#,
        event as EventId,
        section as &SectionId,
    )
    .fetch_one(&mut *login.transaction)
    .await
}

pub struct Basic<T>(pub T);

#[async_trait]
impl CheckPermissions for Basic<(EventId, &'_ SectionId)> {
    async fn explicitly_authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError> {
        event_section_explicitly_authorized(self.0, login, true).await
    }

    async fn exists(&self, login: &mut LoginInfo) -> Result<bool, sqlx::Error> {
        event_section_exists(self.0, login).await
    }
}

#[async_trait]
impl CheckPermissions for (EventId, &'_ SectionId) {
    async fn explicitly_authorized(&self, login: &mut LoginInfo) -> Result<(), AuthorizationError> {
        event_section_explicitly_authorized(*self, login, false).await
    }

    async fn exists(&self, login: &mut LoginInfo) -> Result<bool, sqlx::Error> {
        event_section_exists(*self, login).await
    }
}

const LOGIN_COOKIE_NAME: &str = "login-token";

// TODO: Rename to `unchecked_login`
// TODO: Maybe refactor somehow such that it doesn't trigger the ICE in newer
// Rust versions
pub async fn login(
    user_id: UserId,
    db: &mut sqlx::PgConnection,
) -> Result<(Cookie<'static>, Sensitivity), Error> {
    let session_id = sqlx::query_scalar!(
        r#"INSERT INTO sessions(user_id) VALUES ($1) RETURNING id AS "id: SessionId""#,
        user_id as UserId,
    )
    .fetch_one(db)
    .await?;

    let login_cookie = Cookie::build((LOGIN_COOKIE_NAME, session_id.0.simple().to_string()))
        .http_only(true)
        .same_site(cookie::SameSite::Strict);

    #[cfg(any(debug_assertions, feature = "debug"))]
    let login_cookie = login_cookie.build();

    #[cfg(not(any(debug_assertions, feature = "debug")))]
    let login_cookie = login_cookie.secure(true).build();

    Ok((login_cookie, Sensitive))
}

pub fn with_login_unchecked() -> filter!((SessionId,)) {
    warp::cookie(LOGIN_COOKIE_NAME)
        .recover(|_| async { Err(Rejection::from(Error::from(UserError::NotLoggedIn))) })
        .unify()
}

#[derive(Serialize, Macrow)]
pub struct LoginInfo {
    #[serde(skip)]
    #[macrow(context)]
    pub transaction: sqlx::Transaction<'static, sqlx::Postgres>,
    pub session: SessionId,
    pub user_id: UserId,
    pub user_kind: UserKind,
    pub admin_for: Vec<SectionId>,
}

pub fn with_login(ctx: &'static AppContext) -> filter!((LoginInfo,)) {
    with_login_unchecked().and_then(move |session: SessionId| {
        auto_reject(async move {
            // NOTE: `NOW()` returns the transaction time [so is constant across a
            // transaction], so calling it multiple times / for multiple rows
            // doesn't incur weird behaviour

            tokio::spawn(async move {
                // Clean up old sessions
                let _ = sqlx::query!(
                    r#"
                        DELETE FROM sessions
                        WHERE last_touched <= NOW() - (SELECT signin_length FROM meta)
                    "#
                )
                .execute(&ctx.db)
                .await;
            });

            let mut transaction = ctx.db.begin().await?;

            let user = sqlx::query!(
                r#"
                    UPDATE valid_sessions
                    SET last_touched = now()
                    FROM management_users_with_admin_for as u
                    WHERE valid_sessions.id = $1 AND u.id = valid_sessions.user_id
                    RETURNING
                        valid_sessions.id AS "session!: SessionId",
                        u.id AS "user_id!: UserId",
                        u.kind AS "user_kind!: UserKind",
                        u.admin_for AS "admin_for!: Vec<SectionId>"
                "#,
                session.0,
            )
            .fetch_optional(&mut *transaction)
            .await?
            .ok_or(UserError::NotLoggedIn)?;

            Ok::<_, Error>(LoginInfo!(user; transaction = transaction)?)
        })
    })
}

pub fn with_login_kind(ctx: &'static AppContext, expected: UserKind) -> filter!((LoginInfo,)) {
    with_login(ctx).and_then(move |login: LoginInfo| async move {
        if login.user_kind >= expected {
            Ok(login)
        } else {
            Err(Rejection::from(Error::from(UserError::NotAllowed)))
        }
    })
}

pub async fn logout(
    ctx: StaticContext,
    session_id: SessionId,
) -> Result<(Cookie<'static>, Sensitivity), Error> {
    sqlx::query!(
        r#"DELETE FROM sessions WHERE id = $1"#,
        session_id as SessionId,
    )
    .execute(&ctx.db)
    .await?;

    let login_cookie = remove_cookie(LOGIN_COOKIE_NAME);

    Ok((login_cookie, NotSensitive))
}
