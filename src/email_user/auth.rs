use std::future::Future;

use warp::Rejection;

use crate::{
    helpers::{Email, NonEmptyString},
    prelude::*,
    rejections::auto_reject,
};

pub struct LoginInfo {
    pub transaction: sqlx::Transaction<'static, sqlx::Postgres>,
    pub params: Credentials,
}

#[derive(Serialize, Deserialize, Clone, sqlx::Type, Debug)]
pub struct Credentials {
    pub email: Email,
    pub secret: NonEmptyString,
}

impl Credentials {
    pub fn check(self, ctx: StaticContext) -> impl Future<Output = Result<LoginInfo, Rejection>> {
        auto_reject(async move {
            let mut transaction = ctx.db.begin().await?;

            let valid = sqlx::query_scalar!(
                r#"
                    SELECT
                        EXISTS(SELECT email FROM email_users WHERE email = $1 AND secret = $2)
                            AS "valid!"
                "#,
                &self.email,
                &self.secret,
            )
            .fetch_one(&mut *transaction)
            .await?;

            if valid {
                Ok(LoginInfo {
                    transaction,
                    params: self,
                })
            } else {
                Err(UserError::EmailUserLoginFailure.into())
            }
        })
    }
}
