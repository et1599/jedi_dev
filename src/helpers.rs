use std::{fmt, ops};

use pwhash::bcrypt;
use serde::{Deserialize, Serialize};
use sqlx::Type;
use validator::ValidateEmail;

use crate::rejections::Error;

macro_rules! helper {
    {
        $($vis:vis struct $name:ident(pub $value:ident: String) or $err:literal { $validate:expr })*
    } => {
        $(
            #[derive(Clone, PartialEq, Eq, Hash, Serialize, Deserialize, Type, Debug)]
            #[sqlx(transparent)]
            #[serde(try_from = "String")]
            $vis struct $name(pub String);

            impl TryFrom<String> for $name {
                type Error = &'static str;

                fn try_from($value: String) -> Result<Self, &'static str> {
                    if $validate {
                        Ok($name($value))
                    } else {
                        Err($err)
                    }
                }
            }

            forward_ts!(impl TS for $name = String);

            impl From<$name> for String {
                fn from(value: $name) -> String {
                    value.0
                }
            }

            impl ops::Deref for $name {
                type Target = str;

                fn deref(&self) -> &str {
                    &self.0
                }
            }

            impl fmt::Display for $name {
                fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                    <String as fmt::Display>::fmt(&self.0, f)
                }
            }
        )*
    };
}

helper! {
    pub struct Email(pub value: String) or "invalid email" {
        value.validate_email()
    }

    pub struct NonEmptyString(pub value: String) or "expected a non-empty string" {
        !value.is_empty()
    }

    pub struct PasswordHash(pub value: String) or "unreachable" {
        true
    }
}

#[derive(Serialize, Deserialize, Default)]
#[serde(transparent)]
pub struct Password(pub String);

impl Password {
    pub fn hash(&self) -> Result<PasswordHash, Error> {
        Ok(PasswordHash(bcrypt::hash(&*self.0)?))
    }

    pub fn verify(&self, hash: &PasswordHash) -> bool {
        bcrypt::verify(&*self.0, hash)
    }
}

impl fmt::Debug for Password {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Password(...)")
    }
}

forward_ts!(impl TS for Password = String);
