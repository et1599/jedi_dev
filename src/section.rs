use crate::{
    event::EventOverview,
    helpers::{Email, NonEmptyString},
    management_user::{Invitation, PartialUserOverview, UserOverview},
    prelude::*,
};

#[derive(Clone, Serialize, TS, Macrow, Debug)]
#[ts(export)]
pub struct SectionOverview {
    pub id: SectionId,
    pub name: NonEmptyString,
    pub guest: bool,
}

// #[derive(sqlx::Type)]
// #[sqlx(type_name = "BASIC_SECTION_INFO")]
#[allow(clippy::needless_borrows_for_generic_args)] // Not going to mess with SQLX internals
impl sqlx::encode::Encode<'_, sqlx::Postgres> for SectionOverview {
    fn encode_by_ref(&self, buf: &mut sqlx::postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
        let mut encoder = sqlx::postgres::types::PgRecordEncoder::new(buf);
        encoder.encode(&self.id);
        encoder.encode(&self.name);
        encoder.encode(&self.guest);
        encoder.finish();
        sqlx::encode::IsNull::No
    }

    fn size_hint(&self) -> usize {
        2usize * (4 + 4)
            + <SectionId as sqlx::encode::Encode<::sqlx::Postgres>>::size_hint(&self.id)
            + <NonEmptyString as sqlx::encode::Encode<::sqlx::Postgres>>::size_hint(&self.name)
            + <bool as sqlx::encode::Encode<::sqlx::Postgres>>::size_hint(&self.guest)
    }
}

impl<'r> sqlx::decode::Decode<'r, sqlx::Postgres> for SectionOverview {
    fn decode(
        value: sqlx::postgres::PgValueRef<'r>,
    ) -> Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
        let mut decoder = sqlx::postgres::types::PgRecordDecoder::new(value)?;
        let id = decoder.try_decode::<SectionId>()?;
        let name = decoder.try_decode::<NonEmptyString>()?;
        let guest = decoder.try_decode::<bool>()?;
        Ok(SectionOverview { id, name, guest })
    }
}

impl sqlx::Type<sqlx::Postgres> for SectionOverview {
    fn type_info() -> sqlx::postgres::PgTypeInfo {
        sqlx::postgres::PgTypeInfo::with_name("BASIC_SECTION_INFO")
    }
}

#[derive(Serialize, Deserialize, sqlx::Type, TS, Debug)]
#[ts(export)]
#[sqlx(type_name = "SECTION_STATS")]
pub struct SectionStats {
    pub youth_leaders: i32,
    pub member_count: i32,
    #[ts(type = "string")]
    pub last_stat_update: Date,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct ExtendedSectionOverview {
    #[serde(flatten)]
    #[macrow(flatten)]
    pub basic: crate::section::SectionOverview,
    pub tags: Vec<NonEmptyString>,
    #[serde(flatten)]
    pub stats: SectionStats,
}

#[derive(Serialize, TS)]
#[ts(export)]
pub struct SectionHeadsRow {
    pub section: SectionId,
    pub head_id: UserId,
    pub head_email: Email,
}

#[derive(Deserialize, TS)]
#[ts(export)]
pub struct SectionPut {
    pub name: NonEmptyString,
    #[serde(flatten)]
    pub stats: SectionStats,
    pub tags: Vec<NonEmptyString>,
    #[serde(default)]
    pub guest: bool,
}

#[derive(Serialize, TS)]
#[ts(export)]
pub struct Section {
    #[serde(flatten)]
    pub overview: ExtendedSectionOverview,
    pub heads: Vec<UserOverview>,
    pub partial_heads: Vec<PartialUserOverview>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub relevant_events: Option<Vec<EventOverview>>,
}

diff! {
    pub struct SectionDiff {
        pub name: Option<NonEmptyString>,
        pub add_heads: Vec<UserId>,
        pub remove_heads: Vec<UserId>,
        pub invite_heads: Vec<Invitation>,
        pub youth_leaders: Option<u32>,
        pub member_count: Option<u32>,
        #[ts(type = "string | undefined | null")]
        pub last_stat_update: Option<Date>,
        pub tags: Option<Vec<NonEmptyString>>,
    }
}

#[derive(Serialize, Macrow)]
pub struct TemplateSection {
    pub id: SectionId,
    pub name: NonEmptyString,
}
