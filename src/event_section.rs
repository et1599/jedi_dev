use std::borrow::Cow::{self, Borrowed, Owned};

use crate::{
    event::form::FilledForm,
    helpers::{Email, NonEmptyString},
    management_user::{
        split_users, BasicUserInfo, Invitation, PartialUserOverview, UserKind, UserOverview,
    },
    prelude::*,
    section::SectionStats,
};

#[derive(Serialize, Deserialize)]
pub struct EventSectionInfoParams {
    #[serde(default)]
    pub as_csv: bool,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct EventSectionInfo {
    pub section_id: SectionId,
    pub section_name: NonEmptyString,
    pub max_delegates: i32,
    pub requests: i32,
    pub positioned_requests: i32,
    pub guest_requests: i32,
    #[macrow(context)]
    pub link: NonEmptyString,
}

impl EventSectionInfo {
    pub const HEADER: [&'static str; 7] = [
        "section id",
        "section name",
        "total allowed delegates",
        "total requests",
        "total ordered requests",
        "total guest requests",
        "link",
    ];
}

pub fn event_section_uri(
    ctx: StaticContext,
    event: EventId,
    section: &SectionId,
    secret: &NonEmptyString,
) -> NonEmptyString {
    // NOTE: This sucks; but we only have to do this here, everything else does
    // templating anyway where the URI creation works properly
    NonEmptyString(format!(
        "{}/user/event/{event}/{section}/signup?secret={secret}",
        ctx.origin,
    ))
}

#[derive(Macrow)]
pub struct EventExportRow {
    pub section_id: SectionId,
    pub section_name: NonEmptyString,
    pub allowed_count: i32,
    pub ordered_count: i64,
    pub guest_count: i64,
    pub first_name: Option<NonEmptyString>,
    pub last_name: Option<String>,
    pub email: Option<Email>,
    pub uncompressed_position: Option<i32>,
    pub position: Option<i32>,
    pub included_as_delegate: Option<bool>,
    pub included_as_guest: Option<bool>,
    #[macrow(try_from)]
    pub form: FilledForm,
}

impl EventExportRow {
    pub fn header<'a>(form: impl IntoIterator<Item = &'a str>) -> impl Iterator<Item = &'a str> {
        [
            "section id",
            "section name",
            "total allowed delegates",
            "total ordered requests",
            "total guest requests",
            "first name",
            "last name",
            "email",
            "uncompressed position",
            "position",
            "included as delegate",
            "included as guest",
        ]
        .into_iter()
        .chain(form)
    }

    pub fn into_record(self) -> impl Iterator<Item = Cow<'static, [u8]>> {
        let fmt_bool = |b| if b { "yes" } else { "no" };

        [
            Owned(self.section_id.0 .0),
            Owned(self.section_name.0),
            Owned(self.allowed_count.to_string()),
            Owned(self.ordered_count.to_string()),
            Owned(self.guest_count.to_string()),
            Owned(self.first_name.map_or_else(String::new, |x| x.0)),
            Owned(self.last_name.unwrap_or_default()),
            Owned(self.email.map_or_else(String::new, |x| x.0)),
            self.uncompressed_position
                .map_or(Borrowed(""), |v| Owned(v.to_string())),
            self.position.map_or(Borrowed(""), |v| Owned(v.to_string())),
            Borrowed(self.included_as_delegate.map_or("", fmt_bool)),
            Borrowed(self.included_as_guest.map_or("", fmt_bool)),
        ]
        .into_iter()
        .chain(self.form.0.into_iter().map(move |v| match v {
            None => Borrowed(""),
            Some(FormValue::Boolean(b)) => Borrowed(fmt_bool(b)),
            Some(FormValue::Number(num)) => Owned(num.to_string()),
            Some(FormValue::String(s)) => Owned(s),
        }))
        .map(|v| match v {
            Borrowed(s) => Borrowed(s.as_bytes()),
            Owned(s) => Owned(s.into_bytes()),
        })
    }
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
pub struct Request {
    pub id: RequestId,
    pub first_name: NonEmptyString,
    pub last_name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<Email>,
    #[macrow(try_from)]
    pub form: FilledForm,
}

#[derive(Macrow)]
pub struct DbRequest {
    #[macrow(flatten)]
    pub request: crate::event_section::Request,
    pub position: Option<i32>,
    pub accepted_as_guest: bool,
}

#[derive(Macrow)]
pub struct DbEventSectionInner {
    #[macrow(context)]
    pub user_kind: UserKind,
    pub helpers: Vec<BasicUserInfo>,
    pub heads: Vec<BasicUserInfo>,
    pub max_delegates: i32,
    pub last_section_stats: SectionStats,
    pub is_guest_section: bool,
    pub secret: NonEmptyString,
}

#[derive(Serialize, TS, Macrow)]
#[ts(export)]
#[macrow(try_from = "crate::event_section::DbEventSectionInner")]
pub struct EventSectionInner {
    pub helpers: Vec<UserOverview>,
    pub partial_helpers: Vec<PartialUserOverview>,
    pub heads: Vec<UserOverview>,
    pub partial_heads: Vec<PartialUserOverview>,
    pub max_delegates: i32,
    #[serde(flatten)]
    pub last_section_stats: SectionStats,
    pub is_guest_section: bool,
    #[serde(skip)]
    pub secret: NonEmptyString,
}

impl TryFrom<DbEventSectionInner> for EventSectionInner {
    type Error = sqlx::Error;

    fn try_from(value: DbEventSectionInner) -> Result<Self, sqlx::Error> {
        let (helpers, partial_helpers) = split_users(value.helpers, value.user_kind);
        let (heads, partial_heads) = split_users(value.heads, value.user_kind);

        Ok(EventSectionInner {
            helpers,
            partial_helpers,
            heads,
            partial_heads,
            max_delegates: value.max_delegates,
            last_section_stats: value.last_section_stats,
            is_guest_section: value.is_guest_section,
            secret: value.secret,
        })
    }
}

#[derive(Serialize, TS)]
#[ts(export)]
pub struct EventSection {
    #[serde(flatten)]
    pub inner: EventSectionInner,
    // TODO: Remove & replace with something that doesn't require allocating a list that the
    // frontend can just arbitrarily size, see #174
    pub delegates: Vec<Option<RequestId>>,
    pub guests: Vec<RequestId>,
    pub requests: HashMap<RequestId, Request>,
    pub uri: NonEmptyString,
}

#[derive(PartialEq, Eq, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum MoveRequestTarget {
    NotAccepted,
    Guest,
    #[serde(untagged)]
    Position(u16),
    #[serde(untagged)]
    Section {
        section: SectionId,
    },
}

#[derive(Deserialize, TS)]
#[ts(export)]
pub struct MoveRequest {
    pub request: RequestId,
    #[ts(type = r#""not-accepted" | "guest" | number | { "section": string }"#)]
    pub target: MoveRequestTarget,
}

diff! {
    pub struct EventSectionDiff {
        pub remove_helpers: Vec<UserId>,
        pub invite_helpers: Vec<Invitation>,
        pub move_requests: Vec<MoveRequest>,
        pub remove_requests: Vec<RequestId>,
    }
}

#[derive(Serialize)]
pub struct TemplateEventSection {
    pub event: EventId,
    pub section: SectionId,
}

#[derive(Serialize, Macrow)]
pub struct TemplatePartialRequest {
    pub id: RequestId,
    #[macrow(flatten)]
    pub event: crate::event::TemplateEvent,
    #[macrow(flatten)]
    pub section: crate::section::TemplateSection,
    pub max_delegates: i32,
}

#[derive(Macrow)]
pub struct MaybePartialTemplateRequest {
    #[macrow(flatten)]
    pub partial: crate::event_section::TemplatePartialRequest,
    pub first_name: Option<NonEmptyString>,
    pub last_name: Option<String>,
    pub filled_form: Option<Vec<JsonValue>>,
    pub current_placing: Option<i32>,
    pub current_compressed_placing: Option<i32>,
    pub accepted_as_guest: bool,
}

#[derive(Serialize)]
pub struct TemplateRequest {
    #[serde(flatten)]
    pub partial: TemplatePartialRequest,
    pub first_name: NonEmptyString,
    pub last_name: String,
    pub form: FilledForm,
    pub current_placing: Option<i32>,
    pub current_compressed_placing: Option<i32>,
    pub accepted_as_guest: bool,
    pub signup_allowed: bool,
}
