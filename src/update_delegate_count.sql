-- what all the variables mean:
--
-- D = full number of delegates
-- k = number of included sections
-- JL = number of youth leaders
-- M = member count
--
-- the formula is:
--  1 + floor(
--      0.5 + (D - k) * (
--          1/2 * JL_n/(\sum JL)
--          + 1/2 * sqrt(M_n)/(\sum sqrt(M))
--      )
--  )
UPDATE event_sections
SET max_delegates = 1 + FLOOR(
        0.5 + (max_total_delegates - section_count) * (
            0.5 * (last_section_stats).youth_leaders / GREATEST(total_youth_leaders, 1)
            + 0.5 * SQRT((last_section_stats).member_count) / GREATEST(total_sqrt_member_count, 1)
        )
    )
FROM
    events,
    (
        SELECT
            es.event,
            COUNT(es.section) AS section_count,
            SUM((es.last_section_stats).youth_leaders) AS total_youth_leaders,
            SUM(SQRT((es.last_section_stats).member_count)) AS total_sqrt_member_count
        FROM event_sections AS es JOIN sections AS s ON es.section = s.id
        WHERE NOT s.guest
        GROUP BY es.event
    ) AS aggregates,
    sections
WHERE events.id = event_sections.event
    AND sections.id = event_sections.section
    AND NOT sections.guest
    AND aggregates.event = event_sections.event
    AND event_sections.event = $1
