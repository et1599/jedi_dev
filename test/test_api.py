# requires: pytest, pytest-dependency

import requests
import json
import pytest
import random
import string


def uri(path):
    return f"http://localhost:8080/{path}"


def api(path):
    return uri(f"api/v1/{path}")


class User(object):
    def __init__(
        self,
        name: str,
        email: str,
        password: str | None = None
    ):
        self.name: str = name
        self.email: str = email
        self.password: str = (
            password
            if password is not None
            else "".join(
                random.choice(
                    string.ascii_letters + string.digits +
                    string.punctuation
                )
                for _ in range(32)
            )
        )

    def login(self) -> 'Login':
        return Login(self)

    def assert_failing_login(self):
        Login(self, failing=True)


class Login(object):
    def __init__(self, user: User, *, failing: bool = False):
        self.user = user
        self.session = requests.Session()

        self.session.get(uri("manage/login")).raise_for_status()

        response = self.session.post(
            uri("manage/login"),
            data={
                "email": self.user.email,
                "password": self.user.password,
            },
            allow_redirects=False,
        )
        if not failing:
            assert response.status_code == 303
            assert response.headers["location"] == \
                "/manage/app/dashboard/"
            assert "login-token" in response.cookies
        else:
            assert response.status_code not in [200, 303, 307]
            assert "login-token" not in response.cookies
            return

        self.session.get(
            uri("manage/app/dashboard/"),
            cookies=self.session.cookies
        ).raise_for_status()

    def __enter__(self) -> requests.Session:
        return self.session

    def __exit__(self, _exc_type, _exc_value, _traceback):
        self.session.post(uri("manage/logout"))\
            .raise_for_status()


def email_log(*, email=None, tag=None):
    params = {
        k: v
        for k, v in [("email", email), ("tag", tag)]
        if v is not None
    }
    out = requests.get(uri("email-log"), params=params)
    out.raise_for_status()
    return out.json()


try:
    # "" shouldn't exist as an email, we just want to make sure
    # it doesn't 404 to check that we're running in a debug
    # build
    email_log(email="")
except requests.HTTPError:
    assert False, \
        "The email log is not accessible, so you're probably \
        running a release build. This is not supported by this \
        testing script."


any_type = object()
rest = object()


class Permutation(object):
    def __init__(self, *args):
        self.shapes = args


class Eq(object):
    def __init__(self, value):
        self.value = value


class All(object):
    def __init__(self, *args):
        self.shapes = args


class Any(object):
    def __init__(self, *args):
        self.shapes = args


def assert_matches(obj: object, shape: object) -> None:
    print(obj, shape)
    if shape is any_type:
        return
    elif isinstance(shape, type):
        msg = f"{obj!r} isn't of type {type!r}"
        if shape is float and isinstance(obj, int):
            # deal with json not really delineating
            obj = float(obj)
        assert isinstance(obj, shape), msg
    elif isinstance(shape, tuple):
        assert isinstance(obj, tuple) or isinstance(obj, list)
        obj = list(obj)
        assert len(obj) == len(shape)
        for i, j in zip(obj, shape):
            assert_matches(i, j)
    elif isinstance(shape, Permutation):
        assert isinstance(obj, list)
        assert len(obj) == len(shape.shapes)
        if len(obj) == 0:
            return
        else:
            for idx, i in shape.shapes:
                try:
                    assert_matches(obj[0], i)
                    assert_matches(
                        obj[1:],
                        Permutation(*(
                            shape.shapes[:idx] +
                            shape.shapes[idx + 1:]
                        ))
                    )
                except AssertionError:
                    continue
    elif isinstance(shape, list):
        assert len(shape) == 1, "invalid shape"
        assert isinstance(obj, list), f"{obj!r} isn't a list"
        for i in obj:
            assert_matches(i, shape[0])
    elif isinstance(shape, dict):
        if rest in shape:
            extra = shape[rest]
            shape = {
                k: v
                for k, v in shape.items()
                if k is not rest
            }
        else:
            extra = None

        assert isinstance(obj, dict), f"{obj!r} isn't a dict"
        for k, v in shape.items():
            msg = f"{obj!r} doesn't contain key {k!r}"
            assert k in obj, msg
            assert_matches(obj[k], v)

        for k, v in obj.items():
            if k not in shape:
                msg = f"{obj!r} contains extra key {k}"
                assert extra is not None, msg
                assert_matches(v, extra)
    elif hasattr(shape, "__call__"):
        msg = f"assertion for {obj!r} failed"
        assert shape(obj) in [None, True], msg
    elif isinstance(shape, Eq):
        msg = f"{obj!r} isn't {shape.value!r}"
        assert obj == shape.value, msg
    elif isinstance(shape, Any):
        errors = []
        for i in shape.shapes:
            try:
                assert_matches(obj, i)
                return
            except Exception as e:
                errors.append(e)
        raise errors
    elif isinstance(shape, All):
        for i in shape.shapes:
            assert_matches(obj, i)
    elif isinstance(shape, str) or isinstance(shape, int):
        assert obj == shape, f"{obj!r} isn't {shape!r}"
    elif shape is None:
        assert obj is None, f"{obj!r} isn't None"
    else:
        assert False, "invalid shape: {shape!r}"


section_a = "test-a"
section_a_name = "Test section A"
section_b = "test-b"
section_b_name = "Test section B"

super_admin = User(
    "Super Admin",
    "admin@change.this",
    "change this",
)
admin = User("Test user: Admin", "admin@example.com")
organizer = User(
    "Test user: Organizer",
    "organizer@example.com",
)
head_a = User("Test user: Head A", "a@example.com")
head_b = User("Test user: Head B", "b@example.com")
helper = User("Test user: Helper", "helper@example.com")
users_to_create = [
    (admin, "admin", [section_a], []),
    (organizer, "organizer", [], []),
    (head_a, "normal", [], [section_a]),
    (head_b, "normal", [], [section_b]),
]


user_kind = Any("normal", "organizer", "admin", "super-admin")


def mktest(depends=[]):
    def decorator(f):
        def wrapper():
            requests.post(
                uri("log"),
                data=f"Running {f.__name__}",
            )
            f()
            requests.post(
                uri("log"),
                data=f"Ran {f.__name__}",
            )
        return pytest.mark.dependency(depends=depends)(wrapper)
    return decorator


@mktest()
def test_login():
    super_admin.login()


@mktest(["test_login"])
def test_login_flow():
    with super_admin.login():
        pass


@mktest(["test_login_flow"])
def test_clean_users():
    with super_admin.login() as session:
        users = session.get(api("users"))
        users.raise_for_status()

        partial_users = session.get(api("partial-users"))
        partial_users.raise_for_status()

        all_users = {
            i["email"]: i["id"]
            for i in users.json() + partial_users.json()
        }

        for i in users_to_create:
            email = i[0].email
            if email in all_users:
                session.delete(api(f"user/{all_users[email]}"))


@mktest(["test_login_flow"])
def test_sections():
    with super_admin.login() as session:
        section_a_data = {
            "id": section_a,
            "name": section_a_name,
            "heads": [],
            "youth_leaders": 10,
            "member_count": 30,
            "last_stat_update": "2023-09-29",
            "tags": ["test"],
            "guest": False,
        }
        session.put(
            api(f"section/{section_a}"),
            data=json.dumps(section_a_data),
        ).raise_for_status()

        session.put(
            api(f"section/{section_b}"),
            data=json.dumps({
                "name": section_b_name,
                "heads": [],
                "youth_leaders": 5,
                "member_count": 15,
                "last_stat_update": "2023-09-30",
                "tags": ["test"],
                "guest": False,
            }),
        ).raise_for_status()

        sections = session.get(api("sections"))
        sections.raise_for_status()
        sections = sections.json()

        assert_matches(
            sections,
            [{
                "id": str,
                "name": str,
                "tags": [str],
                "youth_leaders": int,
                "member_count": int,
                "last_stat_update": str,
                "guest": bool,
            }],
        )

        assert any(i["id"] == section_a for i in sections)


@mktest(["test_clean_users", "test_sections"])
def test_create_users():
    with super_admin.login() as session:
        for i, kind, admin_for, head_for in users_to_create:
            session.post(
                api("user"),
                data=json.dumps({
                    "email": i.email,
                    "display_name": i.name,
                    "kind": kind,
                    "admin_for": admin_for,
                }),
            ).raise_for_status()

            secret = email_log(email=i.email, tag="signup")
            print(secret)

            signup = requests.post(
                uri("manage/signup"),
                params={
                    "email": i.email,
                    "secret": secret,
                },
                data={
                    "new_email": i.email,
                    "display_name": i.name,
                    "password": i.password,
                },
                allow_redirects=False,
            )
            assert signup.status_code == 303

            with i.login():
                pass


@mktest(["test_create_users"])
def test_config():
    with head_a.login() as session:
        out = session.get(api("config"))
        out.raise_for_status()
        duration = All(float, lambda x: x > 0)
        assert_matches(
            out.json(),
            {
                "signin_length": duration,
                "archive_length": duration,
                "passive_update_length": duration,
                "active_update_length": duration,
            }
        )


@mktest(["test_create_users"])
def test_self_password_reset():
    with head_a.login():
        pass

    requests.post(
        uri("manage/setup-reset-password"),
        data={"email": head_a.email},
    ).raise_for_status()

    secret = email_log(email=head_a.email, tag="password-reset")

    with head_a.login():
        pass  # head_a login should still be valid

    requests.get(
        uri("manage/reset-password"),
        params={"email": head_a.email, "secret": secret},
    ).raise_for_status()

    password = "another password"
    reset_password_result = requests.post(
        uri("manage/reset-password"),
        params={"email": head_a.email, "secret": secret},
        data={"password": password},
        allow_redirects=False,
    )

    assert reset_password_result.status_code == 303
    assert "login-token" in reset_password_result.cookies

    head_a.assert_failing_login()

    modified_head = User(head_a.name, head_a.email, password)
    with modified_head.login() as session:
        # Change the password back
        session.patch(
            api("user/self"),
            data=json.dumps({"password": head_a.password}),
        ).raise_for_status()

        # Check that this login still works
        session.get(api("config")).raise_for_status()

    # Check that the old password now works again
    with head_a.login():
        pass


@mktest(["test_create_users"])
def test_create_event():
    with organizer.login() as session:
        data = {
            "name": "Test event",
            "description": {"source": "This is a test event"},
            "state": {"type": "setup"},
            "form": [],  # TODO
            "can_users_leave": True,
            "can_users_edit_name": True,
            "max_total_delegates": 10,
            "sections": [section_a, section_b],
        }

        out = session.post(
            api("event"),
            data=json.dumps(data),
        )
        out.raise_for_status()

        id = out.json()
        assert isinstance(id, str)

        global event_id
        event_id = id

        with super_admin.login() as admin_session:
            events_out = admin_session.get(api("events"))
            events_out.raise_for_status()
            events = events_out.json()

        assert_matches(
            events,
            [{
                "id": str,
                "name": str,
                "description": {"source": str, "html": str},
                "state": Any(
                    {"type": "setup"},
                    {
                        "type": "active",
                        "signup_until": str,
                        "decisions_until": str,
                    },
                    {"type": "done", "archived_until": str},
                ),
            }],
        )

        events_dict = {i["id"]: i for i in events}

        description = {
            "source": data["description"]["source"],
            "html": f"<p>{data['description']['source']}</p>\n",
        }

        assert id in events_dict
        assert events_dict[id] == {
            "id": id,
            "name": data["name"],
            "description": description,
            "state": data["state"],
        }

        event_out = session.get(api(f"event/{id}"))
        event_out.raise_for_status()
        assert_matches(
            event_out.json(),
            {
                "id": id,
                "name": data["name"],
                "description": description,
                "state": data["state"],
                "organizers": (
                    {
                        "id":
                            session.get(api("user/self"))
                            .json()["id"],
                        "kind": "organizer",
                        "admin_for": (),
                        "display_name": organizer.name,
                    },
                ),
                "partial_organizers": (),
                "form": Eq(data["form"]),
                "can_users_leave": data["can_users_leave"],
                "can_users_edit_name":
                    data["can_users_edit_name"],
                "sections": Permutation(
                    {
                        "id": section_a,
                        "name": section_a_name,
                    },
                    {
                        "id": section_b,
                        "name": section_b_name,
                    },
                ),
                "max_total_delegates":
                    data["max_total_delegates"],
            }
        )


# TODO: PATCH  api/v1/event/...
# TODO: DELETE api/v1/event/...
# TODO: GET    api/v1/event/.../all/export
# TODO: GET    api/v1/users
# TODO: GET    api/v1/partial-users
# TODO: POST   api/v1/user/.../reset-password
# TODO: POST   api/v1/user/mass-reset-password
# TODO: POST   api/v1/user
# TODO: GET    api/v1/user/...
# TODO: PATCH  api/v1/user/...
# TODO: DELETE api/v1/user/...
# TODO: GET    api/v1/user/self
# TODO: PATCH  api/v1/user/self
# TODO: DELETE api/v1/user/self
# TODO: POST   api/v1/event/.../.../order/lock
# TODO: POST   api/v1/event/.../.../order/unlock
# TODO: GET    api/v1/event/.../...
# TODO: PATCH  api/v1/event/.../...
# TODO: GET    api/v1/section/...
# TODO: PATCH  api/v1/section/...
# TODO: DELETE api/v1/section/...
